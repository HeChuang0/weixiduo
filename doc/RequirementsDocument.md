# 维喜多果园餐厅需求文档
## 经营范围：
#### 高端水果店，兼营国外进口小吃（干果等），兼营水产品，兼营农副健康食品、线下实体店主营新品体验试吃兼当地特色快餐。
## 业务需求：
#### 线上会员卡和相关促销，在线商城
## 技术需要：
#### 微信开发、后台开发、前端界面设计
## 职务需求：
#### 技术相关成员、技术交流员、工作日记登录员
## 任务分配计划：

序号 | 工作内容 |责任人
---|---|---
1 | 需求文档起草 |徐繁韵
2 | 前端api接口设计 |汤福平
3 | 微信前端界面设计 |曾雨琪、甘婷、张子晗
4 | PC端商家后台界面设计 |王钦、徐繁韵
5 | 微信端商家订单处理小程序设计|徐繁韵、王钦
6 | 数据库搭建 | 徐繁韵
7 | 后台框架搭建 |汤福平
8 | 接口实现 |全体后台成员
9 | 设计文档完善、审核 |邓志刚、王晶晶
10 | PC端测试与文档记录 |李泽乾、邓志刚
11 | 微信端测试与文档记录 |许海通、王晶晶
12 | 工作日志记录员 |徐繁韵
注：以实况为准

## 功能设计：（仿良品铺子）
#### 微信公众号底部菜单栏，主菜单（会员中心、进入商城、在线服务）


### 一、会员中心（会员卡、新人礼包、每日签到、积分兑换）
#### 会员等级：铜卡、银卡、金卡、黑钻
#### 默认微信号注册，填写详细信息后是铜卡会员（免费到店体验新品），银卡、金卡、黑钻是用金钱买
#### 新人礼包：新人第一次关注即可领取相应优惠券和现金券
#### 每日签到：每日签到赚得积分兑换优惠券等
#### 积分兑换：可兑换现金券也可以大转盘抽奖

### 二、在线服务（在线客服、我的红包、我的订单，我的积分，线下体验店）
#### 在线客服：在线聊天功能
#### 我的红包：  查看已领取的优惠券
#### 我的订单：  查看消费订单（通过消费订单也可兑换积分）
#### 我的积分：  查看积分
#### 线下体验店：  腾讯地图定位附近体验店铺

### 三、进入商城（现阶段没有子菜单，做二次开发）
#### 买家端：
##### 浏览商品（关键字查询、分类检索、最新产品、产品推荐）
#####         购物车：修改购物车、清空购物车（保留购物信息只修改状态）、购买
#####         订单：付款、取消订单（管理员审核后退款）、确认收货（7天后自动确认收货）、申请退款
#####         个人中心：查看我的订单、查看我的红包、查看我的积分、查看个人详细信息、查看收货地址。。。
#### 卖家端：
##### 商品管理：添加商品、删除商品、修改商品、查询商品、商品上下架
##### 商品类目：添加类目、删除类目、修改类目
#####  订单管理：发货、审核买家取消订单、查看订单、审核申请退款
#####        会员管理：查看会员列表（包括会员信息）
##### 优惠的设置：新人礼包的设置、优惠券的设置
##### 会员卡的设置：增删改
##### 查看所有会员：查看会员详细信息、查看用户积分、查看用户签到

## 功能流程图

## 数据库设计

### 初步设计
#### 1．会员等级表：（id，等级名，折扣）
#### 2．会员表：（id，姓名，性别、家庭住址、年龄、会员真实姓名、手机号、openid、注册时间、会员等级id、创建时间、修改时间）
#### 3．收货地址表：（收货地址Id、会员Id、收货地址）
#### 4．订单表：（订单Id、openid、收货地址id、商品id、金额总数、订单状态（新下单、完结订单）、支付状态（已支付、未支付）、创建时间、修改时间）
#### 5．订单详情表：（详情id、订单id、商品id、当前价格、数量、图片、创建时间、修改时间）
#### 6．商品表：（商品Id、商品名称、单价、库存、描述、图片、状态（正常、下架）、类目Id、创建时间、修改时间）
#### 7．类目表：（类目Id、类目名称）
#### 8．管理员信息表：（管理员Id、登录名、密码、创建时间、更新时间）
#### 9．积分表：（id，openid，积分数）
#### 10．签到表：（签到Id、openid、创建时间）
#### 11．红包表：（id，openid，手机号，优惠券折扣卡,领取时间）
#### 12．礼包表：（礼包id、礼包名称、创建时间、修改时间）
#### 13．新人礼包表：（礼包Id、openid、礼包名称、创建时间）
#### 14．购物车：（购物车Id、openid、商品Id、数量、总额、创建时间、更新时间）

### 数据库表设计

#### 1.会员等级表：（vip_card）
字段 | 类型 |注释
---|--- |---
id | int |自增长id
vip_name | varchar(32) |会员等级名
discount | real |折扣

#### 2.会员表：（vip_member）
字段 | 类型 |注释
---|--- |---
id | int |自增长id
vip_card_id | int |会员等级id
name | varchar(32) |姓名
sex |varchar(5)|性别
haschild |tinyint|是否有小孩（0没有，1有）
ismarried |tinyint|是否已婚（0未婚，1已婚）
real_name |varchar(32)|会员真实姓名
birthday |date|出生年月
address |varchar（64）|家庭住址
phone |Varchar（11）|手机号
openid|varchar(32)|微信openid
create_time|datetime|创建时间
update_time|datetime|修改时间

#### 3.收货地址表：（address）

字段 | 类型 |注释
---|--- |---
id | int |自增长id
openid|varchar(32)|openid
address|varchar(64)|收货地址
#### 4.订单表：（order_master）

字段 | 类型 |注释
---|--- |---
id | varchar(32) |订单id
openid|varchar(32)|openid
address|varchar(64)|收货地址
order_amount|decimal(8,2)|金额总数
order_status|tinyint|订单状态（新下单、完结订单）
pay_status|tinyint|支付状态（已支付、未支付）
create_time|datetime|创建时间
update_time|datetime|修改时间

#### 5.	订单详情表：(order_detail)
字段 | 类型 |注释
---|--- |---
id | int |自增长id
order_id|varchar(32)|订单id
product_id|int|商品id
product_name|varchar(32)|商品
product_price|decimal(8,2)|当前价格
discount_price|decimal(8,2)|折后价格
product_quantity|int|数量
product_icon|varchar(32)|图片
create_time|datetime|创建时间
update_time|datetime|修改时间

#### 6.	商品表：product_info

字段 | 类型 |注释
---|--- |---
id | int |自增长id
product_name|varchar(32)|商品名称
product_price|decimal(8,2)|单价
product_stock|int|库存
product_description|varchar(64)|描述
product_icon|varchar(32)|图片
product_status|tinyint|状态（正常、下架）
Category_Id|int|类目Id
create_time|datetime|创建时间
update_time|datetime|修改时间

#### 7.	类目表：product_category
字段 | 类型 |注释
---|--- |---
id | int |自增长id
category_name|varchar(32)|类目名称
create_time|datetime|创建时间
update_time|datetime|修改时间

#### 8.	管理员表：manager
字段 | 类型 |注释
---|--- |---
id | int |自增长id
manager_name|varchar(32)|用户名
manager_password|varchar(32)|密码
openid|varchar(32)|openid
create_time|datetime|创建时间
update_time|datetime|修改时间

#### 9.	积分表：integral

字段 | 类型 |注释
---|--- |---
id | int |自增长id
openid|varchar(32)|openid
integral_quantity|int|积分
create_time|datetime|创建时间
update_time|datetime|修改时间

#### 10.签到表：sign

字段 | 类型 |注释
---|--- |---
id | int |自增长id
openid|varchar(32)|openid
sign_date|datetime|签到时间

#### 11.	红包表：red_packet
字段 | 类型 |注释
---|--- |---
id | int |自增长id
openid|varchar(32)|openid
cash_coupon|decimal（8,2）|现金券
full_cut|decimal（8,2）|满减标志
type|tinyint|0新人礼包1普通现金券
receive_time|datetime|领取时间

#### 12.	优惠设置表：Discount_set

字段 | 类型 |注释
---|--- |---
id | int |自增长id
discount_name|varchar(32)|礼包名称
cash_coupon|decimal（8,2）|现金券
full_cut|decimal（8,2）|满减标志
type|tinyint|0新人礼包1普通现金券
create_time|datetime|创建时间
update_time|datetime|修改时间

#### 13.	购物车：Shopping_Cart
字段 | 类型 |注释
---|--- |---
id | int |自增长id
openid|varchar(32)|openid
product_id|int|商品id
product_name|varchar(32)|商品
product_price|decimal(8,2)|当前价格
order_amount|decimal(8,2)|金额总数
product_quantity|int|数量
product_icon|varchar(32)|图片
cart_status|tinyint|购物车状态
create_time|datetime|创建时间
update_time|datetime|修改时间
注：购物车初步放在redis里，设定过期时间

## 前端界面设计 
### 前端界面草图
### 前端API接口设计

## 注
#### 1.	数据库表名和表中的字段都是小写，若命名是多个单词的拼接，单词与单词之间需用“_”分割（如product_category）
#### 2.	实体类，类名和属性命名需与数据库对应，类名首字母大写，多个单词拼接的类名每个单词首字母大写，属性首字母不必大写，第二个单词的首字母需大写。
#### 3.	数据库字段命名和实体类中字段命名在拼写上需一致。
#### 4.	本次项目需要达到的目标：会员卡功能可以上线、线上商城可以使用
#### 5.	数据库会员表的设计（涉及比较详尽的信息，如生日、性别、是否有小孩、是否已婚）经后最数据分析，针对性的服务于客户（在生日当天以特殊方式问候顾客、不同性别提供不同服务、等等）真正做到服务有对象。
