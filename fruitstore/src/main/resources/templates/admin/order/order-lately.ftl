<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<html lang="en"/>
<head>
    <meta charset="UTF-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>最新订单</title>
    <link rel="stylesheet" type="text/css" href="/js/css/layui.css"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
</head>
<body style="padding: 5px;">
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>最新订单</legend>
</fieldset>
<div id="orderTable" lay-filter="order-filter"></div>
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">详</a>
    <a class="layui-btn layui-btn-xs" lay-event="cancel">拒</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="send">发</a>
</script>
<script type="text/html" id="payStatusTpl">
    {{#  if(d.payStatus == 0){ }}
    <span class="layui-badge layui-bg-red">未支付</span>
    {{#  } else if(d.payStatus == 1) { }}
    <span class="layui-badge layui-bg-gray">已支付</span>
    {{#  } }}
</script>
<script type="text/html" id="orderStatusTpl">
    {{#  if(d.orderStatus == 0){ }}
    <span class="layui-badge layui-bg-red">新订单</span>
    {{#  } }}
</script>
<script src="/js/layui.all.js"></script>
<script>
    layui.use('table', function(){
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#orderTable'
            ,height: 'full'
            ,url: '/OrderMasterAction/latelyOrderList' //数据接口
            ,page: true //开启分页
            ,groups: 5
            ,limit: 8
            ,limits: [8,6,4]
            ,request: {
                pageName: 'page' //页码的参数名称，默认：page
                ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
            }
            ,response: {
                statusName: 'success' //数据状态的字段名称，默认：code
                ,statusCode: 200 //成功的状态码，默认：0
                ,msgName: 'msg' //状态信息的字段名称，默认：msg
                ,countName: 'total' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,cols: [[ //表头
                {field: 'orderid', title: '订单ID', minWidth: 20}
                ,{field: 'openid', title: '用户ID'}
                ,{field: 'address', title: '收货地址'}
                ,{field: 'orderAmount', title: '金额总数'}
                ,{field: 'orderStatus', title: '订单状态', templet: '#orderStatusTpl', align: 'center'}
                ,{field: 'payStatus', title: '支付状态', templet: '#payStatusTpl', align: 'center'}
                ,{field: 'updateTime', title: '更新时间'}
                ,{field: 'createTime', title: '创建时间'}
                ,{field: '', title: '操作', toolbar: '#barDemo', align: 'center'}
            ]]
        });
        table.on('tool(order-filter)', function(obj){
            var data = obj.data;
            if(obj.event === 'detail'){
                //layer.msg('ID：'+ data.id + ' 的查看操作');
                detail(obj);
            } else if (obj.event === 'send') {
                layer.confirm('您已经确认货源充足，会员信息属实。', function(index){
                    $.get({
                        url: '/OrderMasterAction/finish?orderid=' + obj.data.orderid
                    });
                    layer.msg('发货成功');
                    obj.del();
                    layer.close(index);
                });
            } else if (obj.event === 'cancel') {
                layer.confirm('您确定要取消订单吗？', function(index){
                    $.get({
                        url: '/OrderMasterAction/cancel?orderid=' + obj.data.orderid
                    });
                    layer.msg('取消成功');
                    obj.del();
                    layer.close(index);
                });
            }
        });
    });
    function detail(obj) {
        var data = obj.data;
        var index = layer.open({
            type: 2
            , anim: 0
            , title: '详情'
            , content: '/admin/orderDetail/' + data.orderid
        });
        // layer.msg('点击的ORDERID: ' + data.orderid);
        layer.full(index);
    }
</script>
</body>
</html>