<!DOCTYPE html>
<html lang="en"/>
<head>
    <meta charset="UTF-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="/js/css/layui.css" media="all"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
</head>
<body style="padding:0px 20px 0px 20px;">
<h1 class="layui-header"><i class="layui-icon" style="font-size: 30px;">&#xe60a;</i>详情</h1>
<div class="layui-form">
    <table class="layui-table"  style="width: 500px;">
        <colgroup>
            <col>
            <col>
        </colgroup>
        <thead>
        <tr>
            <th>订单ID</th>
            <th>订单总金额</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td id="orderId">1</td>
            <td id="orderAmount">23</td>
        </tr>
        </tbody>
    </table>
    <hr>
    <table class="layui-table">
        <#--<colgroup>-->
            <#--<col>-->
            <#--<col>-->
            <#--<col>-->
            <#--<col>-->
            <#--<col>-->
        <#--</colgroup>-->
        <thead style="display:table; width:calc( 99% - 16px);table-layout:fixed;">
        <tr>
            <th>商品ID</th>
            <th>商品名称</th>
            <th>价格</th>
            <th>数量</th>
            <th>总价</th>
        </tr>
        </thead>
        <tbody id="orderDetail" style="display:block;max-height:120px;overflow-y:scroll;">

        </tbody>
    </table>

    <table class="layui-table" style="width: 97.5%">
        <colgroup>
            <col width="150">
            <col width="150">
            <col width="150">
            <col width="150">
            <col width="150">
            <col width="150">
        </colgroup>
        <thead>
        <tr>
            <th>优惠券名</th>
            <th>价值</th>
            <th>满足条件</th>
            <th>类型</th>
            <th>有效时间</th>
            <th>过期时间</th>
        </tr>
        </thead>
        <tbody id="discountSet">

        </tbody>
    </table>

    <p id="address" style="margin-bottom:15px;color: #666"></p>

    <button class="layui-btn layui-btn-normal" id="finish">发货</button>
    <button class="layui-btn layui-btn-danger" id="delete">删除订单</button>
</div>
<script src="/js/layui.all.js"></script>
<script>
    htmlobj=$.ajax({url:"/OrderMasterAction/detail?orderid=${id}",async:false});
    var data = JSON.parse(htmlobj.responseText);
    var order = data.data;
    var orderDetail = order[0].orderDetails;
    var discountSet = order[0].discountSet;
    //console.log(discountSet==undefined);

    $("#orderId").text(order[0].orderid);
    $("#orderAmount").text(order[0].orderAmount);

    for (var i = 0; i < orderDetail.length; i++) {
        document.getElementById('orderDetail').innerHTML+='<tr style="display:table;width: 99%;table-layout:fixed;">' +
                '<td>'+orderDetail[i].productId+'</td>' +
                '<td>'+orderDetail[i].productName+'</td>' +
                '<td>'+orderDetail[i].productPrice+'</td>' +
                '<td>'+orderDetail[i].productQuantity+'</td>' +
                '<td>'+orderDetail[i].productPrice * orderDetail[i].productQuantity+'</td></tr>';
    }

    if(discountSet!=undefined){
        var data = JSON.stringify(discountSet);
        console.log(data);
        data = JSON.parse(data);
        console.log(data);
        document.getElementById('discountSet').innerHTML+='<tr>' +
                '<td>'+data.discountName+'</td>' +
                '<td>'+data.cashCoupon+'</td>' +
                '<td>'+data.fullCut+'</td>' +
                '<td>'+discountSetType(data.type)+'</td>' +
                '<td>'+data.startTime+'</td>' +
                '<td>'+data.endTime+'</td></tr>'
    }

    $("#address").html("收货地址：" + order[0].address);

    $("#finish").click(function(){
        layer.confirm("确认要发货吗，请确认收货地址以及货源。", { title: "发货确认" }, function (index) {
            layer.close(index);
            //console.log("完结成功");
            htmlobj=$.ajax({url:"/OrderMasterAction/finish?orderid=${id}",async:false});
            var data = JSON.parse(htmlobj.responseText);
            if(data.success == true){
                $('#finish').removeClass('layui-btn layui-btn-normal');
                $('#finish').attr('class','layui-btn layui-btn-disabled');
                $("#finish").unbind();
                return layer.msg(data.msg);
            }
        });
    });

    $("#delete").click(function(){
        layer.confirm("确认要删除吗，删除后不能恢复", { title: "删除确认" }, function (index) {
            layer.close(index);
            console.log("删除成功");
            var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            parent.layer.close(index); //再执行关闭
        });
    });

    if(order[0].orderStatus == 1){
        $('#finish').removeClass('layui-btn layui-btn-normal');
        $('#finish').attr('class','layui-btn layui-btn-disabled');
        $("#finish").unbind();
    }

    function discountSetType(type) {
        if(type == 0){
            return '<span class="layui-badge layui-bg-red">新人红包</span>';
        }else if(type == 1){
            return '<span class="layui-badge layui-bg-red">普通红包</span>';
        }else if(type == 2){
            return '<span class="layui-badge layui-bg-red">转盘红包</span>';
        }else if(type == 3){
            return '<span class="layui-badge layui-bg-red">积分红包</span>';
        }else if(type == 4){
            return '<span class="layui-badge layui-bg-red">商城红包</span>';
        }
    }

</script>
</body>
</html>