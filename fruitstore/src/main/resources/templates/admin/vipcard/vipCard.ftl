<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<html lang="en"/>
<head>
    <meta charset="UTF-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>会员卡列表</title>
    <link rel="stylesheet" type="text/css" href="/js/css/layui.css"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
</head>
<body style="padding: 5px;">
<div id="orderTable" lay-filter="order-filter">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>会员卡列表</legend>
    </fieldset>

</div>
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs" lay-event="edit">修改</a>
    <#--<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删</a>-->
</script>
<script src="/js/layui.all.js"></script>
<script>
    layui.use('table', function(){
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#orderTable'
            ,height: 'full'
            ,url: '/VipCardAction/get' //数据接口
            ,page: true //开启分页
            ,groups: 5
            ,limit: 8
            ,limits: [8,6,4]
            ,request: {
                pageName: 'page' //页码的参数名称，默认：page
                ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
            }
            ,response: {
                statusName: 'success' //数据状态的字段名称，默认：code
                ,statusCode: 200 //成功的状态码，默认：0
                ,msgName: 'msg' //状态信息的字段名称，默认：msg
                ,countName: 'total' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,cols: [[ //表头
                {field: 'id', title: 'ID'}
                ,{field: 'vipName', title: '会员卡名称'}
                ,{field: 'discount', title: '会员卡折扣'}
                ,{field: '', title: '操作', toolbar: '#barDemo',align: 'center'}
            ]]
        });
        table.on('tool(order-filter)', function(obj){
            var data = obj.data;
            if(obj.event === 'detail'){
                //layer.msg('ID：'+ data.id + ' 的查看操作');
                detail(obj);
            } else if(obj.event === 'del'){
                layer.confirm('真的删除行么', function(index){
                    $.get({
                        url: '/VipCardAction/delete?id=' + obj.data.id
                        , success: function(data){
                            layer.msg(data.msg);
                        }
                        , error: function() {
                            layer.msg('网络错误');
                        }
                    });
                    obj.del();
                    layer.close(index);
                });
            } else if(obj.event === 'edit'){
                //layer.alert('编辑行：<br>'+ JSON.stringify(data))
                var data = obj.data;
                var index = layer.open({
                    type: 2
                    ,anim: 0
                    ,title: '详情'
                    ,content: '/admin/editVipCard/'+data.id
                    ,success: function (layero, index) {
                        var body = layer.getChildFrame('body',index);
                        var iframeWin =window[layero.find('iframe')[0]['name']];
                        //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        body.find("#vipName").val(data.vipName);
                        body.find("#discount").val(data.discount);
                        body.find("#vipCardId").val(data.id);

                    }
                });
                layer.full(index);
            }
        });
    });
    function detail(obj) {
        var data = obj.data;
        var index = layer.open({
            type: 2
            , anim: 0
            , title: '详情'
            , content: '/admin/orderDetail/' + data.orderid
        });
        // layer.msg('点击的ORDERID: ' + data.orderid);
        layer.full(index);
    }
</script>
</body>
</html>