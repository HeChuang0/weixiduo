<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="utf-8"/>
    <title>商户添加</title>
    <meta name="renderer" content="webkit"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" href="/js/css/layui.css"  media="all"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
</head>
<body style="padding-right: 25px">
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>商户添加</legend>
</fieldset>

<form class="layui-form" action="POST">

    <div class="layui-form-item">
        <label class="layui-form-label">商户名称</label>
        <div class="layui-inline">
            <input type="text" name="businessName" lay-verify="required" autocomplete="off" placeholder="请输入商户名称" class="layui-input">
        </div>
    </div>

    <input type="hidden" name="businessImage" id="businessImage"/>

    <div class="layui-form-item">
        <label class="layui-form-label">商户图片</label>
        <div class="layui-upload">
            <button type="button" class="layui-btn" id="up_file">
                <i class="layui-icon">&#xe67c;</i>上传图片
            </button>
        </div>
    </div>

    <div class="layui-input-inline" id="images" style="height: 140px;overflow: hidden;">
        <label class="layui-form-label">图片预览</label>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label"></label>
        <p id="demoText"></p>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">商户地址</label>
        <div class="layui-input-inline">
            <select id="province" lay-filter="province">

            </select>
        </div>
        <div class="layui-input-inline">
            <select id="city" lay-filter="city">

            </select>
        </div>
        <div class="layui-input-inline">
            <select id="county">

            </select>
        </div>
    </div>

    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">详细地址</label>
        <div class="layui-inline">
            <textarea placeholder="请输入详细地址" class="layui-textarea" lay-verify="required" id="detailedAddress"></textarea>
        </div>
    </div>

    <input type="hidden" name="businessAddress" id="businessAddress"/>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="ok" id="submit">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="/js/layui.all.js" charset="utf-8"></script>
<script>

    layui.use(['form'], function(){

        var form = layui.form
                ,layer = layui.layer;

        //初始化省份选择框
        $.ajax({
            type:"GET",
            url:"/DistrictAction/get?parentId=0",
            success:function (data) {
                var str = "";
                for(var i = 0;i < data.data.length ; i++){
                    str += '<option value="'+data.data[i].id+ '">'+data.data[i].name+'</option>';
                }
                $('#province').html(str);
                form.render('select');
            }
        });

        //初始化市选择框
        $.ajax({
            type:"GET",
            url:"/DistrictAction/get?parentId=1",
            success:function (data) {
                var str = "";
                for(var i = 0;i < data.data.length ; i++){
                    str += '<option value="'+data.data[i].id+ '">'+data.data[i].name+'</option>';
                }
                $('#city').html(str);
                form.render('select');
            }
        });

        //初始化县选择框
        $.ajax({
            type:"GET",
            url:"/DistrictAction/get?parentId=36",
            success:function (data) {
                var str = "";
                for(var i = 0;i < data.data.length ; i++){
                    str += '<option value="'+data.data[i].id+ '">'+data.data[i].name+'</option>';
                }
                $('#county').html(str);
                form.render('select');
            }
        });

        //监听选择框事件
        form.on('select(province)', function(data){
            //console.log(data.value); //得到被选中的值
            $.ajax({
                type:"GET",
                url:"/DistrictAction/get?parentId=" + data.value,
                success:function (data) {
                    var substr = "";
                    for(var i = 0;i < data.data.length ; i++){
                        substr += '<option value="'+data.data[i].id+ '">'+data.data[i].name+'</option>';
                    }
                    $('#city').html(substr);
                    form.render('select');

                    console.log($("#city").val());

                    $.ajax({
                        type:"GET",
                        url:"/DistrictAction/get?parentId=" + $("#city").val(),
                        success:function (data) {
                            var str = "";
                            for(var i = 0;i < data.data.length ; i++){
                                str += '<option value="'+data.data[i].id+ '">'+data.data[i].name+'</option>';
                            }
                            $('#county').html(str);
                            form.render('select');
                        }
                    });

                }
            });
        });

        //监听选择框事件
        form.on('select(city)', function(data){
            //console.log(data.value); //得到被选中的值
            $.ajax({
                type:"GET",
                url:"/DistrictAction/get?parentId=" + data.value,
                success:function (data) {
                    var substr = "";
                    for(var i = 0;i < data.data.length ; i++){
                        substr += '<option value="'+data.data[i].id+ '">'+data.data[i].name+'</option>';
                    }
                    $('#county').html(substr);
                    form.render('select');
                }
            });
        });

        //点击提交按钮时，给隐藏字段赋值
        $("#submit").click(function () {
            var images = "";
            $(".layui-upload-img").each(function () {
                images = $(this)[0].src;
            });
            $("#businessImage").val(images);
            var businessAddress = $("#province").find("option:selected").text() + " " + $("#city").find("option:selected").text()
                    + " " + $("#county").find("option:selected").text() + " " + $("#detailedAddress").val();
            console.log(businessAddress);
            $("#businessAddress").val(businessAddress);
        });

        //监听提交
        form.on('submit(ok)', function(data){

            var categoryImg = $("#businessImage").val();
            if(categoryImg == ""){
                layer.alert("你还未上传图片哦");
                return false;
            }

            var data = JSON.stringify(data.field);
            data = data.replace('"file":"",','');
            console.log(data);

            var xmlhttp;
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function()
            {
                if (xmlhttp.readyState==1 || xmlhttp.readyState==2 || xmlhttp.readyState==3 )
                {
                    layer.load();
                }

                if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    layer.closeAll('loading');
                    //var objs = eval(xmlhttp.responseText);
                    var obj = JSON.parse(xmlhttp.responseText);
                    return layer.msg('添加成功');
                }
            }
            xmlhttp.open("POST","/BusinessAction/save",true);
            xmlhttp.setRequestHeader("Content-type","application/json");
            xmlhttp.send(data);

            return false;
        });
    });

    //文件上传
    layui.use('upload', function(){
        var $ = layui.jquery
                ,upload = layui.upload;
        //图片上传
        var uploadInst = upload.render({
            elem: '#up_file'
            ,url: '/upload/'
            ,before: function(obj){
                layer.load(); //上传loading
            }
            ,done: function(res){
                layer.closeAll('loading');
                if(res.success == true){
                    $('#images').html('<label class="layui-form-label">图片预览</label><img src="'+ res.msg +'" class="layui-upload-img" width="120px" height="120px"' +
                            'style="margin: 10px 10px 10px 0;">');

                    return layer.msg('上传成功');
                }
            }
            ,error: function(){
                layer.closeAll('loading');
                //演示失败状态，并实现重传
                var demoText = $('#demoText');
                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function(){
                    uploadInst.upload();
                });
            }
        });
    });
</script>

</body>
</html>