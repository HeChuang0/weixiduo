<!DOCTYPE html>
<html lang="en"/>
<head>
    <meta charset="UTF-8"/>
    <title>商户列表</title>
    <link rel="stylesheet" type="text/css" href="/js/css/layui.css"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
</head>
<body>

<div id="productTable" lay-filter="product-filter">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>商户列表</legend>
    </fieldset>
</div>
<script type="text/html" id="barDemo">
    <#--<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">详</a>-->
    <a class="layui-btn layui-btn-xs" lay-event="edit">改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删</a>
</script>
<script id="businessAddress" type="text/html">
    {{ d.businessAddress.replace(/[ ]/g,"") }}
</script>
<script src="/js/layui.all.js"></script>
<script>
    layui.use('table', function () {
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#productTable'
            ,id: 'id'
            , height: 'full'
            , url: '/BusinessAction/businessList' //数据接口
            , page: true //开启分页
            ,groups: 5
            ,limit: 8
            ,limits: [8,6,4]
            , request: {
                pageName: 'page' //页码的参数名称，默认：page
                , limitName: 'size' //每页数据量的参数名，默认：limit
            }
            , response: {
                statusName: 'success' //数据状态的字段名称，默认：code
                , statusCode: 200 //成功的状态码，默认：0
                , msgName: 'msg' //状态信息的字段名称，默认：msg
                , countName: 'total' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , cols: [[ //表头
                {field: 'businessName', title: '商户名'}
                ,{field: 'businessImage', title: '商户图片',width:200,templet:'<div><img src="{{ d.businessImage }}" width="30" height="30"/></div>',align: 'center'}
                , {field: 'businessAddress', title: '商户地址',templet: '#businessAddress'}
                , {field: '', title: '操作', toolbar: '#barDemo',align: 'center'}
            ]]
        });
        table.on('tool(product-filter)', function(obj){
            var data = obj.data;
            if(obj.event === 'detail'){
                //layer.msg('ID：'+ data.id + ' 的查看操作');
                detail(obj);
            } else if(obj.event === 'del'){
                layer.confirm('真的删除这个商户么，删除将无法恢复', function(index){
                    $.get({
                        url: '/BusinessAction/delete?id=' + obj.data.id
                    });
                    layer.msg('删除成功');
                    obj.del();
                    layer.close(index);
                });
            } else if(obj.event === 'edit'){
                console.log(data);
                //layer.alert('编辑行：<br>'+ JSON.stringify(data))
                var index = layer.open({
                    type: 2
                    , anim: 0
                    , title: '详情'
                    , content: '/admin/editBusiness?id=' + data.id
                    , success: function (layero, index) {
                        /*
                        var body = layer.getChildFrame('body', index);
                        body.find('#businessId').val(data.id);
                        body.find('#businessName').val(data.businessName);
                        body.find('#images').html('<label class="layui-form-label">图片预览</label><img src="'+ data.businessImage +'" class="layui-upload-img" width="120px" height="120px"' +
                                'style="margin: 10px 10px 10px 0;">');
                        body.find('#businessAddress').val(data.businessAddress);
                        */
                    }
                });
                layer.full(index);
            }
        });
    });
    function detail(obj) {
        var data = obj.data;
        var index = layer.open({
            type: 2
            , anim: 0
            , title: '详情'
            , content: '/admin/orderDetail/' + data.orderid
        });
        // layer.msg('点击的ORDERID: ' + data.orderid);
        layer.full(index);
    }

</script>

</body>
</html>