<!DOCTYPE html>
<html lang="en"/>
<head>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="/js/css/layui.css"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <meta charset="UTF-8"/>
    <title>维喜多商家后台</title>
    <style>
    </style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo" style="color: #5FB878;">维喜多果园餐厅商城后台</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <img src="/images/weixiduo.png" class="layui-nav-img">
                    亲爱的${(name)!''}，请不要工作太晚哦！
            </li>
            <li class="layui-nav-item"><a href="/LoginAction/logout">退出系统</a></li>
            <span class="layui-nav-bar" style="left: 131.891px; top: 55px; width: 0px; opacity: 0;"></span></ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
         <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a href="javascript:;">订单管理<span class="layui-nav-more"></span></a>
                    <dl class="layui-nav-child">
                        <dd class="layui-this"><a href="javascript:;" data-url="/admin/order">订单列表</a></dd>
                        <dd><a href="javascript:;" data-url="/admin/orderLately">最新订单</a></dd>
                        <dd><a href="javascript:;" data-url="/admin/orderRefund">退款订单</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">商品管理<span class="layui-nav-more"></span></a>
                    <dl class="layui-nav-child">
                        <dd class=""><a href="javascript:;" data-url="/admin/product">商品列表</a></dd>
                        <dd><a href="javascript:;" data-url="/admin/addProduct">添加商品</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">商户管理<span class="layui-nav-more"></span></a>
                    <dl class="layui-nav-child">
                        <dd class=""><a href="javascript:;" data-url="/admin/business">商户列表</a></dd>
                        <dd><a href="javascript:;" data-url="/admin/addBusiness">添加商户</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item layui-nav-itemed">
                    <a href="javascript:;">商品主分类管理<span class="layui-nav-more"></span></a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;" data-url="/admin/productCategory">分类列表</a></dd>
                        <dd><a href="javascript:;" data-url="/admin/addProductCategory">添加主分类</a></dd>
                        <dd><a href="javascript:;" data-url="/admin/addProductSubCategory">添加子分类</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item layui-nav-itemed">
                    <a href="javascript:;">会员管理<span class="layui-nav-more"></span></a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;" data-url="/admin/vipMember">会员列表</a></dd>
                        <#--<dd><a href="javascript:;" data-url="/admin/vipCard">会员卡列表</a></dd>-->
                        <#--<dd><a href="javascript:;" data-url="/admin/addVipMember">添加会员</a></dd>-->
                    </dl>
                </li>
                <li class="layui-nav-item layui-nav-itemed">
                    <a href="javascript:;">会员卡管理<span class="layui-nav-more"></span></a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;" data-url="/admin/vipCard">会员卡列表</a></dd>
                        <dd><a href="javascript:;" data-url="/admin/addVipCard">添加会员卡</a></dd>
                    </dl>
                </li>

                <li class="layui-nav-item layui-nav-itemed">
                    <a href="javascript:;">现金券管理<span class="layui-nav-more"></span></a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;" data-url="/admin/discountSet">现金券列表</a></dd>
                        <dd><a href="javascript:;" data-url="/admin/addDiscountSet">添加现金券</a></dd>
                    </dl>
                </li>
                <span class="layui-nav-bar"></span></ul>
        </div>
    </div>
    <div class="layui-body" style="height: 100%;">
        <!-- 内容主体区域 -->
            <iframe id="framecontent" src="/admin/order" width="100%" height="900px" frameborder=0 marginheight=0 marginwidth=0 scrolling=yes></iframe>
    </div>

</div>
<script src="/js/layui.all.js"></script>
<script>
    $(".layui-nav-child").children("dd").children("a").on('click', function() {
        $("#framecontent").attr('src', $(this).attr('data-url'));
    });
</script>


<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">
                    提醒
                </h4>
            </div>
            <div class="modal-body">
                你有新的订单
            </div>
            <div class="modal-footer">
                <button onclick="javascript:document.getElementById('notice').pause()" type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button onclick="location.reload()" type="button" class="btn btn-primary">查看新的订单</button>
            </div>
        </div>
    </div>
</div>

</body>

<#--播放音乐-->
<audio id="notice" loop="loop">
    <source src="/mp3/ordermusic.mp3" type="audio/mpeg" />
</audio>
<script>
    var websocket = null;
    if('WebSocket' in window) {
        websocket = new WebSocket('ws://119.23.73.197:8888/webSocket');
    }else {
        alert('该浏览器不支持websocket!');
    }

    websocket.onopen = function (event) {
        console.log('建立连接');
    }

    websocket.onclose = function (event) {
        console.log('连接关闭');
    }

    websocket.onmessage = function (event) {
        console.log('订单来了:' + event.data);
        layer.confirm('您有新的订单，请注意查收!', {
            btn: ['确认'] //按钮
        }, function(){
            location.reload();
        });

        document.getElementById('notice').play();
    }

    websocket.onerror = function () {
        alert('websocket通信发生错误！');
    }

    window.onbeforeunload = function () {
        websocket.close();
    }


</script>
</html>