<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<html lang="en"/>
<head>
    <meta charset="UTF-8"/>
    <title>会员列表</title>
    <link rel="stylesheet" type="text/css" href="/js/css/layui.css"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
</head>
<body>
<script type="text/html" id="ismarriedTpl">
    {{#  if(d.ismarried === 0){ }}
    <span>未婚</span>
    {{#  } else if(d.ismarried === 1) { }}
    <span>已婚</span>
    {{#  } }}
</script>
<script type="text/html" id="haschildTpl">
    {{#  if(d.haschild === 0){ }}
    <span>没有</span>
    {{#  } else if(d.haschild === 1) { }}
    <span>有</span>
    {{#  } }}
</script>
<div id="vipMemberTable">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>会员列表</legend>
    </fieldset>
</div>
<script src="/js/layui.all.js"></script>
<script>
    layui.use('table', function(){
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#vipMemberTable'
            ,height: 'full'
            ,url: '/VipMemberAction/get' //数据接口
            ,page: true //开启分页
            ,groups: 5
            ,limit: 8
            ,limits: [8,6,4]
            ,request: {
                pageName: 'page' //页码的参数名称，默认：page
                ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
            }
            ,response: {
                statusName: 'success' //数据状态的字段名称，默认：code
                ,statusCode: 200 //成功的状态码，默认：0
                ,msgName: 'msg' //状态信息的字段名称，默认：msg
                ,countName: 'total' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,cols: [[ //表头
                {field: 'id', title: 'ID', width:80,  fixed: 'left'}
                ,{field: 'vipCardId', title: 'VIP 号'}
                ,{field: 'name', title: '会员名'}
                ,{field: 'sex', title: '性别', width:80, }
                ,{field: 'address', title: '家庭住址'}
                ,{field: 'realName', title: '真实姓名'}
                ,{field: 'birthday', title: '出生年月'}
                ,{field: 'ismarried', title: '是否已婚', templet: '#ismarriedTpl', align: 'center'}
                ,{field: 'haschild', title: '是否有小孩', templet: '#haschildTpl', align: 'center'}
                ,{field: 'phone', title: '电话'}
                ,{field: 'updateTime', title: '更新时间'}
                ,{field: 'createTime', title: '创建时间'}
            ]]
        });
    });
</script>
</body>
</html>