<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>维喜多官方管理系统</title>
	<script src="../js/jquery-3.2.1.min.js"></script>
	<script src="../js/jquery-ui.min.js"></script>
	<link rel="stylesheet" type="text/css" href="../js/jquery-ui.min.css">
	<link rel="stylesheet" type="text/css" href="../css/login.css">
    <link rel="stylesheet" type="text/css" href="../js/css/layui.css"/>
</head>
<body>
	<div class="main">
		<div class="nav">
		<div class="font t_c">W</div>
		<div class="font t_r">EI</div>
		<div class="font t_e">X</div>
		<div class="font t_a">I</div>
		<div class="font t_t">O</div>
		<div class="font t_d">D</div>
		<div class="font t_m">U</div>
			<div class="nav_left">
				<a>维喜多果园餐厅</a>
			</div>
		</div>
		<div class="middle">
			<div class="m_main m1">
			<form class="form1" action="/LoginAction/login" method="post">
				<div class="title">欢迎登录维喜多商家后台</div>
				<input type="text" class="user_message" name="name">
				<label class="label l4">请输入用户名</label>
					<div class="icon user_icon"></div>
				<input type="password" class="user_message" name="password">
				<label class="label l5">密码不能为空</label>
					<div class="icon pass_icon"></div>
				<div class="m_font f1" id="forget">忘记密码？</div>
				<div class="m_font f2">微信扫码登录</div>
				<input type="submit"  value="登&nbsp&nbsp录" class="order submit">
				<input type="reset" class="order zhuce" value="重&nbsp置">
			</div>
			</form>
			</div>
		</div>
	</div>
</body>
</html>

<script src="/js/layui.all.js"></script>
<script type="text/javascript">
    $("#forget").on('click', function() {
        layer.confirm('忘记密码请联系后台开发人员为您修改密码');
    });
    $(function(){
        $(".middle").show("explode",1000);
        $(".m1 .user_message").blur(function(){
            if($(this).val() == ""){
                var a = $(this).index();
                if(a > 2){
                    a=a/2;
                }
                //$(".m1 .submit").attr("disabled","disabled");
                $(".label").eq(a-1).show("bounce",1000)
            }else{
                //$(".m1 .submit").removeAttr("disabled","disabled");
                $(".label").eq(a-1).hide("bounce",1000)
            }
        })

        $(".m1 .user_message").focus(function(){
            var a = $(this).index();
            if(a > 2){
                a=a/2;
            }
            $(".label").eq(a-1).hide("bounce",1000)
        })
    })

</script>