<!DOCTYPE html>
<html lang="en"/>
<head>
    <meta charset="UTF-8"/>
    <title>主类目列表</title>
    <link rel="stylesheet" type="text/css" href="/js/css/layui.css"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
</head>
<body>
<div id="categoryTable" lay-filter="category-filter">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>主类目列表</legend>
    </fieldset>

</div>
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">子分类</a>
    <a class="layui-btn layui-btn-xs" lay-event="edit">修改</a>
    <#--<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删</a>-->
</script>
<script src="/js/layui.all.js"></script>
<script>
    layui.use('table', function(){
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#categoryTable'
            ,height: 'full'
            ,url: '/ProductCategoryAction/get' //数据接口
            ,page: true //开启分页
            ,groups: 5
            ,limit: 8
            ,limits: [8,6,4]
            ,request: {
                pageName: 'page' //页码的参数名称，默认：page
                ,limitName: 'size' //每页数据量的参数名，默认：limit
            }
            ,response: {
                statusName: 'success' //数据状态的字段名称，默认：code
                ,statusCode: 200 //成功的状态码，默认：0
                ,msgName: 'msg' //状态信息的字段名称，默认：msg
                ,countName: 'total' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,cols: [[ //表头
                {field: 'id', title: 'ID', width:80,  fixed: 'left'}
                ,{field: 'categoryName', title: '类目名'}
                ,{field: 'updateTime', title: '更新时间'}
                ,{field: 'createTime', title: '创建时间'}
                , {field: '', title: '操作', toolbar: '#barDemo',align: 'center'}
            ]]
        });
        table.on('tool(category-filter)', function(obj){
            if(obj.event === 'detail'){
                var data = obj.data;
                var index = layer.open({
                    type: 2
                    , anim: 0
                    , title: '详情'
                    , content: '/admin/productSubCategory/' + data.id
                    , success: function (layero, index) {
                        // var body = layer.getChildFrame('body', index);
                        // var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        // console.log(body.html()); //得到iframe页的body内容
                        // body.find('#categoryName').val(data.categoryName);
                        // body.find('#categotyId').val(data.id);
                    }
                });
                layer.full(index);
            } else if(obj.event === 'del'){
                layer.confirm('真的删除这个类目么，删除将无法恢复', function(index){
                    $.post({
                        url: '/ProductCategoryAction/delete?id=' + obj.data.id
                        , success: function (data) {
                            layer.msg(data.msg);
                            obj.del();
                            layer.close(index);
                        }

                    });

                });
            } else if(obj.event === 'edit'){
                // layer.alert('编辑行：<br>'+ JSON.stringify(data));
                // layer.msg(data.data.id);
                var data = obj.data;
                var index = layer.open({
                    type: 2
                    , anim: 0
                    , title: '详情'
                    , content: '/admin/EditProductCategory/' + data.id
                    , success: function (layero, index) {
                        var body = layer.getChildFrame('body', index);
                        var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        console.log(body.html()); //得到iframe页的body内容
                        body.find('#categoryName').val(data.categoryName);
                        body.find('#categotyId').val(data.id);
                    }
                });
                layer.full(index);
            }
        });
    });
</script>
</body>
</html>