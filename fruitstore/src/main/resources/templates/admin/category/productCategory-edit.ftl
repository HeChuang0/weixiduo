<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="utf-8"/>
    <title>商品主类目修改</title>
    <meta name="renderer" content="webkit"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" href="/js/css/layui.css"  media="all"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
</head>
<body style="padding-right: 25px">
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>商品主类目修改</legend>
</fieldset>

<form class="layui-form" action="POST">

    <div class="layui-form-item">
        <label class="layui-form-label">类目名称</label>
        <div class="layui-inline">
            <input type="text" id="categoryName" name="categoryName" lay-verify="required" placeholder="请输入商品类目名称" autocomplete="off" class="layui-input">
            <input type="hidden" name="id" id="categotyId" value="" />
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="ok">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="/js/layui.all.js" charset="utf-8"></script>
<script>
    layui.use(['form'], function(){
        var form = layui.form
                ,layer = layui.layer;

        //监听提交
        form.on('submit(ok)', function(data){

            var data = JSON.stringify(data.field);
            //data = data.replace('"id":"",','');

            var xmlhttp;
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }

            xmlhttp.onreadystatechange=function()
            {
                if (xmlhttp.readyState==1 || xmlhttp.readyState==2 || xmlhttp.readyState==3 )
                {
                    layer.load();
                }

                if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    layer.closeAll('loading');
                    //var objs = eval(xmlhttp.responseText);
                    var obj = JSON.parse(xmlhttp.responseText);
                    return layer.msg(obj.msg);
                }
            }

            xmlhttp.open("POST","/ProductCategoryAction/update",true);
            xmlhttp.setRequestHeader("Content-type","application/json");
            xmlhttp.send(data);

            return false;
        });

    });

</script>

</body>
</html>