<!DOCTYPE html>
<html lang="en"/>
<head>
    <meta charset="UTF-8"/>
    <title>现金券列表</title>
    <link rel="stylesheet" type="text/css" href="/js/css/layui.css"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
</head>
<body>

<div id="productTable" lay-filter="product-filter">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>现金券列表</legend>
    </fieldset>
</div>
<script type="text/html" id="statusTpl">
    {{#  if(d.type == 0){ }}
    <span class="layui-badge layui-bg-red">新人红包</span>
    {{#  } else if(d.type == 1) { }}
    <span class="layui-badge layui-bg-red">普通红包</span>
    {{#  } else if(d.type == 2) { }}
    <span class="layui-badge layui-bg-red">转盘红包</span>
    {{#  } else if(d.type == 3) { }}
    <span class="layui-badge layui-bg-red">积分红包</span>
    {{#  } else if(d.type == 4) { }}
    <span class="layui-badge layui-bg-red">商城红包</span>
    {{#  } }}
</script>
<script type="text/html" id="barDemo">
    {{#  var now = Number(new Date()) ;var endTime = Number(new Date(d.endTime)) }}
    {{# if(endTime > now){ }}
    <a class="layui-btn layui-btn-xs" lay-event="edit">改</a>
    {{# } else{ }}
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删</a>
    {{# }  }}
    <#--<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">详</a>-->
</script>
<script src="/js/layui.all.js"></script>
<script>
    layui.use('table', function () {
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#productTable'
            , height: 'full'
            , url: '/DiscountSetAction/list' //数据接口
            , page: true //开启分页
            ,groups: 5
            ,limit: 8
            ,limits: [8,6,4]
            , request: {
                pageName: 'page' //页码的参数名称，默认：page
                , limitName: 'size' //每页数据量的参数名，默认：limits
            }
            , response: {
                statusName: 'success' //数据状态的字段名称，默认：code
                , statusCode: 200 //成功的状态码，默认：0
                , msgName: 'msg' //状态信息的字段名称，默认：msg
                , countName: 'total' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , cols: [[ //表头
                {field: 'id', title: 'ID', width: 80, sort: true, fixed: 'left'}
                , {field: 'discountName', title: '现金券名'}
                , {field: 'cashCoupon', title: '价值'}
                , {field: 'fullCut', title: '满足条件'}
                , {field: 'type', title: '类型', templet: '#statusTpl', align: 'center'}
                , {field: 'startTime', title: '有效时间'}
                , {field: 'endTime', title: '过期时间'}
                , {field: '', title: '操作', toolbar: '#barDemo',align: 'center'}
            ]]
        });
        table.on('tool(product-filter)', function(obj){
            var data = obj.data;
            if(obj.event === 'detail'){
                //layer.msg('ID：'+ data.id + ' 的查看操作');
                detail(obj);
            } else if(obj.event === 'del'){
                layer.confirm('真的删除这个现金券么，删除将无法恢复', function(index){
                    $.get({
                        url: '/DiscountSetAction/delete?id=' + obj.data.id
                    });
                    layer.msg('删除成功');
                    obj.del();
                    layer.close(index);
                });
            } else if(obj.event === 'edit'){
                console.log(data);
                //layer.alert('编辑行：<br>'+ JSON.stringify(data))
                var index = layer.open({
                    type: 2
                    , anim: 0
                    , title: '详情'
                    , content: '/admin/editDiscountSet/' + data.id
                    , success: function (layero, index) {
                        var body = layer.getChildFrame('body', index);
                        var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        //console.log(body.html()); //得到iframe页的body内容
                        body.find('#id').val(data.id);
                        body.find('#discountName').val(data.discountName);
                        body.find('#cashCoupon').val(data.cashCoupon);
                        body.find('#fullCut').val(data.fullCut);
                        body.find('#type').val(data.type);
                        body.find('#startTime').val(data.startTime);
                        body.find('#endTime').val(data.endTime);
                        //body.find('#type option[value="'+data.type+'"]').attr("selected",true);
                    }
                });
                layer.full(index);
            }
        });
    });
    function detail(obj) {
        var data = obj.data;
        var index = layer.open({
            type: 2
            , anim: 0
            , title: '详情'
            , content: '/admin/orderDetail/' + data.orderid
        });
        // layer.msg('点击的ORDERID: ' + data.orderid);
        layer.full(index);
    }

</script>

</body>
</html>