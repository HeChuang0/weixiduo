<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="utf-8"/>
    <title>现金券添加</title>
    <meta name="renderer" content="webkit"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" href="/js/css/layui.css"  media="all"/>
</head>
<body style="padding-right: 25px">
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>现金券添加</legend>
</fieldset>

<form class="layui-form" action="">

    <div class="layui-form-item">
        <label class="layui-form-label">现金券名称</label>
        <div class="layui-inline">
            <input type="text" name="discountName" lay-verify="required" autocomplete="off" placeholder="请输入现金券名称" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">现金券价值</label>
        <div class="layui-inline">
            <input type="number" name="cashCoupon" lay-verify="required" placeholder="请输入现金券价值" autocomplete="off" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">满足条件</label>
        <div class="layui-inline">
            <input type="number" name="fullCut" lay-verify="required" placeholder="请输入现金券满足条件" autocomplete="off" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">开始时间</label>
        <div class="layui-input-inline">
            <input type="text" class="layui-input" lay-verify="required" name="startTime" id="test5" placeholder="请选择开始时间">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">过期时间</label>
        <div class="layui-input-inline">
            <input type="text" class="layui-input" lay-verify="required" name="endTime" id="test6" placeholder="请选择过期时间">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">现金券类型</label>
        <div class="layui-inline">
            <select name="type" lay-filter="aihao">
                <option value="0">新人红包</option>
                <option value="1">普通红包</option>
                <option value="2">转盘红包</option>
                <option value="3">积分红包</option>
                <option value="4">商城红包</option>
            </select>
        </div>
    </div>

    <#--<div class="layui-form-item layui-form-text">-->
        <#--<label class="layui-form-label">商品描述</label>-->
        <#--<div class="layui-inline">-->
            <#--<textarea placeholder="请输入商品描述" class="layui-textarea" lay-verify="required" name="productDescription"></textarea>-->
        <#--</div>-->
    <#--</div>-->

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="ok">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="/js/layui.all.js" charset="utf-8"></script>
<script>

    layui.use(['form','laydate'], function(){
        var form = layui.form
                ,layer = layui.layer;

        var laydate = layui.laydate;
        //时间选择器
        laydate.render({
            elem: '#test5'
            ,type: 'datetime'
        });

        laydate.render({
            elem: '#test6'
            ,type: 'datetime'
        });

        //监听提交
        form.on('submit(ok)', function(data){

            var data = JSON.stringify(data.field);

            //layer.alert(data);

            var xmlhttp;
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }

            xmlhttp.onreadystatechange=function()
            {
                if (xmlhttp.readyState==1 || xmlhttp.readyState==2 || xmlhttp.readyState==3 )
                {
                    layer.load();
                }

                if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    layer.closeAll('loading');
                    //var objs = eval(xmlhttp.responseText);
                    var obj = JSON.parse(xmlhttp.responseText);
                    return layer.msg(obj.msg);
                }
            }

            xmlhttp.open("POST","/DiscountSetAction/add",true);
            xmlhttp.setRequestHeader("Content-type","application/json");
            xmlhttp.send(data);

            return false;
        });

    });

</script>

</body>
</html>