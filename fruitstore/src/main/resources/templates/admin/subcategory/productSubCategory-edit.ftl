<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="utf-8"/>
    <title>商品子类目修改</title>
    <meta name="renderer" content="webkit"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" href="/js/css/layui.css"  media="all"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
</head>
<body style="padding-right: 25px">
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>商品子类目修改</legend>
</fieldset>

<form class="layui-form" action="POST">

    <div class="layui-form-item">
        <label class="layui-form-label">子类目名称</label>
        <div class="layui-inline">
            <input type="text" name="subcategoryName" id="subcategoryName" lay-verify="required" autocomplete="off" placeholder="请输入商品子类目名称" class="layui-input">
        </div>
    </div>

    <input type="hidden" name="id" id="id"/>

    <input type="hidden" name="categoryImg" id="categoryImg"/>

    <div class="layui-form-item">
        <label class="layui-form-label">子类目图片</label>
        <div class="layui-upload">
            <button type="button" class="layui-btn" id="up_file">
                <i class="layui-icon">&#xe67c;</i>上传图片
            </button>
        </div>
    </div>

    <div class="layui-input-inline" id="images" style="height: 140px;overflow: hidden;">
        <label class="layui-form-label">图片预览</label>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label"></label>
        <p id="demoText"></p>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">选择父类目</label>
        <div class="layui-input-inline">
            <select name="maincategoryId" id="maincategoryId" lay-filter="father">

            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="ok" id="submit">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script src="/js/layui.all.js" charset="utf-8"></script>
<script>

    layui.use(['form'], function(){

        var form = layui.form
                ,layer = layui.layer;

        var url = window.location.search;
        var params = url.split("=");
        //console.log(params[1]);

        $.ajax({
            type: "GET",
            url: "/ProductSubcategoryAction/findOneById?id=" + params[1],
            success: function (data) {
                //console.log(data);
                var mData = data;
                $('#subcategoryName').val(mData.data[0].subcategoryName);
                $('#id').val(mData.data[0].id);
                $('#images').html('<label class="layui-form-label">图片预览</label><img src="'+ mData.data[0].categoryImg +'" class="layui-upload-img" width="120px" height="120px"' +
                        'style="margin: 10px 10px 10px 0;">');

                /*初始化父类目选择框**/
                $.ajax({
                    type:"GET",
                    url:"/ProductCategoryAction/get",
                    success:function (data) {
                        var str = "";
                        for(var i = 0;i < data.data.length ; i++){
                            str += '<option value="'+data.data[i].id+ '">'+data.data[i].categoryName+'</option>';
                        }
                        $('#maincategoryId').html(str);
                        $("#maincategoryId").val(mData.data[0].maincategoryId);
                        form.render();
                        //console.log($('#maincategoryId').val());
                    }
                });

            }
        });

        //点击提交按钮时，给隐藏字段赋值
        $("#submit").click(function () {
            var images = "";
            $(".layui-upload-img").each(function () {
                images = $(this)[0].src;
            });
            $("#categoryImg").val(images);
        });

        //监听提交
        form.on('submit(ok)', function(data){

            var categoryImg = $("#categoryImg").val();
            if(categoryImg == ""){
                layer.alert("你还未上传图片哦");
                return false;
            }

            var data = JSON.stringify(data.field);
            data = data.replace('"file":"",','');
            console.log(data);

            var xmlhttp;
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function()
            {
                if (xmlhttp.readyState==1 || xmlhttp.readyState==2 || xmlhttp.readyState==3 )
                {
                    layer.load();
                }

                if (xmlhttp.readyState==4 && xmlhttp.status==200)
                {
                    layer.closeAll('loading');
                    //var objs = eval(xmlhttp.responseText);
                    var obj = JSON.parse(xmlhttp.responseText);
                    return layer.msg("修改成功");
                }
            }
            xmlhttp.open("POST","/ProductSubcategoryAction/save",true);
            xmlhttp.setRequestHeader("Content-type","application/json");
            xmlhttp.send(data);

            return false;
        });
    });

    //文件上传
    layui.use('upload', function(){
        var $ = layui.jquery
                ,upload = layui.upload;
        //图片上传
        var uploadInst = upload.render({
            elem: '#up_file'
            ,url: '/upload/'
            ,before: function(obj){
                layer.load(); //上传loading
            }
            ,done: function(res){
                layer.closeAll('loading');
                if(res.success == true){
                    $('#images').html('<label class="layui-form-label">图片预览</label><img src="'+ res.msg +'" class="layui-upload-img" width="120px" height="120px"' +
                            'style="margin: 10px 10px 10px 0;">');

                    return layer.msg('上传成功');
                }
            }
            ,error: function(){
                layer.closeAll('loading');
                //演示失败状态，并实现重传
                var demoText = $('#demoText');
                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function(){
                    uploadInst.upload();
                });
            }
        });
    });
</script>

</body>
</html>