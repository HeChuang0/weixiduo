<!DOCTYPE html>
<html lang="en"/>
<head>
    <meta charset="UTF-8"/>
    <title>子类目列表</title>
    <link rel="stylesheet" type="text/css" href="/js/css/layui.css"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
</head>
<body>
<div id="categoryTable" lay-filter="category-filter">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>子类目列表</legend>
    </fieldset>

</div>
<script type="text/html" id="barDemo">
    <#--<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">详</a>-->
    <a class="layui-btn layui-btn-xs" lay-event="edit">改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删</a>
</script>
<script src="/js/layui.all.js"></script>
<script th:inline="javascript">
    layui.use('table', function(){
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#categoryTable'
            ,id: 'id'
            ,height: 'full'
            ,url: '/ProductSubcategoryAction/findBymaincategoryId?maincategoryId=' + [[${id}]] //数据接口
            ,page: true //开启分页
            ,groups: 5
            ,limit: 8
            ,limits: [8,6,4]
            ,request: {
                pageName: 'page' //页码的参数名称，默认：page
                ,limitName: 'size' //每页数据量的参数名，默认：limit
            }
            ,response: {
                statusName: 'success' //数据状态的字段名称，默认：code
                ,statusCode: 200 //成功的状态码，默认：0
                ,msgName: 'msg' //状态信息的字段名称，默认：msg
                ,countName: 'total' //数据总数的字段名称，默认：count
                ,dataName: 'data' //数据列表的字段名称，默认：data
            }
            ,cols: [[ //表头
                {field: 'subcategoryName', title: '类目名',width:200,align: 'center'}
                ,{field: 'categoryImg', title: '类目图片',width:200,templet:'<div><img src="{{ d.categoryImg }}" width="30" height="30"/></div>',align: 'center'}
                ,{field: 'updateTime', title: '更新时间',width:200}
                ,{field: 'createTime', title: '创建时间',width:200}
                , {field: '', title: '操作', toolbar: '#barDemo',align: 'center',width:200}
            ]]
        });
        table.on('tool(category-filter)', function(obj){
            var data = obj.data;
            if(obj.event === 'detail'){
                //layer.msg('ID：'+ data.id + ' 的查看操作');
                detail(obj);
            } else if(obj.event === 'del'){
                layer.confirm('真的删除这个类目么，删除将无法恢复', function(index){

                    $.get({
                        url: '/ProductSubcategoryAction/delete?id=' + obj.data.id
                        , success: function (data) {
                            layer.msg(data.msg);
                            obj.del();
                            layer.close(index);
                        }

                    });
                    obj.del();
                    layer.close(index);

                });
            } else if(obj.event === 'edit'){
                // layer.alert('编辑行：<br>'+ JSON.stringify(data));
                // layer.msg(data.data.id);
                var data = obj.data;
                var index = layer.open({
                    type: 2
                    , anim: 0
                    , title: '详情'
                    , content: '/admin/EditProductSubCategory?id=' + data.id
                    , success: function (layero, index) {
                        /*
                        var body = layer.getChildFrame('body', index);
                        //console.log(body.html()); //得到iframe页的body内容
                        body.find('#subcategoryName').val(data.subcategoryName);
                        body.find('#id').val(data.id);
                        body.find('#images').html('<label class="layui-form-label">图片预览</label><img src="'+ data.categoryImg +'" class="layui-upload-img" width="120px" height="120px"' +
                                'style="margin: 10px 10px 10px 0;">');
                        body.find("#maincategoryId").val(data.maincategoryId);
                        */
                    }
                });
                layer.full(index);
            }
        });
    });
</script>
</body>
</html>