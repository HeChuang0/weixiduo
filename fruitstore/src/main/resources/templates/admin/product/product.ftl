<!DOCTYPE html>
<html lang="en"/>
<head>
    <meta charset="UTF-8"/>
    <title>商品列表</title>
    <link rel="stylesheet" type="text/css" href="/js/css/layui.css"/>
    <script src="/js/jquery-3.2.1.min.js"></script>
</head>
<body>

<div id="productTable" lay-filter="product-filter">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>商品列表</legend>
    </fieldset>
</div>
<script type="text/html" id="statusTpl">
    {{#  if(d.productStatus == 1){ }}
    <span class="layui-badge layui-bg-gray">下架</span>
    {{#  } else if(d.productStatus == 0) { }}
    <span class="layui-badge layui-bg-red">上架</span>
    {{#  } }}
</script>
<script id="icons" type="text/html">
    <div id="images">
        {{# var icons = d.productIcon.split(";"); }}
        {{# for(var i = 0;i < icons.length;i++){  }}
            <img src="{{ icons[i] }}" width="30" height="30" style="margin: 0px 2px 0px 0px;"/>
        {{# } }}
    </div>
</script>
<script id="specification" type="text/html">
    <span>{{ d.specification.replace(/[ ]/g,"") }}</span>
</script>
<script type="text/html" id="barDemo">
    <#--<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">详</a>-->
    <a class="layui-btn layui-btn-xs" lay-event="edit">改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删</a>
</script>
<script src="/js/layui.all.js"></script>
<script>
    layui.use('table', function () {
        var table = layui.table;
        //第一个实例
        table.render({
            elem: '#productTable'
            ,id: 'id'
            , height: 'full'
            , url: '/ProductInfoAction/get' //数据接口
            , page: true //开启分页
            ,groups: 5
            ,limit: 8
            ,limits: [8,6,4]
            , request: {
                pageName: 'page' //页码的参数名称，默认：page
                , limitName: 'pageSize' //每页数据量的参数名，默认：limit
            }
            , response: {
                statusName: 'success' //数据状态的字段名称，默认：code
                , statusCode: 200 //成功的状态码，默认：0
                , msgName: 'msg' //状态信息的字段名称，默认：msg
                , countName: 'total' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , cols: [[ //表头
                 {field: 'productName', title: '商品名',width: 120}
                , {field: 'productPrice', title: '单价',width: 80}
                , {field: 'specification', title: '商品规格',templet: '#specification',width: 80}
                , {field: 'productStock', title: '库存'}
                , {field: 'productIcon', title: '商品小图',templet: '#icons', align: 'center',width: 160}
                , {field: 'productStatus', title: '状态', templet: '#statusTpl', align: 'center',width: 80}
                , {field: 'productDescription', title: '商品描述'}
                , {field: 'salesVolume', title: '销量',width: 80}
                , {field: 'createTime', title: '创建时间'}
                , {field: '', title: '操作', toolbar: '#barDemo',align: 'center',width: 100}
            ]]
        });
        table.on('tool(product-filter)', function(obj){
            var data = obj.data;
            if(obj.event === 'detail'){
                //layer.msg('ID：'+ data.id + ' 的查看操作');
                detail(obj);
            } else if(obj.event === 'del'){
                layer.confirm('真的删除这个商品么，删除将无法恢复', function(index){
                    $.post({
                        url: '/ProductInfoAction/delete?id=' + obj.data.id
                    });
                    layer.msg('删除成功');
                    obj.del();
                    layer.close(index);
                });
            } else if(obj.event === 'edit'){
                //console.log(data);
                //layer.alert('编辑行：<br>'+ JSON.stringify(data))
                var index = layer.open({
                    type: 2
                    , anim: 0
                    , title: '详情'
                    , content: '/admin/editProduct?id='+data.id
                    , success: function (layero, index) {
                        /*
                        var body = layer.getChildFrame('body', index);
                        var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        //console.log(body.html()); //得到iframe页的body内容
                        body.find('#productName').val(data.productName);
                        body.find('#productId').val(data.id);
                        body.find('#productPrice').val(data.productPrice);
                        body.find('#productStock').val(data.productStock);
                        //body.find('#productStatus').val(data.productStatus);
                        //body.find('#productIcon').val(data.productIcon);
                        var specifications = data.specification.split(" ");
                        body.find('#specificationNumber').val(specifications[0]);
                        //body.find("#specificationUnit").find('option[value="'+specifications[1]+'"]').attr('selected',true);
                        body.find("#specificationUnit").val(specifications[1]);
                        //console.log(specifications[1]);
                        //console.log(body.find("#specificationUnit").val());
                        body.find('#description').val(data.productDescription);
                        var icons = data.productIcon.split(";");
                        for(var i = 0;i < icons.length;i++){
                            body.find("#images").append('<img src="'+ icons[i] +'" class="layui-upload-img" width="120px" height="120px"' +
                                    'style="margin: 10px 10px 10px 0;">');
                        }
                        body.find(".layui-upload-img").click(function () {
                            $(this).remove();
                        });
                        body.find('#categoryId').val(data.categoryId);
                        body.find('#subCategoryId').val(data.subCategoryId);
                        */
                    }
                });
                layer.full(index);
            }
        });
    });
    function detail(obj) {
        var data = obj.data;
        var index = layer.open({
            type: 2
            , anim: 0
            , title: '详情'
            , content: '/admin/orderDetail/' + data.orderid
        });
        // layer.msg('点击的ORDERID: ' + data.orderid);
        layer.full(index);
    }

</script>

</body>
</html>