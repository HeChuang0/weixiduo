<!DOCTYPE html>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html" ; charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no" />
     <link href="/wx/css/font-awesome.min.css" rel="stylesheet"/>
     <title>修改地址</title>
        <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="/wx/css/style.css"/>
        <script src="/wx/js/jquery.min.js"></script>
        <script src="/wx/js/shouquan.js"></script>
       
       </head>
        <body onload="getParam()" onclick="">
        <header data-am-widget="header" class="am-header am-header-default sq-head ">
                <div class="am-header-left am-header-nav">
                    <a href="javascript:history.back()" class="">
                        <i class="am-icon-chevron-left"></i>
                    </a>
                </div>
                <h1 class="title" >
                      <span>修改收货地址</span>
                </h1>
        </header>
            <div id="container" class="update_container">
                <div class="address_main">
                    <div class="line">
                    <input style="display:inline-block" placeholder="*姓名" type="text"  id="realname" value="">
                    </div>
                    <div class="line">
                  
                       <input type="text" placeholder="*手机" id="phone" value="">
                    </div>
                    <div class="line">
                     
                        <select id="s_province" name="s_province" placeholder="*省份"></select>
                        <br>
                    </div>
                    <div class="line">
                        <select id="s_city" name="s_city" ></select>
                        <br>
                      
                    </div>
                    <div class="line">
                   <select id="s_county" name="s_county"></select>
                     </div>
                    <script type="text/javascript" src="/wx/js/area.js"></script>
                    <script type="text/javascript">_init_area();</script>
                    <div class="line">
                            <input type="text" placeholder="*详细地址"  id="address" value="">
                      </div>
                  
                </div>

                <div class="address_sub1" id="doc-confirm-toggle" >确认</div>
                <div class="am-modal-t"></div>
                <div class="address_sub2" ><a href="tureorder.html">取消</a></div>
            </div>
          
    </body>
    <script src="/wx/js/jquery-1.10.2.min.js" type="text/javascript"></SCRIPT>
    <script src="/wx/js/amazeui.min.js"></script>
    <script src="/wx/js/address.js"></script>
    
</html>