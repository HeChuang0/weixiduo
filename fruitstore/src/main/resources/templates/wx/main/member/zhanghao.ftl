<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <script src="http://code.jquery.com/jquery-2.0.0.min.js" integrity="sha256-1IKHGl6UjLSIT6CXLqmKgavKBXtr0/jJlaGMEkh+dhw="
        crossorigin="anonymous">
        </script>
    <script type="text/javascript" src="/wx/js/shouquan.js"></script>
    <link href="/wx/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="/wx/css/css.css" rel="stylesheet" />

    <script type="text/javascript" src="/wx/js/getData.js"></script>
    <script type="text/javascript" src="/wx/js/checkInformation.js"></script>
    <title>会员修改资料</title>

</head>

<body>
    <!-- 顶部 -->
    <div class="ding">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="xia">
                    <a href="vip.html">
                        <img src="/wx/images/xia.png" width="25" height="25" align="center" />
                    </a>
                </td>
                <td class="huiyuan">个人资料</td>
                <td class="home">
                    <img src="/wx/images/home.png" width="25" height="25" align="center" />
                </td>
            </tr>
        </table>
    </div>
    <!-- 头像 -->
    <div class="imgtest">
        <figure class="touxiang">
            <div id="icon">

            </div>
        </figure>
        <div class="shenfen" id="phoneAndLevel">

        </div>
    </div>

    <!-- 会员信息 -->
    <div class="area">
        <div class="main_info">
            <!-- 昵称 -->
            <div class="left">
                <span class="title">昵称：</span>
            </div>
            <div class="right">
                <input type="text" id="name" class="right-input" />
            </div>

            <!-- 姓名 -->
            <div class="left">
                <span class="title">姓名：</span>
            </div>
            <div class="right">
                <input type="text" id="realName" class="right-input" />
            </div>

            <!-- 性别 -->
            <div class="left">
                <span class="title">性别：</span>
            </div>
            <div class="right">
                <div class="right-child">
                    <input type="radio" name="gender" value="男" id="sex" checked />男
                </div>
                <div class="right-child">
                    <input type="radio" name="gender" value="女" id="sex" />女
                </div>
            </div>

            <!-- 手机号 -->
            <div class="left">
                <span class="title">手机号码：</span>
            </div>
            <div class="right">
                <input type="text" id="phone" class="right-input" />
            </div>

            <!-- 生日 -->
            <div class="left" style="height:55px;">
                <span class="title">生日：</span>
            </div>
            <div class="right" style="height:55px;">
                <div style="width:70%;float:left;line-height:45px;">
                    <input id="birthday" type="date" style="border:none;width:100%;text-overflow: ellipsis;
                    white-space: nowrap;
                    overflow: hidden;" onfocus="this.blur()" />
                </div>
                <div style="width:30%;font-size:12px;float:left;color:#999;">保存后不可修改</div>
            </div>

            <!-- 家庭地址 -->
            <div class="left">
                <span class="title">家庭地址：</span>
            </div>
            <div class="right">
                <input type="text" id="address" class="right-input" />
            </div>

            <!-- 婚姻状况 -->
            <div class="left">
                <span class="title">婚姻状况：</span>
            </div>
            <div class="right">
                <div class="right-child">
                    <input type="radio" name="marriage" value="1" />已婚
                </div>
                <div class="right-child">
                    <input type="radio" name="marriage" value="0" checked="checked"/>未婚
                </div>
            </div>

            <!-- 小孩 -->
            <div class="left">
                <span class="title">小孩：</span>
            </div>
            <div class="right">
                <div class="right-child">
                    <input type="radio" name="kid" value="1" />有
                </div>
                <div class="right-child">
                    <input type="radio" name="kid" value="0" checked="checked"/>无
                </div>
            </div>
        </div>

        <!-- 保存按钮 -->
        <div class="save">
            <button class="button" onclick="check()" id="saveData">保存</button>
        </div>
    </div>
</body>
</html>