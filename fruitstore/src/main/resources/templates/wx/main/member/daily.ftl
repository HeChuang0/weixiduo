<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <script src="/wx/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="/wx/js/shouquan.js"></script>
  <script>
    var url = 'weixiduo/member/daily';

    if (getCookie("open") == null || getCookie("open") == "") {
      authorization(url);
    }
  </script>
  <script type="text/javascript" src="/wx/js/calendar.js"></script>
  <script type="text/javascript" src="/wx/js/qiandao.js"></script>

  <link rel="stylesheet" href="/wx/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="/wx/css/style2.css"/>
  <link rel="stylesheet" href="/wx/css/qiandao.css"/>

  <title>每日签到</title>
</head>

<body>
  <div class="sign">
    <div class="sign-1">
      <img src="/wx/images/sign_head.jpg" width="100%" height="auto" />
    </div>
    <div class="calenbox">
      <div id="calendar"></div>
    </div>
    <div class="submit">
      <button class="sign-submit" id="qian" onclick="signSubmit()">签到</button>
    </div>
  </div>
</body>

</html>