<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=0"
    />
    <script type="text/javascript" src="/wx/js/shouquan.js"></script>
    <script>
        var url = 'weixiduo/member/vip';
        if (getCookie("open") == null || getCookie("open") == "") {
            authorization(url)
        }
    </script>
    <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="/wx/css/css.css" rel="stylesheet" />
    <title>会员中心</title>

</head>

<body style="background:#f0f0f0;">
    <div class="content">
        <div class="ding">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="xia">
                        <img src="/wx/images/xia.png" width="25" height="25" />
                    </td>
                    <td class="huiyuan">会员中心</td>
                    <td class="home">
                        <img src="/wx/images/home.png" width="25" height="25" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="huiyuanka">
            <img src="/wx/images/t2.jpg" width="100%" height="auto" />
        </div>
        <div class="chushi">使用时向服务员出示此卡</div>

        <div class="kuang_1">
            <a href="/weixiduo/member/tequan">
            <div class="yue">
                <div class="yue_1">会员尊享特权</div>
                <div class="jiantou">
                        <img src="/wx/images/jian.png" width="30" height="31" />
                    </div>

            </div>
            </a>
        </div>

        <div class="kuang_2">
            <a href="/weixiduo/member/jifen">
            <div class="yue">
                <div class="yue_1">我的积分</div>


                    <div class="jiantou">
                        <img src="/wx/images/jian.png" width="30" height="31" />
                    </div>

            </div>
            </a>
            <div style="width:100%; height:1px; background:#c2c2c2;"></div>

            <div class="yue">
                <a href="/weixiduo/address/gladdress.html">
                <div class="yue_1">地址管理</div>


                    <div class="jiantou">
                        <img src="/wx/images/jian.png" width="30" height="31" />
                    </div>

            </div>
            </a>
            <div style="width:100%; height:1px; background:#c2c2c2;"></div>

            <div class="jifen">
                <div class="yue">
                    <a href="/weixiduo/member/zhanghao">
                    <div class="yue_1">账号设置</div>


                        <div class="jiantou">
                            <img src="/wx/images/jian.png" width="30" height="31" />
                        </div>

                </div>
                </a>
            </div>

        </div>
        <div style="clear:both"></div>
        <div class="foot">让你的果篮子翻滚吧！</div>
        <div style="width:100%; height:100px;"></div>
    </div>
</body>

</html>