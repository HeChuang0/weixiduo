<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <script src="/wx/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/wx/js/shouquan.js"></script>
    <script>
        var url = 'weixiduo/member/redPacket';
        if (getCookie("open") == null || getCookie("open") == "") {
            authorization(url)
        }
    </script>

    <script src="/wx/js/redPacket.js"></script>

    <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/wx/css/redPacket.css" type="text/css"/>
    <link rel="stylesheet" href="/wx/css/style.css" type="text/css"/>

    <title>新人礼包</title>
</head>

<body>
    <div class="bg">
        <div class="sigma-content">
            <div class="sigma-middle-line">
                <span class="sigma-line-text">特权一：免费领专享红包</span>
            </div>
        </div>
        <!-- 优惠券 -->
        <div class="quan">
            <div class="left">
                <p>全</p>
                <p>场</p>
                <p>通</p>
                <p>用</p>
            </div>
            <div class="middle" id="discount">

            </div>
            <button class="right" onclick="window.location.href='youhui.html'">
                <p>立即</p>
                <p>领取</p>
            </button>
        </div>
        <!-- 试吃 -->
        <div class="sigma-content">
            <div class="sigma-middle-line">
                <span class="sigma-line-text">特权二：新品试吃</span>
            </div>
        </div>
        <!-- 商品列表 -->
        <div class="height-define">
            <ul id="goods" data-am-widget="gallery" class="am-gallery pro-list am-avg-sm-3 am-avg-md-3 am-avg-lg-3 am-gallery-default  ">

            </ul>
        </div>
        <div class="more">
            <button style="margin-top:10px;" class="more_button">点击查看更多</button>
        </div>
        <!-- 线下店 -->
        <div class="sigma-content">
            <div class="sigma-middle-line">
                <span class="sigma-line-text">线下实体店服务</span>
            </div>
        </div>
        <ul class="yule" id="shanghu"></ul>
        <div class="more">
            <button onclick="window.location.href='/weixiduo/index'">更多美食，点击进入</button>
        </div>
    </div>


</body>

</html>