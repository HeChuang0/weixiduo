<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css"/>
    <link href="/wx/css/style.css" rel="stylesheet" type="text/css"/>

    <script src="/wx/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/wx/js/shouquan.js"></script>
<#--<script>-->
<#--var url = 'main/member/share.html';-->

<#--if (getCookie("open") == null || getCookie("open") == "") {-->
<#--authorization(url);-->
<#--}-->
<#--</script>-->
    <title>新会员推荐</title>

    <style>
        .share {
            width: 90%;
            height: auto;
            margin: 0 auto;
        }

        .share .introduction {
            width: 100%;
            margin-top: 5px;
            height: 120px;
            background: white;
            border-radius: 10px;

        }

        .share .description {
            display: block;
            height: 15%;
            width: 100%;
            font-size: 16px;
            color: red;
            font-weight: bold;
            text-align: center;
            padding-top: 5px;
        }

        .share .intro_image {
            float: left;
            width: 40%;
            height: 100%;
            padding: 10px;
        }

        .share .intro_image img {
            width: 100%;
            height: 100%;
        }

        .share .intro_desc {
            float: left;
            width: 60%;
            height: 100%;
            padding-top: 5px;
            font-size: 14px;
            color: #666;

        }

        .intro_desc .detail {
            text-indent: 2rem;
        }

        .share .production {
            width: 100%;
            height: auto;
            margin-top: 10px;
            background: white;
            border-radius: 10px;
        }

        .rule {
            width: 100%;
            height: auto;
            margin-top: 10px;
            border-radius: 10px;
            font-size: 16px;
            color: #666;
            background: white;
            padding: 20px;
        }

        .rule .rule1 {
            display: block;
            border-left: 3px solid red;
            padding-left: 5px;
        }

        .rule li {

            padding-left: 10px;
            margin-top: 10px;
        }

        .rule .erweima {
            width: 100%;
            margin: 0 auto;
            text-align: center;
            margin-bottom: 20px;
        }

        .rule .erweima img {
            width: 80%;
        }

        .bottom {
            width: 100%;
            padding: 30px;
            font-size: 24px;
            color: red;
            text-align: center;
            text-shadow: 1px 1px rgba(0, 0, 0, .3);
            font-style: italic;
            font-family: arial, georgia, verdana, simsun, helvetica, sans-serif;
        }

        .fixed {
            width: 80%;
            background: #ff4f4f;
            padding: 10px;
            text-align: center;
            border-radius: 5px;
            font-size: 20px;
            font-weight: bolder;
            font-family: "Times New Roman", Times, serif;
            -webkit-text-fill-color: white;
            margin-top: 20px;
            position: fixed;
            bottom: 0;
            left: 10%;

        }

        #mcover {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.7);
            display: none;
            z-index: 20000;
        }

        #mcover img {
            position: fixed;
            right: 18px;
            top: 5px;
            width: 260px;
            height: 180px;
            z-index: 20001;
        }
    </style>

</head>

<body style="background-color: rgb(254, 229, 149);">
<!-- 顶部 -->
<header data-am-widget="header" class="am-header am-header-default sq-head " style="background:#ff4f4f;">
    <div class="am-header-left am-header-nav">
        <a href="javascript:history.back()" class="">
            <i class="am-icon-chevron-left"></i>
        </a>
    </div>
    <h1 class="am-header-title">
        新会员推荐
    </h1>
</header>

<div class="share">
    <!-- 维喜多店铺简介 -->
    <div class="production">
        <section class="description">维喜多果园餐厅</section>
        <div class="introduction" style="height:160px;">
            <div class="intro_image">
                <img src="/wx/images/shop.jpg" style="height:80%;"/>
            </div>
            <div class="intro_desc">
                <section class="detail">维喜多餐厅是一家高档水果餐厅。</section>
                <section class="detail">我们每天都在关心您的果篮子和菜篮子，并随时为您提供最健康的食品。</section>
                <section class="detail">
                    <i style="color:red">总店地址</i>：江西省 &nbsp 南昌市 &nbsp 青山湖区 &nbsp 新高区恒大御景旁
                </section>
            </div>
        </div>
    </div>

    <!-- 产品简介 -->
    <div class="production">
        <section class="description">主营产品介绍</section>
        <!-- 新鲜果蔬 -->
        <div class="introduction ">
            <div class="intro_image">
                <img src="/wx/images/vegetable.png"/>
            </div>
            <div class="intro_desc">
                <section class="detail">时令果蔬，玲琅满目、新鲜出售、鲜美可口。</section>
                <section class="detail">视觉与味蕾的碰撞，等着你的到来！</section>
            </div>
        </div>

        <!-- 干果小食 -->
        <div class="introduction ">
            <div class="intro_image">
                <img src="/wx/images/dryFruit.jpg"/>
            </div>
            <div class="intro_desc">
                <section class="detail">用心匠造，样样甄选，一切源于生活的滋味。</section>
                <section class="detail">休闲时刻，怎么能没有干果小食的陪伴？</section>
            </div>
        </div>

        <!-- 水产海鲜 -->
        <div class="introduction ">
            <div class="intro_image">
                <img src="/wx/images/seafood.jpg"/>
            </div>
            <div class="intro_desc">
                <section class="detail" style="line-height:40px;">精彩生活，鲜活品味。</section>
                <section class="detail" style="line-height:30px;">海生百味，鲜聚维喜多。</section>
                <section class="detail" style="line-height:40px;">海聚百味，珍鲜荟萃。</section>
            </div>
        </div>

        <!-- 乡土特产 -->
        <div class="introduction ">
            <div class="intro_image">
                <img src="/wx/images/localSpecialty.jpg"/>
            </div>
            <div class="intro_desc">
                <section class="detail">最“土”不过乡情亲，最“特”还是乡土真。</section>
                <section class="detail">乡土特产，生态口感。更多精彩等你来发现！</section>
            </div>
        </div>
        <!-- 标语 -->
        <div class="bottom">分享有奖哦！</div>
    </div>

    <!-- 会员推荐规则 -->
    <div class="rule">
        <p class="rule1">分享规则</p>
        <li>
            <ol>1、分享成功后，您将免费得到一张
                <b style="color:red;">满11减10的优惠券</b>，可在“我的红包”中查看；
            </ol>
            <ol>2、第二次分享，不再获得优惠券，您将获得，
                <b style="color:red;">10积分</b>的奖励,积分到达一定数量可兑换现金券或者银卡一张。
            </ol>
            <ol>3、分享次数不限,多次分享按规则2予以奖励。特别提醒：只有关注了此公众号的果友分享成功后才有奖励哦。

            </ol>
        </li>
        <div class="erweima">
            <img src="/wx/images/erweima.jpg">
        </div>
    </div>
</div>

<div class="fixed" onclick="">
    动动手指头，开始分享吧！
</div>
<div id="mcover" onClick="document.getElementById('mcover').style.display='';">
    <img src="/wx/images/tishi.png">
</div>

<!-- 分享给微信朋友 -->

<script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
<script>
    var root = 'http://wxdshop.natappvip.cc';
    var curl = window.location.href;//分享页面地址----要修改！！！

    $('.fixed').click(function () {
        $.ajax({
            url: root + '/sdkjs/getSignature?url=' + curl,
            type: 'GET',
            success: function (content) {
                var data = content.data;
                console.log(data);
                //alert(data[0].appId + '/n' + data[0].timestamp + '/n' + data[0].nonceStr + '/n' + data[0].signature);
                //配置微信信息
                wx.config({
                    debug: false,
                    appId: data[0].appId,
                    timestamp: data[0].timestamp,
                    nonceStr: data[0].nonceStr,
                    signature: data[0].signature,
                    jsApiList: [
                        // 所有要调用的 API 都要加到这个列表中
                        'onMenuShareTimeline',       // 分享到朋友圈接口
                        'onMenuShareAppMessage',  //  分享到朋友接口
                    ]
                });
                wx.ready(function () {
                    // 微信分享的数据
                    var shareData = {
                        "imgUrl": 'http://p5kllyq5h.bkt.clouddn.com/weixiduo.png',    // 分享显示的缩略图地址  ----要添加！！！
                        "link": 'http://wxdshop.natappvip.cc/weixiduo/member/share',    // 分享地址
                        "desc": '时刻关注您的果篮子和菜篮子，挑逗你的味蕾，快来关注我们吧！',   // 分享描述
                        "title": '维喜多果园餐厅欢迎您的到来！',   // 分享标题
                        success: function () {
                            //分享成功后做相应的数据处理
                            var useropenid = getCookie("open");
                            if (useropenid!=""){
                                $.get(
                                        root + '/sdkjs/shareSuccess?openid='+useropenid,
                                        function (content) {
                                            alert('分享成功！请在“我的红包”和“我的积分中”查看您获得的奖励。')
                                        }
                                )
                            }else {
                                alert("亲爱的果友，您还未授权或者未关注我们，所以不能领取分享奖励哦！")
                            }

                        }
                    };
                    wx.onMenuShareTimeline(shareData);
                    wx.onMenuShareAppMessage(shareData);
                });
                wx.error(function (res) {
                    alert("好像出错了...");
                })
            },
            error: function () {
                alert("服务器出错了...");
            }
        })
        $('#mcover').css("display", "block");//给用户分享提示
    })


</script>
</body>

</html>