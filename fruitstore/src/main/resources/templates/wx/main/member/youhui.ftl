<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />

    <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
    <link href="/wx/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/wx/css/redPacket.css" rel="stylesheet" type="text/css">

    <script src="/wx/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/wx/js/shouquan.js"></script>
    <script src="/wx/js/redPacket.js"></script>
    <script type="text/javascript">

        var root = 'http://wxdshop.natappvip.cc';
        $(function () {
            var c2 = window.location.href.split('&')[1]
            if (c2) {
                var c1 = window.location.href.split('&')[0]
                var $id = c1.split('=')[1];
                $.get(root+'/RedPacketAction/findOne?id='+$id,
                function(content){
                    var info=content.data;
                    var amount=info[0].cashCoupon;
                    var startTime=info[0].startTime;
                    var endTime=info[0].endTime;
                    var $mm = $('#mm');
                    var $timeLimit = $("#timeLimit");
                    var html = '<section class="money">' + amount + '<span>元</span></section>';
                    var time = '有效期:<span style="color:red">' + startTime
                            + '</span>至<span style="color:red">' + endTime + '</span>';
                    $mm.html(html);
                    $timeLimit.html(time);
                })

            }
            else {

                $.ajax({
                    url: root + '/RedPacketAction/getNew',
                    success: function (content) {
                        console.log(content.data);
                        var data = content.data;
                        var $mm = $('#mm');
                        var $timeLimit = $("#timeLimit");

                        console.log(data[0].cashCoupon);
                        var html = '<section class="money">' + data[0].cashCoupon + '<span>元</span></section>';
                        var time = '有效期:<span style="color:red">' + data[0].startTime
                                + '</span>至<span style="color:red">' + data[0].endTime + '</span>';


                        $mm.html(html);
                        $timeLimit.html(time);

                    }

                }
                )
            }
        }
        )

    </script>
    <title>领取优惠券</title>
</head>

<body>
    <!-- 顶部 -->
    <header data-am-widget="header" class="am-header am-header-default sq-head " style="background:#ff4f4f;">
        <div class="am-header-left am-header-nav">
            <a href="javascript:history.back()" class="">
                <i class="am-icon-chevron-left"></i>
            </a>
        </div>
        <h1 class="am-header-title">
            领取优惠券
        </h1>
    </header>
    <!-- 优惠券详情 -->
    <div class="detail">
        <span>官方商城全场券 新人优惠买满立减</span>
    </div>
    <div class="detail-2" id="mm"></div>
    <div class="tip">
        <p class="tip-1">温馨提示：</p>
        <hr>
        <p class="tip-2">仅限维喜多果园餐厅新会员使用，买满立减~</p>
        <p class="tip-2">新会员限领一张</p>
        <p class="tip-2" id="timeLimit"></p>

    </div>
    <div class="get">
        <button onclick="getPacket()" class="submit">领取</button>
    </div>
</body>

</html>