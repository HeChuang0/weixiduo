<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>用户所有订单</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="renderer" content="webkit" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
	<link href="/wx/css/style.css" rel="stylesheet" type="text/css" />
	<link href="/wx/css/check.css" rel="stylesheet" type="text/css" />
	<script src="/wx/js/jquery-1.10.2.min.js"></script>
	<script src="/wx/js/shouquan.js"></script>
	<script src="/wx/js/myAlert.js"></script>

	<script>
		var url = '/weixiduo/order/allorder';
		if(getCookie("open")==null||getCookie("open")==""){
		  authorization(url);
		  console.log('ok')
		}
	  </script>
</head>

<body onload="getAllOrder()" onclick="">
	<header data-am-widget="header" class="am-header am-header-default sq-head ">
		<div class="am-header-left am-header-nav">
			<a href="javascript:history.back()" class="">
				<i class="am-icon-chevron-left"></i>
			</a>
		</div>
		<h1 class="am-header-title">
			<a href="" class="">全部订单</a>
		</h1>
	</header>
 
	<ul class="order-style">
		<li class="current all_order">
			<a href="javascript:void(0)">全部</a>
		</li>
		<li class="unpay_order">
			<a href="javascript:void(0)">待付款</a>
		</li>
		<li class="unreceive_order">
			<a href="javascript:void(0)" >待收货</a>
		</li>
		<li>
			<a href="javascript:void(0)">待评价</a>
		</li>
	</ul>
	<!--代付款-->
	<ul class="order_content">
		

    </ul>
	<!--交易成功-->

	<script src="/wx/js/order.js"></script>
	
</body>

</html>