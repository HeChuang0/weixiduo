<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>支付页面</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="renderer" content="webkit" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
	<script src="/wx/js/jquery-1.10.2.min.js"></script>
	<script src="/wx/js/shouquan.js"></script>
	<link href="/wx/css/style.css" rel="stylesheet" type="text/css" />
	<script src="/wx/js/pay.js"></script>
	<script src="/wx/js/jquery.min.js"></script>

</head>

<body>
	<header data-am-widget="header" class="am-header am-header-default sq-head-order ">
		<div class="am-header-left am-header-nav">
			<a href="javascript:history.back()" class="">
				<i class="am-icon-chevron-left"></i>
			</a>
		</div>
		<h1 class="am-header-title">
			<span href="" class="title">订单提交</span>
		</h1>
	</header>
	<div class="order-detail">
		<div class="title">订单提交成功</div>
		<div class="con">请于4小时内完成支付</div>
		<div class="con">否则系统将自动取消该订单</div>
	</div>

	<div class="pay-style">
		<div class="con con_order"></div>
		<div class="con con_pay"></div>
		<div class="con">支付方式：微信支付</div>
	</div>
	<div class="toPay">
		<button class="login-btn" value="" onclick="pay()">去支付</button>
	</div>
	<div class="explain">1.支付前请确认您的联系方式和收货地址无误。支付不成功不会扣钱，请放心使用！</div>
	<div class="explain">2.支付成功后，商家会尽快给您安排发货，并时刻与您保持消息畅通。</div>
	<div class="explain">3.您可以在“我的订单”中查看您的订单，或者拨打微喜多客服电话：13576070917咨询。</div>
	<script>
		var root = 'http://wxdshop.natappvip.cc';
		var $pay = $(".login-btn");
		var parament = window.location.search.split("&&")[1];
		var orderid = parament.split("=")[1];
		var returnurl = 'http://wxdshop.natappvip.cc/weixiduo/order/sell?orderid='+orderid;
		//单击事件
		function pay() {
			window.location.href = root + '/pay/create?orderId=' + orderid + '&returnUrl=' + returnurl;
		}
	</script>
</body>

</html>