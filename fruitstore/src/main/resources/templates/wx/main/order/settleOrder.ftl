<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>确定订单</title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        <meta name="renderer" content="webkit" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
	   <script src="/wx/js/jquery-1.10.2.min.js"></script>
	   <script src="/wx/js/shouquan.js"></script>
		<link href="/wx/css/style.css" rel="stylesheet" type="text/css" />
	<style>
	.coupon {
    font-size: 1.8rem	
    }
</style>
	
	</head>

	<body>
		<header data-am-widget="header" class="am-header am-header-default sq-head ">
			<div class="am-header-left am-header-nav">
				<a href="javascript:history.back()" class="">
					<i class="am-icon-chevron-left"></i>
				</a>
			</div>
			<h1 class="am-header-title">
  	            <a href="" class="">确认订单</a>
            </h1>
		</header>
		
	    <h5 class="order-tit">收货人信息</h5>
		<div class="order-address">
	    <div class="add-address">
           <a href="/weixiduo/address/add">+新建收货地址</a>
            <i class="am-icon-angle-right"></i>
		</div>
	</div>
		<div style="background: #eee; height: 10px;"></div>
	
        <h5 class="order-tit">确认订单信息</h5>
        <ul class="shopcart-list" style="padding-bottom: 0;">
	    	
	    </ul>
	    <ul class="order-infor">
	    	<li class="order-infor-first">
	    		<span>商品总计：</span>
	    		<i class="totalP"></i>
	    	</li>
	    	<li class="order-infor-first">
	    		<span>运费：</span>
	    		<i>包邮</i>
	    	</li>
	    	<li class="order-infor-first">
	    		<a href="">积分抵费></a>
	    	</li>
	    	<li class="order-infor-first">
	    		<a href="javascript:void(0)" class=" coupon">选择优惠券></a>
	    	</li>
	    </ul>
	    <div style="background: #eee; height: 10px;"></div>
	    <textarea placeholder="备注说明" class="bz-infor"></textarea>
	    <div style="background: #eee; height: 10px;"></div>
	    <div style="height: 55px;"></div>
	    <div class="shop-fix">
	    	<div class="order-text">
	    		应付总额：<span class="totalPrice"></span>
	    	</div>
	    	<a href="javascript:void(0)" class="js-btn">提交订单</a>
	    </div>
<!--底部-->
 <div style="height: 55px;"></div>
 <div data-am-widget="navbar" class="am-navbar am-cf am-navbar-default sq-foot am-no-layout" id="">
      <ul class="am-navbar-nav am-cf am-avg-sm-4">
          <li>
              <a href="/weixiduo/index" class="curr">
                  <span class="am-icon-home"></span>
                  <span class="am-navbar-label">首页</span>
              </a>
          </li>
          <li>
              <a href="/weixiduo/category/category" class="">
                  <span class="am-icon-th-large"></span>
                  <span class="am-navbar-label">分类</span>
              </a>
          </li>

          <li>
              <a href="/weixiduo/shopCart/shopcart" class="">
                  <span class="am-icon-shopping-cart"></span>
                  <span class="am-navbar-label">购物车</span>
              </a>
          </li>
          <li>
              <a href="/weixiduo/member/vip" class="">
                  <span class="am-icon-user"></span>
                  <span class="am-navbar-label">会员中心</span>
              </a>
          </li>
      </ul>
</div>
 
 
 
<script src="/wx/js/settleOrder.js"></script>
<script src="/wx/js/jquery.min.js"></script>
<script src="/wx/js/amazeui.min.js"></script>   
	</body>
</html>
