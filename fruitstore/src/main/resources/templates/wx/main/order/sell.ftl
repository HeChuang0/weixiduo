<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>订单详情页面</title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        <meta name="renderer" content="webkit" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
		<link href="/wx/css/style.css" rel="stylesheet" type="text/css" />
        <script src="/wx/js/jquery-1.10.2.min.js"></script>
        <script src="/wx/js/shouquan.js"></script>
    </head>
    <style>
    .detach_time,.receive_info>div,.order_number,.pay_method,.pay_time{
        text-align: right;
    }
    .order_reset{
        border:none;
    }
    .all_product_info .detail_li{
        position: relative;
    }
    .product_info .detail_quantity{
        position: absolute;
        top:6px;
        right:20px; 

    }

    
    </style>
	<body>
		<header data-am-widget="header" class="am-header am-header-default sq-head ">
			<div class="am-header-left am-header-nav">
				<a href="javascript:history.back()" class="">
					<i class="am-icon-chevron-left"></i>
				</a>
			</div>
			<h1 class="am-header-title">
  	            <a href="javascript:void(0)">订单详情</a>
            </h1>
        </header>
<!--商品信息 -->   
<table class="am-table" style="margin-bottom:5px;">
        <tbody>
            <tr>
                <td>商品信息</td>
              
            </tr>
        </tbody>
    </table> 
<ul class="all_product_info"> 
    <li>
        
    </li>
</ul> 
    <table class="am-table product_amount" style="margin-bottom:5px;">
            <tbody>
                <tr >
                    <td></td>
                   
                </tr>
              
            </tbody>
        </table>

	    <div style="height:10px;background-color: #F0F0F0"></div>
	    <table class="am-table detach_info">
            <tbody>
                <tr>
                    <td>配送信息</td>
                     
                </tr>
                <tr>
                    <td>送达时间：</td>
                    <td class="detach_time">3-5天</td>
                </tr>
                <tr >
                    <td style="width:30%;">送货地址：</td>
                    <td class="receive_info">
                    
                    </td>
                </tr>
              
            </tbody>
        </table>
        <div style="height:10px;background-color: #F0F0F0"></div>
	    <table class="am-table detach_info">
            <tbody>
                <tr>
                    <td>订单信息</td>
                     
                </tr>
                <tr>
                    <td>订单号：</td>
                    <td class="order_number"></td>
                </tr>
                <tr >
                    <td>支付方式：</td>
                    <td class="pay_method">
                     微信支付
                    </td>
                </tr>
                <tr >
                        <td>下单时间：</td>
                        <td class="pay_time">
                    
                        </td>
                    </tr>
              
            </tbody>
        </table>

<!--底部-->
<div style="height: 55px;"></div>
<script src="/wx/js/sell.js"></script>
<script src="/wx/js/jquery.min.js"></script>
<script src="/wx/js/amazeui.min.js"></script>   
	</body>
</html>
