<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>果篮子</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="renderer" content="webkit" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
	<link href="/wx/css/style.css" rel="stylesheet" type="text/css" />
	<script src="/wx/js/jquery-1.10.2.min.js"></script>
    <script src="/wx/js/shouquan.js"></script>
	<script src="/wx/js/myAlert.js"></script>
	<link rel="stylesheet" href="/wx/css/myAlert.css" type="text/css"/>
    <script src="/wx/js/lazyLoad.js"></script>

    <script>
        var url = 'weixiduo/shopCart/shopcart';
        if(getCookie("open")==null||getCookie("open")==""){
            authorization(url);
            console.log('ok')
        }
    </script>

</head>

<body onload="getShopCart()">

	<header data-am-widget="header" class="am-header am-header-default sq-head ">
		<div class="am-header-left am-header-nav">
			<a href="javascript:history.back()" class="">
				<i class="am-icon-chevron-left"></i>
			</a>
		</div>
		<h1 class="am-header-title">
			<a href="" class="">果蔬篮子</a>
		</h1>
	</header>
	<div class="am-modal-t"></div>
	<div class="shop-group-item">
		<ul class="shopcart-list">
			<img src="http://owtl83r0c.bkt.clouddn.com/placeholder.gif"/>
	    <div style="height: 10px; background: #eee;"></div>
		</ul>

		<div class="shop-fix">

			<label class="am-checkbox am-warning">
				<input type="checkbox" class="totalCheck"/>
			</label>
			<span class="AllCheck">全选</span>
			<a href="javascript:void(0)" class="js-btn">去结算</a>
			<div class="js-text">
				<P>合计：
					<span >￥</span><b class="totalPrice" >0.00</b>
				</P>
			</div>
		</div>
	</div>

	<!--底部-->
	<div style="height: 55px;"></div>
	<div data-am-widget="navbar" class="am-navbar am-cf am-navbar-default sq-foot am-no-layout" id="">
		<ul class="am-navbar-nav am-cf am-avg-sm-4">
            <li>
                <a href="/weixiduo/index">
                    <span class="am-icon-home"></span>
                    <span class="am-navbar-label">首页</span>
                </a>
            </li>
            <li>
                <a href="/weixiduo/category/category" class="">
                    <span class="am-icon-th-large"></span>
                    <span class="am-navbar-label">分类</span>
                </a>
            </li>

            <li>
                <a href="/weixiduo/shopCart/shopcart" class="curr">
                    <span class="am-icon-shopping-cart"></span>
                    <span class="am-navbar-label">果篮子</span>
                </a>
            </li>
            <li>
                <a href="/weixiduo/member/vip" class="">
                    <span class="am-icon-user"></span>
                    <span class="am-navbar-label">会员中心</span>
                </a>
            </li>
		</ul>
	</div>



	<script>
		//购物数量加减
		$(function () {
			$('body').on('click','.increase',function () {
				var self = $(this);
				var current_num = parseInt(self.siblings('input').val());
				current_num += 1;
				self.siblings('input').val(current_num);
				var productId=$(this).parents('li').attr('id');
                var openid=getCookie("open"); //保存openid
				TotalPrice();
				$.get(
				'http://wxdshop.natappvip.cc/cart/update?openid='+openid+'&productId='+productId+'&count='+current_num,//#endregion
				function(){
					console.log('ok')
				}
				)
			})
			$('body').on('click','.decrease',function () {
				var self = $(this);
				var current_num = parseInt(self.siblings('input').val());
				if (current_num > 1) {
					current_num -= 1;
					self.siblings('input').val(current_num);
					var productId=$(this).parents('li').attr('id');
					TotalPrice();
					$.get(
				   'http://wxdshop.natappvip.cc/cart/update?openid='+openid+'&productId='+productId+'&count='+current_num,//#endregion
				  function(){
					console.log('ok')
				 }
				 )
				}
			} )
	
		})







	</script>
    <script src="/wx/js/shopCart.js"></script>
	<script src="/wx/js/amazeui.min.js"></script>
</body>

</html>