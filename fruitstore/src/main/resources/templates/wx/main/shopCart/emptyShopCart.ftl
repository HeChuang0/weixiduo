<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>空果篮子</title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        <meta name="renderer" content="webkit" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
		<link href="/wx/css/style.css" rel="stylesheet" type="text/css" />
		<script src="/wx/js/jquery-1.10.2.min.js"></script>
	</head>
	<body>
		<header data-am-widget="header" class="am-header am-header-default sq-head ">
			<div class="am-header-left am-header-nav">
				<a href="javascript:history.back()" class="">
					<i class="am-icon-chevron-left"></i>
				</a>
			</div>
			<h1 class="am-header-title">
  	            <a href="" class="">果蔬篮子</a>
            </h1>
	    </header>
	    <div style="height: 49px;"></div>
	    <!--购物车空的状态-->
	    <div class="login-logo" style="margin:0 auto">
            <p style="text-align:center"><img src="/wx/images/shopCart.png" /></p>
	    	<p style="text-align:center" class="center">亲、您的篮子还是空空的哦，快去装满它!</p>
            <p style="text-align:center"><a href="/weixiduo/index" class="goshopping" >前去逛逛</a></p>
	    </div>
	    
<!--底部-->
 <div style="height: 55px;"></div>
 <div data-am-widget="navbar" class="am-navbar am-cf am-navbar-default sq-foot am-no-layout" id="">
      <ul class="am-navbar-nav am-cf am-avg-sm-4">
          <li>
              <a href="/weixiduo/index" class="curr">
                  <span class="am-icon-home"></span>
                  <span class="am-navbar-label">首页</span>
              </a>
          </li>
          <li>
              <a href="/weixiduo/category/category" class="">
                  <span class="am-icon-th-large"></span>
                  <span class="am-navbar-label">分类</span>
              </a>
          </li>

          <li>
              <a href="/weixiduo/shopCart/shopcart" class="">
                  <span class="am-icon-shopping-cart"></span>
                  <span class="am-navbar-label">果篮子</span>
              </a>
          </li>
          <li>
              <a href="/weixiduo/member/vip" class="">
                  <span class="am-icon-user"></span>
                  <span class="am-navbar-label">会员中心</span>
              </a>
          </li>
      </ul>
</div>
<script src="/wx/js/jquery.min.js"></script>
<script src="/wx/js/amazeui.min.js"></script>   
	</body>
</html>
