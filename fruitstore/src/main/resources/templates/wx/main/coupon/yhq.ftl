<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>优惠券</title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        <meta name="renderer" content="webkit" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
		<link href="/wx/css/style.css" rel="stylesheet" type="text/css" />
		<script src="/wx/js/jquery-1.10.2.min.js"></script>
		<script src="/wx/js/shouquan.js"></script>
		<!-- <script>
			var url = 'main/index.html';
			if(getCookie("open")==null||getCookie("open")==""){
			  authorization(url);
			  console.log('ok')
			}
		  </script> -->
	</head>
	<body style="background: #efefef;">
		<header data-am-widget="header" class="am-header am-header-default sq-head ">
			<div class="am-header-left am-header-nav">
				<a href="javascript:history.back()" class="">
					<i class="am-icon-chevron-left"></i>
				</a>
			</div>
			<h1 class="am-header-title">
  	            <a href="" class="">优惠券</a>
            </h1>
	    </header>
	   <div class="whitebar">
			<div data-am-widget="tabs" class="am-tabs am-tabs-d2">
				<ul class="am-tabs-nav am-cf" style="position: fixed; top: 49px;">
					<li class="am-active  useable"><a href="javascript:void(0)">可使用</a></li>
				    <li class="overdue"><a href="javascript:void(0)">已过期</a></li>
				</ul>
				<div style="height: 49px;"></div>
			</div>
			</div>
		</div>
		<ul class="yhq">
			</ul>
		<script src="/wx/js/yhq.js"></script>
	</body>
</html>
