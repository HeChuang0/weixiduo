<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>海鲜水产</title>

    <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
    <link href="/wx/css/style.css" rel="stylesheet" type="text/css" />

    <script src="/wx/js/shouquan.js"></script>
    <script>
        var url = '/weixiduo/list/seafood';

        if (getCookie("open") == null || getCookie("open") == "") {
            authorization(url);
        }
    </script>

    <script src="/wx/js/jquery-1.10.2.min.js"></script>
    <script src="/wx/js/lazyLoad.js"></script>
    <script src="/wx/js/category.js"></script>


    <style>
        .decorate {
            margin-top: 5px;
            width: 100%;
            height: 180px;
            ;
        }

        .decorate img {
            width: 100%;
            height: 100%;
        }
    </style>
</head>

<body onload="goodlist(15)">
    <header data-am-widget="header" class="am-header am-header-default sq-head ">
        <div class="am-header-left am-header-nav">
            <a href="javascript:history.back()" class="">
                <i class="am-icon-chevron-left"></i>
            </a>
        </div>
        <h1 class="am-header-title">
            <a href="javascript:void(0)" class="">海鲜水产</a>
        </h1>
    </header>
    <!-- 顶部图片 -->
    <div class="decorate">
        <img src="/wx/images/seafood.jpg" />
    </div>
    <!-- 海鲜水产商品列表 -->
    <ul data-am-widget="gallery" class="categoryContent am-gallery am-avg-sm-2 am-avg-md-3 am-avg-lg-3 am-gallery-default am-no-layout ">
        <!-- 加载时的动态图片 -->
        <img src="http://owtl83r0c.bkt.clouddn.com/placeholder.gif"/>
    </ul>

    <!--底部-->
    <div style="height: 55px;"></div>
    <div data-am-widget="navbar" class="am-navbar am-cf am-navbar-default sq-foot am-no-layout" id="">
        <ul class="am-navbar-nav am-cf am-avg-sm-4 ">
            <li>
                <a href="/weixiduo/index" class="curr">
                    <span class="am-icon-home"></span>
                    <span class="am-navbar-label">首页</span>
                </a>
            </li>
            <li>
                <a href="/weixiduo/category/category" class="">
                    <span class="am-icon-th-large"></span>
                    <span class="am-navbar-label">分类</span>
                </a>
            </li>

            <li>
                <a href="/weixiduo/shopCart/shopcart" class="">
                    <span class="am-icon-shopping-cart"></span>
                    <span class="am-navbar-label">果篮子</span>
                </a>
            </li>
            <li>
                <a href="/weixiduo/member/vip" class="">
                    <span class="am-icon-user"></span>
                    <span class="am-navbar-label">会员中心</span>
                </a>
            </li>
        </ul>
    </div>

</body>

</html>