<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>商品详情</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta name="renderer" content="webkit" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
	<link href="/wx/css/style.css" rel="stylesheet" type="text/css" />
	<script src="/wx/js/jquery-1.10.2.min.js"></script>
	<script src="/wx/js/shouquan.js"></script>
    <script src="/wx/js/detail.js"></script>
    <script src="/wx/js/myAlert.js"></script>
    <link rel="stylesheet" href="/wx/css/myAlert.css" type="text/css"/>

	<style>
		.am-slider-default .am-control-nav {
			text-align: center;
		}

		.am-slider-default .am-control-nav li a.am-active {
			background: #fdb325;
		}

		.am-slider-default .am-control-nav li a {
			border: 0;
			width: 10px;
			height: 10px;
		}

		img {
			display: block
		}
		#tab1 p{
			text-indent: 2rem;
		}
	</style>
</head>

<body>

	<header data-am-widget="header" class="am-header am-header-default sq-head ">
		<div class="am-header-left am-header-nav">
			<a href="javascript:history.back()" class="">
				<i class="am-icon-chevron-left"></i>
			</a>
		</div>
		<h1 class="am-header-title">
			<a href="" class="">商品详情</a>
		</h1>
	</header>


	<!--图片轮换-->

	<div data-am-widget="slider" class="am-slider am-slider-a1" data-am-slider='{&quot;directionNav&quot;:false}'>
		<ul class="am-slides ">
			<li style="height:240px">
				<img style="height:100%;width:100%" class="first_lunbo" src="http://owtl83r0c.bkt.clouddn.com/placeholder.gif" />
			</li>
			<li style="height:240px">
				<img style="height:100%;width:100%" class="second_lunbo" src="http://owtl83r0c.bkt.clouddn.com/placeholder.gif" />
			</li>
				

		</ul>
	</div>
	<div class="detal-info">
		<p></p>
		<h3>
		</h3>
	</div>

	<div class="d-amount">
		<h4>数量：</h4>
		<div class="d-stock">
			<a class="decrease">-</a>
			<input id="num" readonly="readonly"  class="text_box"  type="text" value="1"/>
		    <input class="nums" type="hidden" value="1"/>
			<a class="increase">+</a>
			<span id="dprice" class="price" style="display:none"> 36</span>
		</div>
	</div>
	<div class="am-modal-t"></div>
	<div style="background: #eee; height: 10px;"></div>
	<div class="am-tabs detail-list" data-am-tabs>
		<ul class="am-tabs-nav am-nav am-nav-tabs">
			<li class="am-active">
				<a href="#tab1">商品详情</a>
			</li>
			<li>
				<a href="#tab2">商品评论</a>
			</li>
		</ul>

		<div class="am-tabs-bd">
		<div class="am-tabs-bd">
			<div class="am-tab-panel am-fade am-in am-active detail " id="tab1">
				
				
			</div>
			<div class="am-tab-panel am-fade detail " id="tab2">
				<div class="comment">
					<span>好评：</span>
					<div class="com-good"></div>
					<span>100%</span>
				</div>
				<div class="comment">
					<span>中评：</span>
					<div class="com-bad"></div>
					<span>0%</span>
				</div>
				<div class="comment">
					<span>差评：</span>
					<div class="com-bad"></div>
					<span>0%</span>
				</div>
				<ul class="comment-list">
					<li>
						<a href="">全部</a>
					</li>
					<li>
						<a href="">好评（20）</a>
					</li>
					<li>
						<a href="">中评（5）</a>
					</li>
					<li>
						<a href="">差评（0）</a>
					</li>
				</ul>
				<ul class="comment-pic">
					<li>
						<div class="tit">
							<img src="/wx/images/tx.png" class="tx" />
							<span>songke2014</span>
							<i> [2016-01-01]</i>
						</div>
						<div class="comment-con">
							买了些给家人，都说不错！
						</div>
					</li>
					<li>
						<div class="tit">
							<img src="/wx/images/tx.png" class="tx" />
							<span>songke2014</span>
							<i> [2016-01-01]</i>
						</div>
						<div class="comment-con">
							买了些给家人，都说不错！
						</div>
					</li>
				</ul>
			</div>
		</div>
		</div>
		<div style="background: white;height: 10px;"></div>
		<div class="am-panel am-panel-default">
			<div class="am-panel-hd" style="background: #eee">买了该商品的会员还买了</div>
			<div class="am-panel-bd">
				正在开发中。。。
			</div>
		</div>

	</div>





	<!--底部-->
	<div style=" height: 55px;"></div>
	<ul class="fix-shopping">
		<li class="icon">
			<a href="">
				<i class=" am-icon-user am-icon-md"></i>
			</a>
			<div>我的客服</div>
		</li>
		<li class="icon">
			<a href="/weixiduo/index">
				<i class=" am-icon-home am-icon-md"></i>
			</a>
			<div>首页</div>
		</li>
		<li class="icon">
			<a href="/weixiduo/shopCart/shopcart">
				<i class=" am-icon-cart-plus am-icon-md"></i>
			</a>
			<div>果篮子</div>
		</li>
		<li class="pay">
			<a  class="join">
				<div class="addToCart"  onclick="$.myConfirm({title:' ',message:'已加入到果篮中！',callback:function(){}},this)">
				加入果篮
			</div>
			</a>
		</li>


	</ul>


	<script>
		//购物数量加减
		$(function () {
			$('.increase').click(function () {
				var self = $(this);
				var current_num = parseInt(self.siblings('input').val());
				current_num += 1;
				self.siblings('input').val(current_num);
				// update_item(self.siblings('input').data('item-id'));
			})
			$('.decrease').click(function () {
				var self = $(this);
				var current_num = parseInt(self.siblings('input').val());
				if (current_num > 1) {
					current_num -= 1;
					self.siblings('input').val(current_num);
					// update_item(self.siblings('input').data('item-id'));
				}
			})
		})
	</script>




	<script src="/wx/js/amazeui.min.js"></script>


</body>

</html>