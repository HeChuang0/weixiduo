<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>搜索页</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="renderer" content="webkit" />
    <meta http-equiv="Access-Control-Allow-Origin" content="http://apz24u.natappfree.cc" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
    <link href="/wx/css/style.css" rel="stylesheet" type="text/css" />
    <script src="/wx/js/jquery-1.10.2.min.js"></script>
    <script src="/wx/js/lazyLoad.js"></script>
    <script src="/wx/js/cache.history.js"></script>
    <style>
 
    </style>
</head>

<body>
 <!--输入区-->
    <div class="search-whole">
        <div class="input-define" style="height:45px">
            <header data-am-widget="header" class="am-header am-header-default tm-head sortbar-fixed" id="shortbar">
                <h1 class="am-header-title1">
                    <div class="search-box">
                        <input type="text" name="title" class="index-search" placeholder="开启食物之旅吧......" />
                        <span class="search-font">搜索</span>
                        <input type="submit" value="" class="search-icon" />
                    </div>
                </h1>
            </header>
        </div>
        <!--搜索历史-->
        <div class="search-history">
            <p class="search-top-desc">
                <p class="search-title">搜索历史</p>
                <a href="#" class="delete-search">
                    <img src="http://owtl83r0c.bkt.clouddn.com/delete.png" />
                </a>
            </p>
            <div class="search-history-list">
            </div>
        </div>

        <!--热门搜索-->
        <div class="search-hot">
              <p class="hot_title">热门搜索</p>
              <p class="first_hot"><span class="hot">进口柠檬</span>
                <span class="hot">黄金梨</span>
                  <span class="hot">奶油草莓</span></p>
              
               <p class="second_hot"><span class="hot">新西兰巧克力</span>
                <span class="hot">进口蓝莓</span>
                <span class="hot">榴莲干</span></p>
          
        </div>

        <!-- 商品区 -->
           <ul data-am-widget="gallery"  class="am-gallery  am-avg-sm-2 am-avg-md-2 am-avg-lg-4 am-gallery-default search_goods"  >
         </ul>


        <script src="/wx/js/search.js"></script>
        <!--底部-->
        <div style="height: 55px;"></div>
        <div data-am-widget="navbar" class="am-navbar am-cf am-navbar-default sq-foot am-no-layout" id="">
            <ul class="am-navbar-nav am-cf am-avg-sm-4">
                <li>
                    <a href="/weixiduo/index" class="curr">
                        <span class="am-icon-home"></span>
                        <span class="am-navbar-label">首页</span>
                    </a>
                </li>
                <li>
                    <a href="/weixiduo/category/category" class="">
                        <span class="am-icon-th-large"></span>
                        <span class="am-navbar-label">分类</span>
                    </a>
                </li>

                <li>
                    <a href="/weixiduo/shopCart/shopcart" class="">
                        <span class="am-icon-shopping-cart"></span>
                        <span class="am-navbar-label">购物车</span>
                    </a>
                </li>
                <li>
                    <a href="/weixiduo/member/vip" class="">
                        <span class="am-icon-user"></span>
                        <span class="am-navbar-label">会员中心</span>
                    </a>
                </li>
            </ul>
        </div>



      <script src="/wx/js/amazeui.min.js"></script>
    </div>
</body>

</html>