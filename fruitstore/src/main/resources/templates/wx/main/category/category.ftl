<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>分类</title>
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.3, user-scalable=no" />
        <meta name="renderer" content="webkit" />
        <meta http-equiv="Cache-Control" content="no-siteapp" />
    		<link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
	     	<link href="/wx/css/style.css" rel="stylesheet" type="text/css" />
			<script src="/wx/js/jquery-1.10.2.min.js"></script>
			<script src="/wx/js/shouquan.js"></script>
	
	</head>
	<body onload="getCategory()" onclick="">
		<header data-am-widget="header" class="am-header am-header-default sq-head ">
			<div class="am-header-left am-header-nav">
				<a href="javascript:history.back()" class="">
					<i class="am-icon-chevron-left"></i>
				</a>
			</div>
			<h1 class="am-header-title">
  	            <a href="javascript:void(0)" class="">商品分类</a>
            </h1>
	    </header>
	    <div style="height: 49px;"></div>
	    <div class="cate-search">
	    	<input type="text" class="cate-input" placeholder="请输入您要的搜索的产品关键词" />
	    	<input type="button" class="cate-btn" />
	    </div>
	    <div class="content-list">
	   	<div class="list-left" onclick="">
	    </div>
		<div class="list-right">
			<ul data-am-widget="gallery" class="subCategory am-gallery am-avg-sm-2 am-avg-md-2 am-avg-lg-4 am-gallery-default am-no-layout">
			</ul>
		</div>
	</div>

	<!--底部-->
	<div style="height: 55px;"></div>
	<div data-am-widget="navbar" class="am-navbar am-cf am-navbar-default sq-foot am-no-layout" id="">
		<ul class="am-navbar-nav am-cf am-avg-sm-4 ">
			<li>
				<a href="/weixiduo/index">
					<span class="am-icon-home"></span>
					<span class="am-navbar-label">首页</span>
				</a>
			</li>
			<li>
				<a href="/weixiduo/category/category" class="curr">
					<span class="am-icon-th-large"></span>
					<span class="am-navbar-label">分类</span>
				</a>
			</li>

			<li>
				<a href="/weixiduo/shopCart/shopcart" class="">
					<span class="am-icon-shopping-cart"></span>
					<span class="am-navbar-label">果篮子</span>
				</a>
			</li>
			<li>
				<a href="/weixiduo/member/vip" class="">
					<span class="am-icon-user"></span>
					<span class="am-navbar-label">会员中心</span>
				</a>
			</li>
		</ul>
	</div>

	<script src="/wx/js/category.js"></script>
	<script src="/wx/js/jquery.min.js"></script>
	<script src="/wx/js/amazeui.min.js"></script>
</body>

</html>