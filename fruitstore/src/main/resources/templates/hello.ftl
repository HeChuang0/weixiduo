<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>进入商城首页</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta name="renderer" content="webkit" />
    <meta http-equiv="Access-Control-Allow-Origin" content="http://apz24u.natappfree.cc" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <script src="/wx/js/jquery-1.10.2.min.js"></script>
    <link href="/wx/css/amazeui.min.css" rel="stylesheet" type="text/css" />
    <link href="/wx/css/style.css" rel="stylesheet" type="text/css" />
    <script src="/wx/js/jquery-1.10.2.min.js"></script>
    <script src="/wx/js/lazyLoad.js"></script>
    <script src="/wx/js/shouquan.js"></script>
    <script src="/wx/js/index.js"></script>
    <script>
        var url = 'weixiduo/index';
        if(getCookie("open")==null||getCookie("open")==""){
            authorization(url);
            console.log('ok')
        }
    </script>
</head>
<body>

<style>
    .height-define-zi .am-gallery-title-font{
        text-align: center;
        display:block;                     /*内联对象需加*/
        width:5em;
        word-break:keep-all;           /* 不换行 */
        white-space:nowrap;          /* 不换行 */
        overflow:hidden;               /* 内容超出宽度时隐藏超出部分的内容 */
        text-overflow:ellipsis;         /* 当对象内文本溢出时显示省略标记(/wx.) ；需与overflow:hidden;一起使用。*/
    }


</style>

<!--输入区-->
<div>
    <div class="input-define" style="height:45px">
        <header data-am-widget="header" class="am-header am-header-default tm-head sortbar-fixed" id="shortbar">
            <h1 class="am-header-title1">
                <div class="search-box">
                    <input type="text" name="title" class="index-search" placeholder="开启食物之旅吧" />
                    <span class="search-font">搜索</span>
                    <!--   <input type="submit" value="" class="search-icon" />  -->
                </div>
            </h1>
        </header>
    </div>
</div>

<!--图片轮换-->
<div class="am-slider am-slider-default" data-am-flexslider="{playAfterPaused: 2000}" id="demo-slider-0" style="position: relative;">
    <ul class="am-slides">
        <li>
            <img src="/wx/images/lunbo1.jpg" />
        </li>
        <li>
            <img src="/wx/images/lunbo2.jpg" />
        </li>
        <li>
            <img src="/wx/images/lunbo3.jpg" />
        </li>
        <li>
            <img src="/wx/images/lunbo4.jpg" />
        </li>
        <li>
            <img src="/wx/images/lunbo5.jpg" />
        </li>

    </ul>
</div>
<!--导航-->
<ul class="sq-nav">
    <li>
        <div class="am-gallery-item">
            <a href="/weixiduo/member/daily" class="">
                <img src="/wx/images/icon5.png" />
                <p>签到有礼</p>
            </a>
        </div>
    </li>
    <li>
        <div class="am-gallery-item">
            <a href="/weixiduo/member/redPacket" class="">
                <img src="/wx/images/icon.png" />
                <p>新人专区</p>
            </a>
        </div>
    </li>

    <li>
        <div class="am-gallery-item">
            <a href="" class="">
                <img src="/wx/images/icon6.png" />
                <p>精装礼品</p>
            </a>
        </div>
    </li>
    <li>
        <div class="am-gallery-item">
            <a href="/weixiduo/category/category" class="">
                <img src="/wx/images/icon7.png" />
                <p>分类</p>
            </a>
        </div>
    </li>
</ul>
<!-- 优惠券 -->
<div style="height:10px;background-color:#f2f2f2;margin-bottom:6px"></div>
<ul class="receive">

</ul>
<!--上市新品 -->
<div style="height:0.8rem;background-color:white;clear:both"></div>
<div class="sq-title">
    <i class="am-icon-volume-up"></i>
    <div class="fnTimeCountDown">
        新品上市啦
    </div>
</div>

<div class="height-define">
    <ul data-am-widget="gallery" class="am-gallery pro-list am-avg-sm-6 am-avg-md-6 am-avg-lg-6 am-gallery-default  height-define-zi">

    </ul>
</div>
<!-- 更多商品展示 -->
<div class="sq-title">
    <i class="am-icon-flag-checkered" style="font-size:20px"></i>
    <span style="margin-left:5px">猜你喜欢</span>
</div>
<ul data-am-widget="gallery"  class="am-gallery pro-list am-avg-sm-2 am-avg-md-2 am-avg-lg-4 am-gallery-default product_display"  >



</ul>


<div style="text-align:center">
    <button data-am-smooth-scroll class="am-btn-sm am-btn-warning">回顶部
        <i class="am-gotop-icon am-icon-chevron-up" style="margin-left:4px"></i>
    </button>
</div>
<!--底部-->
<div style="height: 55px;"></div>
<div data-am-widget="navbar" class="am-navbar am-cf am-navbar-default sq-foot am-no-layout" id="">
    <ul class="footerbar am-navbar-nav am-cf am-avg-sm-4">
        <li>
            <a href="/weixiduo/index" class="curr" id="first">
                <span class="am-icon-home"></span>
                <span class="am-navbar-label">首页</span>
            </a>
        </li>
        <li>
            <a href="/weixiduo/category/category" class="">
                <span class="am-icon-th-large"></span>
                <span class="am-navbar-label">分类</span>
            </a>
        </li>

        <li>
            <a href="/weixiduo/shopCart/shopcart" class="">
                <span class="am-icon-shopping-cart"></span>
                <span class="am-navbar-label">果篮子</span>
            </a>
        </li>
        <li>
            <a href="/weixiduo/member/vip" class="">
                <span class="am-icon-user"></span>
                <span class="am-navbar-label">会员中心</span>
            </a>
        </li>
    </ul>
</div>
<script src="/wx/js/amazeui.min.js"></script>
</body>

</html>