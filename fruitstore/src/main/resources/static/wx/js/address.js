
var $root='http://wxdshop.natappvip.cc'

var $checked=0; //地址修改页面获取的checked值
var openid=getCookie("open"); //保存openid
// var openid='osCI90cXzUyKJXk11UihtaVdZR5Q';
$(function () {
     
    /**
   * 新增地址
   */
    $('.address_sub1').on('click', function () {
        var name = $('#realname').val()
        var phone = $('#phone').val()
        var province = $('#s_province').val()
        var city = $('#s_city').val()
        var county = $('#s_county').val()
        var address = $('#address').val()
        var add=[province,city,county,address]
        var ad=add.join(" ")
        console.log(ad)
        var c1=window.location.href.split("?")[1]; 
        if(c1){
            var c2=c1.split("=")[1]; 
            console.log(c1)
        }
    
       
        /**
         * 校验弹出的提示框
         */
        var html = '<div class="am-modal am-modal-confirm" tabindex="-1" id="my-confirm">' +
            '<div class="am-modal-dialog">' +
            '<div class="am-modal-hd">提示</div>' +
            '<div class="am-modal-bd">' +
            '带*为必填项' +
            '</div>' +
            '<div class="am-modal-footer">' +
            '<span class="am-modal-btn" data-am-modal-confirm>确定</span>' +
            '<span class="am-modal-btn" data-am-modal-cancel>取消</span>' +
            ' </div>' +
            '</div>' +
            '</div>';
        console.log(phone.length)
        if (name != null && phone != null && address != null && (province != '*省份') && (city != '*市') && (county != '*区/县')) {
            if (phone.length != 11) {
                $('.am-modal-t').append(html)
                $('.am-modal-t').find('.am-modal-bd').text('号码格式错误')
                $('#my-confirm').modal({
                    relatedTarget: this,
                    onConfirm: function (options) {
                        $('.am-modal-t').empty()
                    },
                    onCancel: function () {
                        $('.am-modal-t').empty()
                    }
                });
            }
            else {
                /**
                 * 进入修改页面，携带checked和id
                 */
                if($(this).parent(".update_container")==true){
                    console.log('updadte')
                var data={
                    "id":c2,
                    "address":ad,
                    "consignee": name,
                    "openid":openid,
                    "phone": phone,
                    "checked":$checked
                  }
                  
                $.ajax({
                    type:'post',
                    contentType:'application/json;charset=UTF-8',
                    url:$root+'/AddressAction/update',
                    data:JSON.stringify(data),
                    dataType:'json',
                    success:function(){
                      console.log('进入update页面')
                      console.log('ok')
                      window.location.href = 'gladdress.html'
                    }
                })
            }
           else
            {
                /**
                 * 进入新增页面，不携带checked和id
                 */
                var data={
                    "address": ad,
                    "consignee": name,
                    "openid": openid,
                    "phone": phone
                  }
                  
                $.ajax({
                    type:'post',
                    contentType:'application/json;charset=UTF-8',
                    url:$root+'/AddressAction/add',
                    data:JSON.stringify(data),
                    dataType:'json',
                    success:function(){
                      console.log('ok')
                      console.log('添加成功')
                      window.location.href = 'gladdress.html'
                    }
                })
            }
            
              
            }
        }
        else {
            $('.am-modal-t').append(html)
            $('#my-confirm').modal({
                relatedTarget: this,
                onConfirm: function (options) {
                    $('.am-modal-t').empty()
                },
                onCancel: function () {
                    $('.am-modal-t').empty()
                }
            });
        }
    })

  /**
   * 修改默认收获地址
   */

    $('body').on('click','.Addreradio',function () {
    console.log('thiere')
    $(this).parents('ul').find('input').prop('checked', false)
    $(this).prop('checked', true)
    $(this).parents('ul').find('li')
    $(this).parents('ul').find('li').removeClass("curr")
    $(this).parents('li').addClass('curr')
    var $id=$(this).parents('li').attr('id');
    console.log($id)
    $.get(
        $root+'/AddressAction/updateChecked?id='+$id+'&openid='+openid,
        function(){
            console.log('ok')
        }
    )


})


})



/**
 * 地址列表
 */
function getInfo() {
    $.get(
        $root+'/AddressAction/findByOpenid?openid='+openid,
        function (content) {
            var info = content.data;
            console.log(info)
            $('.address-list').empty()
            $.each(info, function (n) {
                if (info[n].checked == 1) {
                    console.log(info[n].id)
                    var html = '<li class="curr" id="' + info[n].id + '" >' +
                        '<p>收货人：' + info[n].consignee + '&nbsp;&nbsp;' + info[n].phone + '</p>' +
                        '<p class="order-add1">收货地址：' + info[n].address + '</p>'
                        + '<hr />' +
                        '<div class="address-cz">' +
                        '<label class="am-radio am-warning">' +
                        '<input type="radio" class="Addreradio" checked  data-am-ucheck >' + '设为默认' +
                        '</label>' +
                        '<a href="/weixiduo/address/update?id='+info[n].id+'"><img src="/wx/images/bj.png" width="18" />&nbsp;编辑</a>' +
                        '<div class="del" onclick="$.myConfirm({title:\' \',message:\'确认删除该地址吗?\',callback:function(){}},this)">'
                        + '<a>' +
                        '删除' +
                        '</a>'+
                    '</div>' +
                        '</div>' +
                        '</li>'
                    $('.address-list').append(html);
                    console.log(html)
                }
                else {
                    console.log(info[n].id)
                    var html = '<li class=" " id="' + info[n].id + '" >' +
                        '<p>收货人：' + info[n].consignee + '&nbsp;&nbsp;' + info[n].phone + '</p>' +
                        '<p class="order-add1">收货地址：' + info[n].address + '</p>'
                        + '<hr />' +
                        '<div class="address-cz">' +
                        '<label class="am-radio am-warning">' +
                        '<input type="radio" class="Addreradio" data-am-ucheck  >' + '设为默认' +
                        '</label>' +
                        '<a href="/weixiduo/address/update?id='+info[n].id+'"><img src="/wx/images/bj.png" width="18" />&nbsp;编辑</a>' +
                        '<div class="del" onclick="$.myConfirm({title:\' \',message:\'确认删除该地址吗?\',callback:function(){}},this)">'
                        + '<a>' +
                        '删除' +
                        '</a>'
                    '</div>' +
                        '</div>' +
                        '</li>'
                    $('.address-list').append(html);
                    console.log(html)
                }

            })
        }
    )}

    

     /**
      * 修改地址，获取地址列表页面传过来的id
      */

    function getParam(){
        var c1=window.location.href.split("?")[1]; 
        var c2=c1.split("=")[1]; 
        $.get($root+'/AddressAction/getOne?id='+c2,
        function(content){
            console.log(content.data)
            var address=content.data[0].address
            var chec=content.data[0].checked
            $checked=chec
            var ad=address.split(" ")
            var province=ad[0]
            var city=ad[1]
            console.log(province)
            var county=ad[2]
            var addre=ad[3]
            console.log(county)
            console.log(addre)
            console.log(city)
            $('#realname').val(content.data[0].consignee)
            $('#phone').val(content.data[0].phone)
        /*  $('#s_province').val(province)
            $('#s_city').val('lilili')
            $('#s_county').val('lili') */
            $('#address').val(addre)
 
        }
    )
    }
    
 