/**
 *  存储搜索历史相关方法,localStorage
 * */

var SEARCH_KEY = '__search__'
var SEARCH_MAX_LEN = 10

function saveSearch(query) {
    var searchList = localStorage.getItem(SEARCH_KEY) ? localStorage.getItem(SEARCH_KEY).split(',') : []
    var index = searchList.indexOf(query)
    if (index === 0) {
        return
    }
    if (index > 0) {
        searchList.splice(index, 1)
    }
    searchList.unshift(query)
    if (SEARCH_MAX_LEN && searchList.length > SEARCH_MAX_LEN) {
        searchList.pop()
    }
    localStorage.setItem(SEARCH_KEY, searchList)
    return searchList
}

/* function deleteSearch(query) {
    var searchList = localStorage.getItem(SEARCH_KEY) || []
    var index = searchList.indexOf(query)
    if (index > -1) {
        searchList.slice(index, 1)
    }
    localStorage.set(SEARCH_KEY, searchList)
    return searchList
} */

/* function getSearch () {
    var searchList = localStorage.getItem(SEARCH_KEY).split(',')
    return searchList[0]
} */

function clearSearch() {
    localStorage.removeItem(SEARCH_KEY)
    return []
}