var $root='http://wxdshop.natappvip.cc'//保存域名
$('.cate-input').on('click', function (e) {
      window.location.href = 'weixiduo/display/search'
})
function getCategory() {
 $.get(
    $root+'/BuyerProductCategoryAction/get',
    function (content) {
      var info = content.data
      console.log(info)
      $('.content-list>.list-left').empty()
      $.each(info, function (n) {
        /**
         * 获取左侧列表时默认第一个被选中，右侧显示第一个主分类的子分类
         */
        if (n == 0) {
          var html = '<li class="current" id=" ' + info[n].id + '  "  onclick="" >' + info[n].categoryName + '</li>'
          $('.content-list>.list-left').append(html)
          $('.subCategory').empty()
          $.get(
           $root+'/BuyerProductSubcategoryAction/findByMaincategoryId?id=' + info[n].id,
            function (content) {
              var info = content.data;
              console.log(info);
              $.each(info, function (n) {
                var html = '<li id="' + info[n].id + '">' +
                  '<div class="am-gallery-item">' +
                  '<a href="list.html?id=' + info[n].id + '" >' +
                  '<img src="'+info[n].categoryImg +'">' +
                  '<h3 class="am-gallery-title">' + info[n].subcategoryName + '</h3>' +
                  '</a>' +
                  '</div>' +
                  '</li>'
                $('.subCategory').append(html)
              })

            })

        }
        else {
          var html = '<li id=" ' + info[n].id + '  " onclick="" >' + info[n].categoryName + '</li>'
          $('.content-list>.list-left').append(html)
        }
      })


    })
}
$(function () {


  /**
 * 子分类内容显示
 */
  $('body').on('click', '.list-left>li', function () {
    $('.list-left>li').removeClass('current');
    $(this).addClass('current');
    var $id = $(this).attr('id');
    console.log("id")
    console.log("here")
    $('.subCategory').empty()
    $.get(
      $root+'/BuyerProductSubcategoryAction/findByMaincategoryId?id=' + $id,
      function (content) {
        var info = content.data;
        console.log(info);
        $.each(info, function (n) {
          var html = '<li id="' + info[n].id + '">' +
            '<div class="am-gallery-item">' +
            '<a href="list.html?id=' + info[n].id + '" >' +
            '<img src="'+info[n].categoryImg +'">' +
            '<h3 class="am-gallery-title">' + info[n].subcategoryName + '</h3>' +
            '</a>' +
            '</div>' +
            '</li>'
          $('.subCategory').append(html)
        })

      })

  })


  /**
   * 销量，价格排序
   */

  $('.list-nav>li').on('click', function () {
    $('.list-nav>li').removeClass("current")
    $(this).addClass('current')
    /**
     * 判断点击的是价格排序域
     */
    if ($(this).hasClass('jiage')) {
      $(this).parent('.list-nav').find('.sales').removeClass('am-icon-arrow-up')
      $(this).parent('.list-nav').find('.sales').removeClass('am-icon-arrow-down')
     
      console.log('排序后的data')
      console.log($data)
      getData($data)
      if ($(this).hasClass('up')) {
        if($(this).find('i').hasClass('am-icon-arrow-down')){
          $(this).find('i').removeClass('am-icon-arrow-down')
          $(this).find('i').addClass('am-icon-arrow-up')
        }
        else{
          $(this).find('i').addClass('am-icon-arrow-up') 
        }
        $(this).removeClass('up')
        $(this).addClass('down')
        $data.sort(priceup)
        getData($data)

      } else {
        $(this).removeClass('down')
        $(this).addClass('up')
        $data.sort(pricedown)
        if($(this).find('i').hasClass('am-icon-arrow-up')){
          $(this).find('i').removeClass('am-icon-arrow-up')
          $(this).find('i').addClass('am-icon-arrow-down')
        }
        else{
          $(this).find('i').addClass('am-icon-arrow-down') 
        }
        getData($data)
      }

    }
    /**
     * 判断点击的是销量排序域
     */
    if ($(this).hasClass('xiaoliang')) {
      $(this).parent('.list-nav').find('.price').removeClass('am-icon-arrow-up')
      $(this).parent('.list-nav').find('.price').removeClass('am-icon-arrow-down')
     
      console.log($data)
      getData($data)
      if ($(this).hasClass('down')) {
        if($(this).find('i').hasClass('am-icon-arrow-up')){
          $(this).find('i').removeClass('am-icon-arrow-up')
          $(this).find('i').addClass('am-icon-arrow-down')
        }
        else{
          $(this).find('i').addClass('am-icon-arrow-down') 
        }
        $(this).removeClass('down')
        $(this).addClass('up')
        $data.sort(volumndown)
        getData($data)

      } else {
        if($(this).find('i').hasClass('am-icon-arrow-down')){
          $(this).find('i').removeClass('am-icon-arrow-down')
          $(this).find('i').addClass('am-icon-arrow-up')
        }
        else{
          $(this).find('i').addClass('am-icon-arrow-up') 
        }
        $(this).removeClass('up')
        $(this).addClass('down')
        $data.sort(volumnup)
        getData($data)
      }

    }

    if ($(this).hasClass('overall')) {
      $(this).parent('.list-nav').find('.price').removeClass('am-icon-arrow-up')
      $(this).parent('.list-nav').find('.price').removeClass('am-icon-arrow-down')
      $(this).parent('.list-nav').find('.sales').removeClass('am-icon-arrow-up')
      $(this).parent('.list-nav').find('.sales').removeClass('am-icon-arrow-down')
      getData($data1)
     
    }



  });


/**
 * 
 * 价格排序
 */
function priceup(a1,a2){
  if(a1.productPrice>a2.productPrice){
   return 1
}
  else{
      return -1
  }
}
function pricedown(a1,a2){
  if(a1.productPrice>a2.productPrice){
  return -1
}
  else{
      return 1
  }
}
/**
 * 
 * 销量排序
 */
function volumnup(a1,a2){
  if(a1.salesVolume>a2.salesVolume){
   return 1
}
  else{
      return -1
  }
}
function volumndown(a1,a2){
  if(a1.salesVolume>a2.salesVolume){
  return -1
}
  else{
      return 1
  }
}
/**
 * 试吃页面
 */
$.get($root+'/BuyerProductInfoAction/findByForetaste',
function(content){
  console.log(content.data)
  getData(content.data)
}
)

/**
 * 新品页面
 */
$.get($root+'/BuyerProductInfoAction/getNewProducts',
function(content){
  console.log(content.data)
  getData(content.data)
}
)

})

/**
 * 分类详细内容获取，通过获取子分类发送过来id获取子分类下的所有商品
 * 
 */
var $data=[]//用于价格，销量排序
var $data1=[]//用于综合
function getParam() {
  var c1 = window.location.href.split("?")[1];
  var c2 = c1.split("=")[1];
  console.log(c2)
  
  $.get($root+'/BuyerProductInfoAction/findBySubCategoryId?subId=' + c2,
    function (content) {
      var info = content.data
      $.each(info,function(n){
        $data.push(info[n])
        $data1.push(info[n])
      })
      console.log('排序前的data')
      console.log($data)
      getData(info)
    


    }
  )
}

function getData(info) {
  $('.categoryContent').empty()
   console.log(info)
  $.each(info, function (n) {
    var html = '<li id="' + info[n].id + '">' +
      '<div class="am-gallery-item">' +
      '<a href="../display/detail.html?id='+info[n].id+'"><img src="http://owtl83r0c.bkt.clouddn.com/placeholder.gif" data=" '+info[n].productIcon.split(";")[0]+'" class="list-picture" /></a>' +
      '<div class="am-gallery-title am-gallery-title-font">' + info[n].productName + '</div>' +
      '<div class="am-gallery-desc am-gallery-desc-font">￥' + info[n].productPrice+'/'+info[n].specification

      + '</div>' +

      '</div>' +
      '</div>' +
      '</li>'
      $('.categoryContent').append(html)
      console.log ($('.categoryContent'))
      $('img.list-picture').lazyLoad()
  })


}
function goodlist(categoryId) {
 
  $.ajax({
      url: $root+'/BuyerProductInfoAction/findByCategoryId?categoryId=' + categoryId,
      success: function (content) {
          var data = content.data;
          console.log(data);
          getData(data);//category.js中的方法
      }
  })
}

