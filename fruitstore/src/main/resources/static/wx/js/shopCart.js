
// var openid='osCI90cXzUyKJXk11UihtaVdZR5Q'
var $root='http://wxdshop.natappvip.cc' //保存域名
var openid=getCookie("open"); //保存openid

$(function () {
  /**
   * 
   * 购物车列表操作
   * 
   */
  // 点击商品按钮
  $('body').on('click', '.goodsCheck', (function () {
    var $flag=0
    if($(this).is(":checked")){
     //判断是否被选中
        $flag=1
    }
    var $id=$(this).parents('li').attr('id')
    console.log('单个商品flag的值'+$flag)
    console.log($id)
   $.get($root+'/cart/selectProduct?openid='+openid+'&&productId='+$id+'&&status='+$flag,
   function(content){
     console.log('ok')
   }
  )
    var goods = $(this).closest(".shop-group-item").find(".goodsCheck"); //获取所有商品
    var goodsC = $(this).closest(".shop-group-item").find(".goodsCheck:checked"); //获取所有被选中的商品
    var total = $(this).closest(".shop-group-item").find(".totalCheck"); //获取全选按钮
    if (goods.length == goodsC.length) { //如果选中的商品等于所有商品
      total.prop('checked', true); //全选按钮被选中
      TotalPrice();
    }
    else { //如果选中的商品不等于所有商品
      total.prop('checked', false); //全选按钮不被选中
      // 计算
      TotalPrice();
      // 计算
    }
  })
  )
  // 点击全选按钮
  $(".totalCheck").click(function () {
    var $flag=0
    if ($(this).prop("checked") == true) { //如果全选按钮被选中
      $(".goods-check").prop('checked', true); //所有按钮都被选中
      TotalPrice();
    }
    else {
      $(".goods-check").prop('checked', false); //else所有按钮不全选
      TotalPrice();
    }
    if($(this).is(":checked")){
      //判断是否被选中
         $flag=1
     }
     $.get($root+'/cart/selectAll?openid='+openid+'&&status='+$flag,
    function(content){
      console.log('ok')
    }

  );
})
/* 点击结算按钮 */

$('.js-btn').on('click',function(){
  if($('.goodsCheck').is(":checked"))
  {
    window.location.href="../order/settleOrder.html"
  }
  else{
    var html = '<div class="am-modal am-modal-confirm" tabindex="-1" id="my-confirm">' +
            '<div class="am-modal-dialog">' +
            '<div class="am-modal-hd">提示</div>' +
            '<div class="am-modal-bd">' +
            '未选择商品' +
            '</div>' +
            '<div class="am-modal-footer">' +
            '<span class="am-modal-btn" data-am-modal-confirm>确定</span>' +
            '<span class="am-modal-btn" data-am-modal-cancel>取消</span>' +
            ' </div>' +
            '</div>' +
            '</div>';
            $('.am-modal-t').append(html)
            $('#my-confirm').modal({
                relatedTarget: this,
                onConfirm: function (options) {
                    $('.am-modal-t').empty()
                },
                onCancel: function () {
                    $('.am-modal-t').empty()
                }
            }); 
  }

}
)
})



//计算
  function TotalPrice() {
    var allprice = 0; //总价
    //循环所有商品
    $(".goodsCheck").each(function () { //循环所有商品
      if ($(this).is(":checked")) { //如果该商品被选中
        var num = parseInt($(this).parents("li").find("#num").val());//得v到商品的数量
        console.log('数量' + num);
        var price = parseFloat($(this).parents("li").find(".shop-list-price").html()); //得到商品的单价
        console.log('价格' + $(this).parents("li").find(".shop-list-price").html());
        var total = price * num; //计算单个商品的总价
        allprice += total; //计算总价

      }
      $('.totalPrice').text(allprice.toFixed(2)); //显示被选中商品的店铺总价
    });



  }

  /**
       * 查询购物车列表  使用图片懒加载
  */
 
function getShopCart() {
  $.get(
    $root+'/cart/list?openid='+openid,
    function (content) {
      var info = content.data
      var $data = info[0].cartProductVOList
      if($data.length==0)      //判断购物车是否为空，若为空，则跳转至空购物车页面
      {
           window.location.href="emptyShopCart.html"
      }
      console.log($data)
      $('.shopcart-list').empty()
      $.each($data, function (n) {
        var $src=$data[n].productIcon.split(';')
        console.log('li的id:'+$data[n].productId)
        var html = '<li id="' + $data[n].productId + '">' +
          '<label class="am-checkbox am-warning">' +
          '<input type="checkbox" class="goodsCheck goods-check"  data-am-ucheck uncheck>' +
          '</label>' +
          '<a href="detail.html">' +
          '<img class="shop-pic" src=" " data="' +$src[0]  + '"/>' +
          '</a>' +
          '<div class="shop-list-mid">' +
          '<div class="tit">' +
          '<a href="detail.html">' + $data[n].productName + '</a>' +
          '</div>' +
          '<div class="d-stock">' +
          '<a class="decrease">-</a>' +
          '<input id="num" readonly="" class="text_box" name="" type="text" value="' + $data[n].quantity + '">' +
          '<a class="increase">+</a>' +
          '</div>' +
          '</div>' +
          '<span class="notation">￥</span><b class="shop-list-price">' + $data[n].productPrice + '</b>' +
          '<div class="del" onclick="$.myConfirm({title:\' \',message:\'确认删除该商品吗?\',callback:function(){}},this)">' +
          '<i class="am-icon-trash">'
          + '</i>' +
          '</div>' +
          '</li>'
        $('.shopcart-list').append(html)
        $('img.shop-pic').lazyLoad()
        if($data[n].productChecked==1){
          console.log($data[n].productChecked)
          $('.goodsCheck').prop('checked', true)
          }
      })
      TotalPrice();
      var goods =$(".shop-group-item").find(".goodsCheck"); //获取所有商品
      var goodsC = $(".shop-group-item").find(".goodsCheck:checked"); //获取所有被选中的商品
      var total = $(".shop-group-item").find(".totalCheck"); //获取全选按钮
      if (goods.length == goodsC.length) {
        console.log(goods)
         //如果选中的商品等于所有商品
        total.prop('checked', true); //全选按钮被选中
        TotalPrice();
      }


    }

  )
}