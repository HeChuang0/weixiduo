var root='http://wxdshop.natappvip.cc'
// var openid='osCI90cXzUyKJXk11UihtaVdZR5Q';
var openid=getCookie("open");//保存openid

// console.log(openid)
$(function(){


/**
 * 用户所有订单
 * 
 */
$('.all_order').on('click',function(){
    $(this).parent().find('li').removeClass('current')
    $(this).addClass('current')
    $('.order_content').empty()
    $('.order_content').append(allGoods)
})


/**
* 待支付订单
* 
*/
$('.unpay_order').on('click',function(){
   $(this).parent().find('li').removeClass('current')
   console.log('unpay')
   $(this).addClass('current')
   $('.order_content').empty()
   $('.order_content').append(unpayGoods)
})

$('body').on('click','.c-comment-list',function(){
    var $id=$(this).parent('li').attr('id')
    console.log($id)
    window.location.href='sell.html?id='+$id
})


    $('body').on('click','.order_cancel',function() {
        var $orderid = $(this).parents('li').attr('id');
        var $this = $(this)
        if ($this.text() == '取消订单'||$this.text() == '申请退款')
            $.get(root + '/BuyerOrderAction/cancel?orderid=' + $orderid+'&openid='+openid,
                function (content) {

                    $this.text('已取消');
                    $this.parent('li').find('.c-comment-suc').text('已取消')
                    $this.css('background-color', 'gray')
                    $this.parent('div').find('.to_pay').remove()
                    $this.parent('div').append('<a class="to_delete" onclick="">'+'删除订单'+'</a>')

                }
                )
                }
            )


    $('body').on('click','.to_delete',function() {
        var orderid=$(this).parents('li').attr('id')

        $.get($root+"/BuyerOrderAction/delete?orderid="+orderid+'&openid='+openid,
            function(){
            window.location.href=" "
            }
        )


    })



})



var unpayGoods=[]//存放未支付商品

var allGoods=[]//存放所有的商品
function getAllOrder(){
    $.get(
        root+'/BuyerOrderAction/buyerOrderList?openid='+openid,
        function(content){
        var info=content.data
        console.log(info)
        $('.order_content').empty()
        $.each(info,function(n){
            /**
             * 显示未支付订单
             */

            if(info[n].payStatus==0){
                if(info[n].orderStatus==0){
                    var html='<li  class="orderGoods" id="'+info[n].orderid+'">'+
                        '<div class="c-comment">'+
                        '<span class="c-comment-num">订单编号'+info[n].orderid+'</span>'+
                        '<span class="c-comment-suc">待付款</span>'+
                        '</div>'+

                        '<div class="c-comment-list" style="border: 0;">'+
                        '<a class="o-con" href="javascript:void(0)">'+
                        '<div class="o-con-img">'+
                        '<img src="/wx/images/basket.jpg">'+
                        '</div>'+
                        '<div class="o-con-txt">'+
                        '<p>创建时间：'+info[n].createTime+'</p>'+

                        '<p>合计：'+
                        '<span>￥'+info[n].orderAmount+'</span>'+
                        '</p>'+
                        '</div>'+
                        '</a>'+

                        '</div>'+
                        '<div class="c-com-btn">'+
                        '<a href="javascript:void(0)" class="order_cancel" >取消订单</a>'+
                        '<a href="/weixiduo/order/pay?orderAmount='+info[n].orderAmount+'&&orderid='+info[n].orderid+'" class="to_pay">立即支付</a>'+
                        '</div>'+
                        '<div class="clear"></div>'+

                        '</li>'
                    unpayGoods.push(html)
                    allGoods.push(html)
                    $('.order_content').append(html)
                }
                if(info[n].orderStatus==2){
                    var html='<li  class="orderGoods" id="'+info[n].orderid+'">'+
                        '<div class="c-comment">'+
                        '<span class="c-comment-num">订单编号'+info[n].orderid+'</span>'+
                        '<span class="c-comment-suc">已取消</span>'+
                        '</div>'+

                        '<div class="c-comment-list" style="border: 0;">'+
                        '<a class="o-con" href="javascript:void(0)">'+
                        '<div class="o-con-img">'+
                        '<img src="/wx/images/basket.jpg">'+
                        '</div>'+
                        '<div class="o-con-txt">'+
                        '<p>创建时间：'+info[n].createTime+'</p>'+

                        '<p>合计：'+
                        '<span>￥'+info[n].orderAmount+'</span>'+
                        '</p>'+
                        '</div>'+
                        '</a>'+

                        '</div>'+
                        '<div class="c-com-btn">'+
                        '<a class="to_delete" onclick="">删除订单</a>'+'</div>'+
                        '<div class="clear"></div>'+

                        '</li>'
                    unpayGoods.push(html)
                    allGoods.push(html)
                    $('.order_content').append(html)
                }
        }
        /**
         * 显示已支付订单
         */
        if(info[n].payStatus==1){
            if(info[n].orderStatus==0){
                var html='<li  class="orderGoods" id="'+info[n].orderid+'">'+
                    '<div class="c-comment">'+
                    '<span class="c-comment-num">订单编号'+info[n].orderid+'</span>'+
                    '<span class="c-comment-suc">已付款</span>'+
                    '</div>'+

                    '<div class="c-comment-list" style="border: 0;">'+
                    '<a class="o-con" href="javascript:void(0)">'+
                    '<div class="o-con-img">'+
                    '<img src="/wx/images/basket.jpg">'+
                    '</div>'+
                    '<div class="o-con-txt">'+
                    '<p>创建时间：'+info[n].createTime+'</p>'+

                    '<p>合计：'+
                    '<span>￥'+info[n].orderAmount+'</span>'+
                    '</p>'+
                    '</div>'+
                    '</a>'+

                    '</div>'+
                    '<div class="c-com-btn">'+
                    '<a href="javascript:void(0)" class="order_cancel" >申请退款</a>'+
                    '<a href="javascript:void(0)">确定收货</a>'+

                    '</div>'+
                    '<div class="clear"></div>'+

                    '</li>'
                allGoods.push(html)
                $('.order_content').append(html)
            }
            if(info[n].orderStatus==1){
                var html='<li  class="orderGoods" id="'+info[n].orderid+'">'+
                    '<div class="c-comment">'+
                    '<span class="c-comment-num">订单编号'+info[n].orderid+'</span>'+
                    '<span class="c-comment-suc">已发货</span>'+
                    '</div>'+

                    '<div class="c-comment-list" style="border: 0;">'+
                    '<a class="o-con" href="javascript:void(0)">'+
                    '<div class="o-con-img">'+
                    '<img src="/wx/images/basket.jpg">'+
                    '</div>'+
                    '<div class="o-con-txt">'+
                    '<p>创建时间：'+info[n].createTime+'</p>'+

                    '<p>合计：'+
                    '<span>￥'+info[n].orderAmount+'</span>'+
                    '</p>'+
                    '</div>'+
                    '</a>'+

                    '</div>'+
                    '<div class="c-com-btn">'+

                    '<a href="javascript:void(0)">确定收货</a>'+

                    '</div>'+
                    '<div class="clear"></div>'+

                    '</li>'
                allGoods.push(html)
                $('.order_content').append(html)

            }
            if(info[n].orderStatus==2){

                var html='<li  class="orderGoods" id="'+info[n].orderid+'">'+
                    '<div class="c-comment">'+
                    '<span class="c-comment-num">订单编号'+info[n].orderid+'</span>'+
                    '<span class="c-comment-suc">已取消</span>'+
                    '</div>'+

                    '<div class="c-comment-list" style="border: 0;">'+
                    '<a class="o-con" href="javascript:void(0)">'+
                    '<div class="o-con-img">'+
                    '<img src="/wx/images/basket.jpg">'+
                    '</div>'+
                    '<div class="o-con-txt">'+
                    '<p>创建时间：'+info[n].createTime+'</p>'+

                    '<p>合计：'+
                    '<span>￥'+info[n].orderAmount+'</span>'+
                    '</p>'+
                    '</div>'+
                    '</a>'+

                    '</div>'+
                    '<div class="c-com-btn">'+
                    '<a href="javascript:void(0)" class="order_cancel" style="background-color:gray;" >已取消</a>'+
                    '<a class="to_delete" onclick="">删除订单</a>'+

                    '</div>'+
                    '<div class="clear"></div>'+

                    '</li>'
                allGoods.push(html)
                $('.order_content').append(html)

            }

        }
       

        }
    )
        
  
 
 }
)
  
 }