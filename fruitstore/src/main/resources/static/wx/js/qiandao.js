var date = new Date;
var year = date.getFullYear();
var month = date.getMonth() + 1;
if (month <= 9) {
    month = "0" + month;
}
var day = date.getDate();
if (day <= 9) {
    day = "0" + day;
}
//格式为：2018-03-03
var str = year + "-" + month + "-" + day;//签到日期
//上传数据需要的字段
var useropenid = getCookie("open");
console.log("签到界面openid=" + useropenid);
var qiandao = 10;//签一次到给用户加的积分
var hasQiandao = false;//全局变量


//（用户进入页面）就先判断用户是否已经签过到
var root = 'http://wxdshop.natappvip.cc';
$.ajax({
    url: root + '/SignAction/signList?openid=' + useropenid,
    success: function (content) {
        console.log(content);
        var data = content.data;//得到的是一个数组
        console.log(data.length);
        for (var i = 0; i < data.length; i++) {
            //循环遍历data数组
            if (data[i].signDate == str) {
                //说明该用户今天已经签过到了，则不能再次签到
                //设置样式
                $('td').each(function () {
                    if ($(this).text() <= 9) {
                        var today = "0" + $(this).text();
                        //console.log(today);
                    }
                    if ($(this).text() == day || today == day) {
                        console.log($(this));
                        $(this).css('color', 'red');
                    }
                })
                $('#qian').text('已签到');
                //签到按钮设置为灰色不可用
                $('#qian').attr('disabled', true);
                $('#qian').css('background', 'gray');
            }
        }
    }
})

//单击事件
function signSubmit() {
    //循环遍历每一个td单元格
    $('td').each(function () {
        //console.log($(this).text());
        if ($(this).text() <= 9) {
            var today = "0" + $(this).text();
        }
        //点击“签到”后找到对应td单元格
        if ($(this).text() == day || today == day) {
            //用户同一天不能连续签到
            if (hasQiandao == true) {
                alert("您今天已经签到了哟！");
            }
            else {
                hasQiandao = true;
                console.log('打印当前单元格：' + $(this));
                $(this).css('color', 'red');
                $('#qian').text('已签到');
                //签到按钮设置为灰色不可用
                $('#qian').attr('disabled', true);
                $('#qian').css('background', 'gray');

                //接下来要实现用户积分增加的功能
                $(function () {
                    var data = {
                        "openid": useropenid,
                        "integralQuantity": qiandao
                    }
                    var info = JSON.stringify(data);
                    $.ajax({
                        contentType: "application/json;charset=UTF-8",
                        url: root + '/SignAction/sign',
                        type: 'POST',
                        data: info,
                        success: function (content) {
                            console.log(content);
                            alert('签到成功！积分+10');
                        },
                        error: function () {
                            alert("服务器出错了");
                        }
                    })
                })
            }
        }
    })
}
