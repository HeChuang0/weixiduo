/**
 * @Description: 图片懒加载插件
 *  <img class="lazy" src="/img/placeholder.gif" data=""/>
 *  `src`: 加载占位图
 *  `data`: 图片实际路径
 *  调用：$('img.lazy').lazyLoad()
 */

;(function ($) {
    $.fn.lazyLoad = function () {
        var $this = $(this),
            _windowScrollTop = 0,
            _windowHeight = $(window).height()
            console.log('lazyLoad')
        $(window).on('scroll', function() {
            _windowScrollTop = $(window).scrollTop()
            lazyLoad()
        })

        lazyLoad()

        function lazyLoad () {
            $.each($this, function (index, item) {
                var $img = $(item)
                if ($img.attr('data')) {
                    var offsetTop = $img.offset().top
                    if (offsetTop<= _windowHeight + _windowScrollTop) {
                        console.log($img.attr('data'))
                        $img.attr('src', $img.attr('data'))
                        $img.removeAttr('data')
                    }
                }
            })
        }
    }
}
)(jQuery)
