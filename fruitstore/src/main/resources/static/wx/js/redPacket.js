
var root = 'http://wxdshop.natappvip.cc';
//添加红包需要上传的数据

var useropenid = getCookie("open");
console.log("新人专区界面openid=" + useropenid);


//特权一
$.ajax({
    url: root + '/RedPacketAction/getNew',
    async: true,
    success: function (content) {
        console.log(content.data);
        var data = content.data;
        var $discount = $('#discount');
        var $mm = $('#mm');

        var did = data[0].id;//赋值给全局变量
        setCookie("discountId",did,1)
        var html_one = '<p class="p2"><b>' + data[0].cashCoupon + '</b>元优惠券</p>' +
            '<p class="p3">满' + data[0].fullCut + '立减</p>';

        var html_two = '<section class="money">' + data[0].cashCoupon + '<span>元</span></section>';

        $discount.html(html_one);
        $mm.html(html_two);
        console.log('渲染成功');
    }
})

var  discountId=getCookie("discountId");
//特权三
$.ajax({
    url: root + '/BuyerBusinessAction/nearbyBusinesses?openid=' + useropenid,
    success: function (content) {
        console.log(content.data);
        var data = content.data;

        var $shanghu = $('#shanghu');
        console.log(data.length);
        console.log(data[0].businessImage + data[0].businessAddress + data[0].businessName + data[0].distance);
        for (var i = 0; i < data.length; i++) {
            var html = '<li style="background-color:white;"><img src="' + data[i].businessImage + '"/>' +
                '<div style="width: 70%; float: left;">' +
                '<h3 class="title">' + data[i].businessName + '</h3>' +
                '<p class="intro">' + data[i].businessAddress + '</p>' +
                '<p class="intro" style="font-size:12px;">距离您大概' + data[i].distance + '公里</p></div></li>';
            $shanghu.append(html);
        }
    }
})

//1、进入页面首先判断该用户是否已经领取过该券，是则按钮变灰不可用 -->
//2、否则用户可领取优惠券，领取之后按钮立即变灰不可用 -->
console.log('全局变量' + discountId);
//进入页面立即判断用户是否已领取过优惠券：
/**
 * 判断是购物优惠券还是新人优惠券
 */
var coupon_info
var c2 = window.location.href.split('&')[1]
if (c2) {
    console.log('ok')
    var c1 = window.location.href.split('&')[0]
    var $id = c1.split('=')[1]
    console.log($id)
    coupon_info = {
        'openid': useropenid,
        'discountId': $id
    }
    $.get(
        'http://wxdshop.natappvip.cc/RedPacketAction/findNewPackage?openid=' + useropenid + '&discountId=' + $id,
        function (content) {
            console.log(content.data)
            var info = content.data
            console.log(info.length)
            if (info.length != 0) {
                $('.submit').text('已领取');
                $('.submit').attr('disabled', true);
                $('.submit').css('background-color', 'gray');
            }
        }
    )

}
else {

    $.ajax({
        url: root + '/RedPacketAction/findNewPackage?openid=' + useropenid + '&discountId=' + discountId,
        success: function (data) {
            console.log(data);
            console.log(data.success);
            //var dd = data.data;
            var hasGet = data.success;
            if (hasGet == true) {
                //表示已领取过优惠券：
                //设置按钮内容为“已领取”
                $('.submit').text('已领取');
                //按钮变灰不可用
                $('.submit').attr('disabled', true);
                $('.submit').css('background-color', 'gray');
            }
        }
    })

}


//还是新用户，未领取过：
var data = {
    "openid": useropenid,
    "discountId": discountId
}
console.log("读取data:" + data.openid + data.discountId);
var info = JSON.stringify(data);

function getPacket() {
    if (c1) {
        $.ajax({
            contentType: "application/json;charset=UTF-8",
            url: 'http://wxdshop.natappvip.cc/RedPacketAction/receive',
            type: 'POST',
            data: JSON.stringify(coupon_info),
            dataType: 'json',
            success: function (data) {
                console.log(data);
                //var content = data.data;
                console.log($('.submit').text());
                //设置按钮内容为“已领取”
                $('.submit').text('已领取');
                //按钮变灰不可用
                $('.submit').attr('disabled', true);
                $('.submit').css('background-color', 'gray');
            },
            error: function () {
                alert("服务器出错了");
            }
        })
    }
    else {
        $.ajax({
            contentType: "application/json;charset=UTF-8",
            url: root + '/RedPacketAction/receive',
            type: 'POST',
            data: info,
            success: function (data) {
                console.log(data);
                //var content = data.data;
                console.log($('.submit').text());
                //设置按钮内容为“已领取”
                $('.submit').text('已领取');
                //按钮变灰不可用
                $('.submit').attr('disabled', true);
                $('.submit').css('background-color', 'gray');
            },
            error: function () {
                alert("服务器出错了");
            }
        })
    }

}
/**
 *显示是试吃商品
 */
$(function () {
    $.get('http://wxdshop.natappvip.cc/BuyerProductInfoAction/findByForetaste',
        function (content) {
            console.log(content.data)
            $('#goods').empty()
            var info = content.data
            $.each(info, function (n) {
                var src = info[n].productIcon.split(';')
                if (n < 3) {
                    var html = '<li class="testify" style="background-color:white;">' +
                        '<div class="am-gallery-item">' +
                        '<a href="../display/detail.html?id=' + info[n].id + '" class="">' +
                        '<img src="' + src[0] +
                        '" />' +
                        '<h3 class="am-gallery-title">【试吃专享】' + info[n].productName + '</h3>' +
                        '<div class="am-gallery-desc">￥' + info[n].productPrice + '</div>' +
                        '</a>' +
                        '</div>' +
                        '</li>'
                    $('#goods').append(html)
                }
            }
            )

        }
    )
    /**
     * 跳转到更多试吃页面
     */
    $('.more_button').on('click', function () {
        window.location.href = '/weixiduo/display/foreTaste'
    }
    )

})
