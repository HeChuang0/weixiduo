var $root='http://wxdshop.natappvip.cc' //保存域名
var openid=getCookie("open");//保存openid
console.log(openid)
$(function(){
    /**
     * 显示用户的默认收货地址
     */
    var $address=" "
    $.get($root+'/AddressAction/findByOpenid?openid='+openid,
        function (content) {
            var info = content.data
            console.log(info)
            $.each(info,function(n){
                if(info[n].checked==1){
                    var html= '<div class="order-name">' +
                    '<p class="order-tele">'+info[n].consignee+
                      '&nbsp;&nbsp;&nbsp;'+info[n].phone+'</p>'+
                    '<p class="order-add">'+info[n].address+'</p>' +
                    '<a href="/weixiduo/address/gladdress"><i class="am-icon-angle-right ">'
                    + '</i>' + '</a>' +
                    '</div>'
                    $('.order-address').empty().append(html)
                    $address=info[n].consignee+info[n].phone+info[n].address
                    
                }
            })
          
})
/**
 * 显示要结算的商品
 */
var $goods=[]//用于存放商品
var totalPrice=0//用于存放商品总价
var total=0//用于存放支付总额
var coupon=0//用于存放优惠券
$.get($root+'/cart/list?openid='+openid,
function(content){
    
    console.log(content.data)
    var info=content.data
    var $data = info[0].cartProductVOList
    $('.shopcart-list').empty()
    $.each($data,function(n){
        if($data[n].productChecked==1){
            var $src=$data[n].productIcon.split(';')
        	var html='<li>'+
                '<img src="'+$src[1]+'" class="shop-pic" />'+
                '<div class="order-mid">'+
                	'<div class="tit">'+$data[n].productName+'</div>'+
                	'<div class="order-price">￥'+$data[n].productPrice+'<i>X'+$data[n].quantity+'</i></div>'+
                '</div>'+
            '</li>'
            $('.shopcart-list').append(html)
            var detail={
                'productIcon':$src[1],
                 'productId':$data[n].productId,
                 'productName':$data[n].productName,
                 'productPrice':$data[n].productPrice,
                 'productQuantity':$data[n].quantity
            }
            $goods.push(detail)
            totalPrice=totalPrice+$data[n].productPrice*$data[n].quantity
            total=totalPrice-coupon
        }
    })
    $('.totalP').html('￥'+totalPrice.toFixed(2))
    $('.totalPrice').html('￥'+total.toFixed(2))
   
})
$('.order-infor-first a').on('click',function(){
    window.location.href = '../coupon/yhq.html?tol='+totalPrice
        
})
var couponId=-1 //保存优惠券id
/* 显示要使用的优惠券 */
var c1=window.location.href.split("?")[1]; 
console.log(c1)
if(c1) {
  c2=c1.split('&')[0]
  c3=c1.split('&')[1]
  c4=c1.split('&')[2]     
    var cashCoupon=c4.split('=')[1]
    var fullCut=c3.split("=")[1]
    var id=c2.split("=")[1]
    console.log(cashCoupon)
    console.log(fullCut)
    coupon=cashCoupon
    $('.coupon').html('满'+fullCut+'减'+cashCoupon+'>')
    couponId=id
    console.log(id)
}



/**
 * 提交订单
 */

$('.js-btn').on('click',function(){
    if(couponId==-1){
        var data={
            'address':$address,
            // 'discountId':0,
            'openid':openid,
            'orderAmount':totalPrice,
            'orderDetails': $goods
    
        }
    }
    else{
           var data={
            'address':$address,
            'discountId':couponId,
            'openid':openid,
            'orderAmount':totalPrice,
            'orderDetails': $goods
        }
    }
    console.log(data);
    //点击提交按钮的时候先判断收货人信息是否为空？
    if($address == null  || $address==" "){
        //收货人信息为空时
        alert("请先完善收货人信息哟");
       //跳转到管理地址界面
        window.location.href='/weixiduo/address/add';
    }

    $.ajax({
            type:'post',
            contentType:'application/json;charset=UTF-8',
            url:$root+'/BuyerOrderAction/create',
            data:JSON.stringify(data),
            dataType:'json',
            success:function(content){
                var info=content.data[0]
                var tol=info.orderAmount
                var order=info.orderid
                console.log(order)
                window.location.href = 'pay.html?tol='+tol+'&&'+'order='+order
            }
        })


})



})

