package com.hechuang.fruitstore.form;

import com.hechuang.fruitstore.enums.AddressStatusEnum;
import lombok.Data;

/**
 * Created by Stefan
 * Create Date 2018-01-17/16:32
 */
@Data
public class AddressForm {
    private Integer id;
    private String openid;//微信号
    private String address;//收货地址
    private String consignee;//收货人
    private String phone;
    private Integer checked= AddressStatusEnum.NOTCHECKED.getCode();//是否默认1是默认地址
}
