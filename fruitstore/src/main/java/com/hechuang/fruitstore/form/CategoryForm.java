package com.hechuang.fruitstore.form;

import lombok.Data;

/**
 * Created by Stefan
 * Create Date 2018-01-17/16:34
 */
@Data
public class CategoryForm {
    protected Integer id;
    private String categoryName;//类目名
}
