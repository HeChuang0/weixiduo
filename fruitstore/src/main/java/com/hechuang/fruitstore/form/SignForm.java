package com.hechuang.fruitstore.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;
import lombok.Data;

import java.util.Date;

/**
 * Created by Stefan
 * Create Date 2018-01-18/1:33
 */
@Data
public class SignForm {
    @NotNull
    private String openid;//
    @NotNull
    private Integer integralQuantity;//签到积分
}
