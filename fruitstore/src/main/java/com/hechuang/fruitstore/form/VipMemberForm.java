package com.hechuang.fruitstore.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Stefan
 * Create Date 2018-01-17/16:36
 */
@Data
public class VipMemberForm {
    protected Integer id;
    @NotEmpty
    private String name;//会员名
    @NotEmpty
    private String sex;//性别，先生，女士
    @NotEmpty
    private String openid;//微信用户唯一标识
    private Integer vipCardId;//vip_card的外键
    private String address;//家庭住址
    private String realName;//真实姓名
    private String birthday;//出生年月
    private Integer haschild;//是否有小孩（0无1有）
    private Integer ismarried;//是否已婚（0未婚，1已婚）
    private String phone;//
}
