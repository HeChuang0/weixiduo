package com.hechuang.fruitstore.form;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by Stefan
 * Create Date 2018-02-26/17:34
 */
@Data
public class BusinessForm {
    private Integer id;
    @NotNull
    private String businessName;
    @NotNull
    private String businessImage;
    @NotNull
    private String businessAddress;
}
