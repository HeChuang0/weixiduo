package com.hechuang.fruitstore.form;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Stefan
 * Create Date 2018-01-17/16:31
 */
@Data
public class ProductForm implements Serializable {
    private static final long serialVersionUID = -8002739623766264692L;
    protected Integer id;
    @NotNull(message = "商品名不能为空")
    private String productName;//商品名
    @NotNull(message = "单价不能为空")
    private BigDecimal productPrice;//单价
    @NotNull(message = "库存不能为空")
    private Integer productStock;//库存
    @NotNull(message = "主类目ID不能为空")
    private Integer categoryId;//类目id
    @NotNull(message = "子类目ID不能为空")
    private Integer subCategoryId;//子类目id
    private String specification;//规格
    private Integer salesVolume;//销量
    private String productDescription;//商品的描述
    private String productIcon;//商品小图
    private Integer productStatus;//状态（0正常，1下架）
}
