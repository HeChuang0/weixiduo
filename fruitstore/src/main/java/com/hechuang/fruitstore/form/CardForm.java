package com.hechuang.fruitstore.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by Stefan
 * Create Date 2018-01-17/16:34
 */
@Data
public class CardForm {
    protected Integer id;
    @NotEmpty
    private String vipName;//会员卡名
    @NotNull
    private Double discount;//会员折扣
}
