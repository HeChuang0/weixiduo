package com.hechuang.fruitstore.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Stefan
 * Create Date 2018-01-17/16:33
 */
@Data
public class DiscountSetForm {
    private Integer id;
    @NotNull(message = "优惠券名不可为空")
    private String discountName;//
    @NotNull(message = "现金券不可为空")
    private BigDecimal cashCoupon;//现金券
    @NotNull(message = "满减标志不可为空")
    private BigDecimal fullCut;//满减标志
    @NotNull(message = "优惠券类型不可为空")
    private Integer type;//0新人红包，1是普通，2大转盘的,3是积分兑换的，4是商成发放的
    @NotNull(message = "开始时间不可为空")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date startTime;//有效期开始时间
    @NotNull(message = "结束时间不可为空")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;//有效期结束时间
}
