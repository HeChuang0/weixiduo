package com.hechuang.fruitstore.form;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Created by Stefan
 * Create Date 2018-02-07/20:22
 */
@Data
public class SubcategoryForm {
    private Integer id;
    @NotNull
    private String subcategoryName;
    @NotNull
    private String categoryImg;
    @NotNull
    private Integer maincategoryId;
}
