package com.hechuang.fruitstore.handler;

import com.hechuang.fruitstore.exception.LoginException;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.ResultVO;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Create by ToFree on 2018/1/19
 */
@ControllerAdvice
public class WeiXiDuoExceptionHandler {
    //拦截登录异常
    @ExceptionHandler(value = LoginException.class)
    public ModelAndView handLoginException() {
        return new ModelAndView("redirect:/");
    }

    @ResponseBody
    @ExceptionHandler(value = WeiXiDuoException.class)
    public ResultVO handWeiXiDuoException(WeiXiDuoException e) {
        return ResultVOUtil.error(e.getMessage());
    }
}
