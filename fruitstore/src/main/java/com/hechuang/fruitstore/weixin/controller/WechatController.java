package com.hechuang.fruitstore.weixin.controller;

import com.hechuang.fruitstore.config.UrlConfig;
import com.hechuang.fruitstore.constant.RedisConstant;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.pojo.VipMember;
import com.hechuang.fruitstore.service.VipMemberServiceDao;
import com.hechuang.fruitstore.utils.CookieUtil;
import com.hechuang.fruitstore.utils.HttpClientUtil;
import com.hechuang.fruitstore.weixin.utils.MessageInit;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;

/**
 * Created by Stefan
 * Create Date 2018-01-18/1:09
 */
@Controller
@RequestMapping("/weixin/auth")
@Slf4j
public class WechatController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private  String openid;
    @Autowired
    WxMpService wxMpService;
    @Autowired
    private WxMpMessageRouter router;

    @Autowired
    VipMemberServiceDao vipMemberServiceDao;

    @Autowired
    MessageInit messageInit;

    @Autowired
    private WxMpService wxOpenService;
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private UrlConfig urlConfig;
    @ApiOperation("微信的第一次握手")
    @GetMapping(produces = "text/plain;charset=utf-8")
    public String authGet(@RequestParam(name = "signature",required = false) String signature, @RequestParam(name = "timestamp", required = false) String timestamp,@RequestParam(name = "nonce", required = false) String nonce, @RequestParam(name = "echostr", required = false) String echostr){
        this.log.info("\n接收到来自微信服务器的认证消息：[{}, {}, {}, {}]", signature,
                timestamp, nonce, echostr);
        if (StringUtils.isAnyBlank(signature, timestamp, nonce, echostr)) {
            throw new IllegalArgumentException("请求参数非法，请核实!");
        }
        log.info("调用了");

        if (wxMpService.checkSignature(timestamp, nonce, signature)) {      log.info("调用了123{}",echostr);
            return echostr;
        }
        log.info("调用了456");
        return "非法请求";
    }
    //信息的交互
    @PostMapping(produces = "application/xml; charset=UTF-8")
    public String post(@RequestBody String requestBody,
                       @RequestParam("signature") String signature,
                       @RequestParam("timestamp") String timestamp,
                       @RequestParam("nonce") String nonce,
                       @RequestParam(name = "encrypt_type",
                               required = false) String encType,
                       @RequestParam(name = "msg_signature",
                               required = false) String msgSignature) {
        this.logger.info(
                "\n接收微信请求：[signature=[{}], encType=[{}], msgSignature=[{}],"
                        + " timestamp=[{}], nonce=[{}], requestBody=[\n{}\n] ",
                signature, encType, msgSignature, timestamp, nonce, requestBody);

        if (!this.wxMpService.checkSignature(timestamp, nonce, signature)) {
            throw new IllegalArgumentException("非法请求，可能属于伪造的请求！");
        }

        String out = null;
        String message = null;
        if (encType == null) {
            // 明文传输的消息
            WxMpXmlMessage inMessage = WxMpXmlMessage.fromXml(requestBody);
            log.info("hahah{}", inMessage);
            WxMpXmlOutMessage outMessage = this.route(inMessage);
            log.info("{}", outMessage);
            if (outMessage == null) {
                log.info("发送自动回复的消息");
                return "你好";
            }

            out = outMessage.toXml();
        } else if ("aes".equals(encType)) {
            log.info("发送自动回复的消息");
            // aes加密的消息
            WxMpXmlMessage inMessage = WxMpXmlMessage.fromEncryptedXml(
                    requestBody, this.wxMpService.getWxMpConfigStorage(), timestamp,
                    nonce, msgSignature);
            log.info("inMessage:{}", inMessage.getEvent());
            if (inMessage.getEvent().equals("subscribe")){
               // message= messageInit.initText(inMessage.getToUser(),inMessage.getFromUser(), messageInit.menuText());
            }
                // this.logger.debug("\n消息解密后内容为：\n{} ", inMessage.toString());
            /*WxMpXmlOutMessage outMessage = route(inMessage);
            log.info("outMessage:和电视机电视机{}",outMessage);
            log.info("{}",inMessage.getEvent().equals("subscribe"));
            if (outMessage == null) {
                return "";
            }

            out = outMessage
                    .toEncryptedXml(this.wxMpService.getWxMpConfigStorage());
        }*/


                log.info("输出的信息{}", message);
           // return out;
        }
        return message;
    }

    private WxMpXmlOutMessage route(WxMpXmlMessage message) {
            try {
                WxMpXmlOutMessage wxMpXmlOutMessage=this.router.route(message);
                return  wxMpXmlOutMessage;
                //return this.router.route(message);
            } catch (Exception e) {
                this.logger.error(e.getMessage(), e);
            }

            return null;
        }
    @ApiOperation("网页授权")
    @GetMapping("/authorize")
    public String authorize(@RequestParam("returnUrl") String returnUrl) {
        //1. 配置
        //2. 调用方法
        String url = urlConfig.getWechatMpAuthorize() + "/weixin/auth/userInfo";
        String redirectUrl = wxMpService.oauth2buildAuthorizationUrl(url, WxConsts.OAuth2Scope.SNSAPI_BASE, returnUrl);
        log.info("开始网页授权");
        log.info(redirectUrl);
        return "redirect:" + redirectUrl;
    }
    //获取用户信息
    @GetMapping("/userInfo")
    public String userInfo(@RequestParam("code") String code,
                           @RequestParam("state") String returnUrl,HttpServletResponse response) {
        WxMpOAuth2AccessToken wxMpOAuth2AccessToken = new WxMpOAuth2AccessToken();
        try {
            wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
        } catch (WxErrorException e) {
            log.error("【微信网页授权】{}", e);
            throw new WeiXiDuoException(ResultEnum.WECHAT_MP_ERROR.getCode(), e.getError().getErrorMsg());
        }


        String openId = wxMpOAuth2AccessToken.getOpenId();
        log.info("openid{}",wxMpOAuth2AccessToken.getOpenId());


        try {
            VipMember pojo=  vipMemberServiceDao.findByOpenid(openId);
            if (pojo==null) {
                WxMpUser userWxInfo = wxMpService.getUserService().userInfo(openId, null);
                //新关注的用户存到数据库中成为一普通会员
                VipMember vipMember = new VipMember();
                vipMember.setOpenid(userWxInfo.getOpenId());
                vipMember.setName(userWxInfo.getNickname());
                vipMember.setSex(userWxInfo.getSex());
                vipMember.setHead(userWxInfo.getHeadImgUrl());
                if (userWxInfo.getCity() != null & userWxInfo.getProvince() != null) {
                    vipMember.setAddress(userWxInfo.getProvince() + userWxInfo.getCity());
                }
                vipMemberServiceDao.addVipMember(vipMember);
                log.error("用户信息：{}", userWxInfo);
            }
        } catch (WxErrorException e) {
            e.printStackTrace();
        }

        return "redirect:" + returnUrl + "?openid=" + openId;
    }







}
