package com.hechuang.fruitstore.weixin.service;

import com.hechuang.fruitstore.dto.OrderDTO;

/**
 * Create by stefan
 * Date on 2018-03-23  19:02
 * Convertion over Configuration!
 */
public interface IPushMessageService {
    /**
     * 订单状态变更消息
     * @param orderDTO
     */
    void orderStatus(OrderDTO orderDTO);
}
