package com.hechuang.fruitstore.weixin.controller;

import com.hechuang.fruitstore.dto.OrderDTO;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.service.OrderMasterServiceDao;
import com.hechuang.fruitstore.utils.JsonUtil;
import com.hechuang.fruitstore.vo.OrderVO;
import com.hechuang.fruitstore.weixin.service.PayServiceDao;
import com.lly835.bestpay.model.PayResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * Created by Stefan
 * Create Date 2018-01-19/18:16
 */
@Controller
@RequestMapping("/pay")
@Slf4j
public class PayController {
    @Autowired
    OrderMasterServiceDao orderMasterServiceDao;
    @Autowired
    PayServiceDao payServiceDao;
    @GetMapping("/create")
    public ModelAndView create(@RequestParam("orderId") String orderId, @RequestParam(value = "returnUrl",required = false) String returnUrl,Map<String,Object> map) {
        log.info("跳转地址returnUrl:{}",returnUrl);
        //1.查订单
        log.info("orderid{}",orderId);
        OrderVO orderVO=orderMasterServiceDao.detail(orderId);
        log.info("orderVO参数：{}",orderVO);
        OrderDTO orderDTO=new OrderDTO();
        BeanUtils.copyProperties(orderVO,orderDTO);
        log.info("openid{}",orderDTO.getOpenid());
        if (orderDTO==null){
            throw new WeiXiDuoException(ResultEnum.ORDER_NOT_EXIST);
        }
        //2.支付
        PayResponse payResponse=payServiceDao.create(orderDTO);
        log.info("微信支付{}", JsonUtil.toJson(payResponse));
        map.put("payResponse",payResponse);
        map.put("returnUrl",returnUrl);
        return new ModelAndView("pay/create",map);
    }

    /**
     * 微信异步通知
     * @param notifyData
     */
    @PostMapping("/notify")
    public ModelAndView notify(@RequestBody String notifyData){
        log.info("进入异步通知");
        payServiceDao.notify(notifyData);
        return new ModelAndView("pay/success");


    }
}
