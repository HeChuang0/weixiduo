package com.hechuang.fruitstore.weixin.handler;
import com.hechuang.fruitstore.pojo.VipMember;
import com.hechuang.fruitstore.service.VipMemberServiceDao;
import com.hechuang.fruitstore.weixin.builder.TextBuilder;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
@Component
@Slf4j
public class SubscribeHandler extends AbstractHandler {
  @Autowired
  VipMemberServiceDao vipMemberServiceDao;

  @Override
  public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,Map<String, Object> context, WxMpService weixinService, WxSessionManager sessionManager) throws WxErrorException {

    this.logger.info("新关注用户 OPENID: " + wxMessage.getFromUser());

    // 获取微信用户基本信息
    WxMpUser userWxInfo = weixinService.getUserService()
        .userInfo(wxMessage.getFromUser(), null);


    if (userWxInfo != null) {
      // TODO 可以添加关注用户到本地
      //新关注的用户存到数据库中成为一普通会员
      VipMember vipMember=new VipMember();
      vipMember.setOpenid(userWxInfo.getOpenId());
      vipMember.setName(userWxInfo.getNickname());
      vipMember.setSex(userWxInfo.getSex());
      vipMember.setHead(userWxInfo.getHeadImgUrl());
      if (userWxInfo.getCity()!=null&userWxInfo.getProvince()!=null){
        vipMember.setAddress(userWxInfo.getProvince()+userWxInfo.getCity());
      }
      vipMemberServiceDao.addVipMember(vipMember);

      log.error("用户信息：{}",userWxInfo);
    }

    WxMpXmlOutMessage responseResult = null;
    try {
      responseResult = handleSpecial(wxMessage);
    } catch (Exception e) {
      this.logger.error(e.getMessage(), e);
    }

    if (responseResult != null) {
      return responseResult;
    }

    try {
      log.info("发送关注消息");
     /* return new TextBuilder().build("亲爱的"+userWxInfo.getNickname()+"，感谢您的关注！维喜多果园餐厅迎来了第一次公测：【维系多1.0】！\n" +
              "【服务体验】：我们致力打造线下新品体验，线上一体化会员服务！线上购买、线下免费体验新品。\n" +
              "【我们主营】：高档水果、国外进口干果小吃、优质水产品、健康农副食。线下体验店还有独具当地特色的小吃、快餐。\n" +
              "维喜多正以星星之火在全国各地扎根，请记住我们的名字【维喜多】！\n" +
              "程序现处于公测阶段，有任何问题请联系我们的客服宝宝，我们会尽快为您解决！", wxMessage, weixinService);*/
     WxMpXmlOutMessage textBuilder=new TextBuilder().build("感谢关注", wxMessage, weixinService);
     log.info("{}",textBuilder);
        return new TextBuilder().build("感谢关注", wxMessage, weixinService);
    } catch (Exception e) {
      this.logger.error(e.getMessage(), e);
    }

    return null;
  }

  /**
   * 处理特殊请求，比如如果是扫码进来的，可以做相应处理
   */
  private WxMpXmlOutMessage handleSpecial(WxMpXmlMessage wxMessage)
      throws Exception {
    //TODO
    return null;
  }

}
