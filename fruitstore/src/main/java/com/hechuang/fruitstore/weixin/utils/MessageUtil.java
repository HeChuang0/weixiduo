package com.hechuang.fruitstore.weixin.utils;

import com.hechuang.fruitstore.weixin.myweixin.NewsMessage;
import com.hechuang.fruitstore.weixin.myweixin.TestMessage;
import com.hechuang.fruitstore.weixin.myweixin.news;
import com.thoughtworks.xstream.XStream;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Stefan
 * Create Date 2017-11-18/11:04
 */
@Component
public class MessageUtil {
    /*
    * 将xml转化为map集合
    * */
    public  Map<String,String> xmlToMap(HttpServletRequest request) throws IOException, DocumentException {
        Map<String,String> map = new HashMap<>();
        SAXReader reader=new SAXReader();
        InputStream ins=request.getInputStream();
        Document doc = reader.read(ins);
        Element root=doc.getRootElement();
        List<Element> list=root.elements();
        for (Element e:list){
            map.put(e.getName(),e.getText());
        }
        ins.close();
        return map;
    }
    /*
    * 将文本对象转为xml
    * */
    public  String textMessageToXml(TestMessage tm){
        XStream xStream=new XStream();
        xStream.alias("xml",tm.getClass());
        return xStream.toXML(tm);
    }

    /*
     * 图文消息转换xml
     *
     * */
    public  String newsMessageToXml(NewsMessage nm){
        XStream xStream=new XStream();
        xStream.alias("xml",nm.getClass());
        xStream.alias("item",new news().getClass());
        return xStream.toXML(nm);
    }
}
