package com.hechuang.fruitstore.weixin.utils;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.bean.menu.WxMenuButton;
import org.springframework.stereotype.Component;

/**
 * Created by Stefan
 * Create Date 2018-01-18/22:58
 */
/*
*菜单的初始化
* */
@Component
public class MenuUtil {
    public WxMenu initMenu(){
        String url="http://wxdshop.natappvip.cc/weixiduo";
        WxMenu menu = new WxMenu();

         /*主菜单 会员中心*/
        WxMenuButton button1 = new WxMenuButton();
        button1.setName("会员中心");
        WxMenuButton button11 = new WxMenuButton();
        button11.setType(WxConsts.MenuButtonType.VIEW);
        button11.setName("会员卡");
        button11.setUrl(url+"/member/vip");
        WxMenuButton button12 = new WxMenuButton();
        button12.setType(WxConsts.MenuButtonType.VIEW);
        button12.setName("新人礼包");
        button12.setUrl(url+"/member/redPacket");
        WxMenuButton button13 = new WxMenuButton();
        button13.setType(WxConsts.MenuButtonType.VIEW);
        button13.setName("新会员推荐");///shopCart/shopcart
        button13.setUrl(url+"/member/share");
        WxMenuButton button14 = new WxMenuButton();
        button14.setType(WxConsts.MenuButtonType.VIEW);
        button14.setName("每日签到");
        button14.setUrl(url+"/member/daily");
        WxMenuButton button15 = new WxMenuButton();
        button15.setName("健康论坛");
        button15.setType(WxConsts.MenuButtonType.VIEW);
        button15.setUrl("http://mp.weixin.qq.com/mp/homepage?__biz=MzUxMDQ0NDcyMA==&hid=1&sn=c7ee4bd124c9ad79692ffeaf4b0c98b2#wechat_redirect");
        button1.getSubButtons().add(button11);
        button1.getSubButtons().add(button12);
        button1.getSubButtons().add(button13);
        button1.getSubButtons().add(button14);
        button1.getSubButtons().add(button15);

        /*主菜单 进入商城*/
        WxMenuButton button2 = new WxMenuButton();
        button2.setName("果园餐厅");

        WxMenuButton button21 = new WxMenuButton();
        button21.setName("新鲜果蔬");
        button21.setType(WxConsts.MenuButtonType.VIEW);
        button21.setUrl(url+"/list/vegetable");

        WxMenuButton button22 = new WxMenuButton();
        button22.setName("干果小食");
        button22.setType(WxConsts.MenuButtonType.VIEW);
        button22.setUrl(url+"/list/dryFruit");

        WxMenuButton button23 = new WxMenuButton();
        button23.setName("水产海鲜");
        button23.setType(WxConsts.MenuButtonType.VIEW);
        button23.setUrl(url+"/list/seafood");

        WxMenuButton button25 = new WxMenuButton();
        button25.setName("进入商城");
        button25.setType(WxConsts.MenuButtonType.VIEW);
        button25.setUrl(url+"/index");

        WxMenuButton button24 = new WxMenuButton();
        button24.setName("乡土特产");
        button24.setType(WxConsts.MenuButtonType.VIEW);
        button24.setUrl(url+"/list/local");
        button2.getSubButtons().add(button25);
        button2.getSubButtons().add(button21);
        button2.getSubButtons().add(button22);
        button2.getSubButtons().add(button23);
        button2.getSubButtons().add(button24);

        /*主菜单 在线服务*/
        WxMenuButton button3 = new WxMenuButton();
        button3.setName("第一时间");

        WxMenuButton button31 = new WxMenuButton();
        button31.setType(WxConsts.MenuButtonType.VIEW);
        button31.setName("在线客服");
        button31.setUrl("http://mp.weixin.qq.com/s/dUS1xX7GndJrGXpt82koEA");
        WxMenuButton button32 = new WxMenuButton();
        button32.setType(WxConsts.MenuButtonType.VIEW);
        button32.setName("我的订单");
        button32.setUrl(url+"/order/allorder");
        WxMenuButton button33 = new WxMenuButton();
        button33.setType(WxConsts.MenuButtonType.VIEW);
        button33.setName("果蔬篮子");
        button33.setUrl(url+"/shopCart/shopcart");
        WxMenuButton button34 = new WxMenuButton();
        button34.setType(WxConsts.MenuButtonType.VIEW);
        button34.setName("我的红包");
        button34.setUrl(url+"/coupon/yhq");
        WxMenuButton button35 = new WxMenuButton();
        button35.setType(WxConsts.MenuButtonType.VIEW);
        button35.setName("线下体验店");
        button35.setUrl(url+"/display/experienceShops");
        button3.getSubButtons().add(button31);
        button3.getSubButtons().add(button32);
        button3.getSubButtons().add(button33);
        button3.getSubButtons().add(button34);
        button3.getSubButtons().add(button35);

        menu.getButtons().add(button1);
        menu.getButtons().add(button2);
        menu.getButtons().add(button3);
        return menu;
    }
}
