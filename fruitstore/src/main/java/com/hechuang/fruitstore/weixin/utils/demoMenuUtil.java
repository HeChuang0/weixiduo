package com.hechuang.fruitstore.weixin.utils;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.bean.menu.WxMenuButton;
import org.springframework.stereotype.Component;

/**
 * Created by Stefan
 * Create Date 2018-01-18/22:58
 */
/*
*菜单的初始化
* */
@Component
public class demoMenuUtil {

    public WxMenu initMenu(){
        String url="http://wxdshop.natappvip.cc/weixiduo";
        WxMenu menu = new WxMenu();

         /*主菜单 会员中心*/
        WxMenuButton button1 = new WxMenuButton();
        button1.setName("会员中心");
        WxMenuButton button11 = new WxMenuButton();
        button11.setType(WxConsts.MenuButtonType.VIEW);
        button11.setName("会员卡");
        button11.setUrl(url+"/member/vip");
        WxMenuButton button12 = new WxMenuButton();
        button12.setType(WxConsts.MenuButtonType.VIEW);
        button12.setName("新人礼包");
        button12.setUrl(url+"/member/redPacket");
        WxMenuButton button13 = new WxMenuButton();
        button13.setType(WxConsts.MenuButtonType.CLICK);
        button13.setName("新会员推荐");
        WxMenuButton button14 = new WxMenuButton();
        button14.setType(WxConsts.MenuButtonType.VIEW);
        button14.setName("签到有礼");
        button14.setUrl(url+"/member/daily");
        button1.getSubButtons().add(button11);
        button1.getSubButtons().add(button12);
        button1.getSubButtons().add(button13);
        button1.getSubButtons().add(button14);

        /*主菜单 进入商城*/
        WxMenuButton button2 = new WxMenuButton();
        button2.setName("进入商城");
        button2.setType(WxConsts.MenuButtonType.CLICK);

        /*主菜单 在线服务*/
        WxMenuButton button3 = new WxMenuButton();
        button3.setName("第一时间");

        WxMenuButton button31 = new WxMenuButton();
        button31.setType(WxConsts.MenuButtonType.CLICK);
        button31.setName("在线客服");
        WxMenuButton button32 = new WxMenuButton();
        button32.setType(WxConsts.MenuButtonType.VIEW);
        button32.setName("我的订单");
        button32.setUrl(url+"/order/allorder");
        WxMenuButton button33 = new WxMenuButton();
        button33.setType(WxConsts.MenuButtonType.CLICK);
        button33.setName("招商加盟");
        WxMenuButton button34 = new WxMenuButton();
        button34.setType(WxConsts.MenuButtonType.VIEW);
        button34.setName("我的红包");
        button34.setUrl(url+"/member/youhui");
        WxMenuButton button35 = new WxMenuButton();
        button35.setType(WxConsts.MenuButtonType.CLICK);
        button35.setName("线下体验店");
        button3.getSubButtons().add(button31);
        button3.getSubButtons().add(button32);
        button3.getSubButtons().add(button33);
        button3.getSubButtons().add(button34);
        button3.getSubButtons().add(button35);

        menu.getButtons().add(button1);
        menu.getButtons().add(button2);
        menu.getButtons().add(button3);
        return menu;
    }
}
