package com.hechuang.fruitstore.weixin.controller;

import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.ResultVO;
import com.hechuang.fruitstore.weixin.service.SDKJSService;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.exception.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Create by stefan
 * Date on 2018-04-11  14:55
 * Convertion over Configuration!
 */
@RestController
@RequestMapping("/sdkjs")
@Slf4j
public class SDKJsController {
    @Autowired
    SDKJSService sdkjsService;
    @GetMapping("/getSignature")
    public ResultVO getSignature(@RequestParam  String url){
        WxJsapiSignature wxJsapiSignature=null;
        try {
            wxJsapiSignature=sdkjsService.createSignature(url);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return ResultVOUtil.success("获得signature成功",wxJsapiSignature);

    }
    @GetMapping("/shareSuccess")
    public ResultVO shareSuccess(@RequestParam String openid){
        log.info("分享成功");
        return  ResultVOUtil.success("分享成功",sdkjsService.shareSuccess(openid));
    }

}
