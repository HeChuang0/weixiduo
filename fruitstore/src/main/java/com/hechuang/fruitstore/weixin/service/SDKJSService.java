package com.hechuang.fruitstore.weixin.service;

import com.hechuang.fruitstore.dao.IntegralDao;
import com.hechuang.fruitstore.dao.RedPacketDao;
import com.hechuang.fruitstore.dao.shareDao;
import com.hechuang.fruitstore.pojo.Integral;
import com.hechuang.fruitstore.pojo.RedPacket;
import com.hechuang.fruitstore.pojo.Share;
import com.hechuang.fruitstore.service.RedPacketServiceDao;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Create by stefan
 * Date on 2018-04-11  14:55
 * Convertion over Configuration!
 */
@Service
public class SDKJSService {
    @Autowired
    WxMpService wxMpService;
    @Autowired
    shareDao shareDao;
    @Autowired
    RedPacketDao redPacketDao;
    @Autowired
    IntegralDao integralDao;
    public WxJsapiSignature createSignature(String url) throws WxErrorException {
        WxJsapiSignature wxJsapiSignature= wxMpService.createJsapiSignature(url);
        return  wxJsapiSignature;
    }


    //重新设计一张表记录分享情况，openid，分享次数
    @Transactional
    public  Share shareSuccess(String openid) {
        //第一次分享获得一张现金券10元的满10.1元可用
        Share share = shareDao.findOne(openid);
        if (share == null) {
            share = new Share();
            share.setOpenid(openid);
            share.setCount(1);
            shareDao.save(share);

            //获得优惠券
            RedPacket redPacket = new RedPacket();
            redPacket.setOpenid(openid);
            redPacket.setDiscountId(10);
            redPacketDao.save(redPacket);

        } else {
            share.setCount(share.getCount() + 1);
            shareDao.save(share);//更改分享次数

            //获得积分10
            //查找是否已经有积分
            Integral integral = integralDao.findByOpenid(openid);
            if (integral == null) {
                integral = new Integral();
                integral.setIntegralQuantity(10);
                integral.setOpenid(openid);
                integralDao.save(integral);
            }
            integral.setIntegralQuantity(integral.getIntegralQuantity() + 10);
            integralDao.save(integral);


            //shareDao.save()
            //以后分享就是加积分，每次10，
        }
        return share;
    }

}
