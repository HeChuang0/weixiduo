package com.hechuang.fruitstore.weixin.utils;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Stefan
 * Create Date 2017-12-04/19:15
 */
@Getter
@Component
@ConfigurationProperties(prefix="MessageSign")
public class MessageSign {
    public  String MESSAGE_TEXT="text";
    public  String MESSAGE_NEWS="news";
    public  String MESSAGE_Image;
    public  String MESSAGE_VOICE;
    public  String MESSAGE_VIDEO;
    public  String MESSAGE_LINK;
    public  String MESSAGE_LOCATION="LOCATION";
    public  String MESSAGE_EVENT="event";
    public  String MESSAGE_SUBSCRIBE="subscribe";
    public  String MESSAGE_UNSUBSCRIBE="unsubscribe";
    public  String MESSAGE_CLICK="CLICK";
    public  String MESSAGE_VIEW="VIEW";
    public  String MESSAGE_SCANCODE;
}
