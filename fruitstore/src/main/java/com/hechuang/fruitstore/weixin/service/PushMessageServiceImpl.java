package com.hechuang.fruitstore.weixin.service;

import com.hechuang.fruitstore.dto.OrderDTO;
import com.hechuang.fruitstore.weixin.config.WechatMpConfiguration;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Create by stefan
 * Date on 2018-03-23  19:08
 * Convertion over Configuration!
 */
public class PushMessageServiceImpl implements IPushMessageService {
    @Autowired
    WxMpService wxMpService;
    @Autowired
    private WechatMpConfiguration wechatMpConfiguration;

    @Override
    public void orderStatus(OrderDTO orderDTO) {

    }

    public WxJsapiSignature createSignature(String url) throws WxErrorException {
        WxJsapiSignature wxJsapiSignature= wxMpService.createJsapiSignature(url);
        return  wxJsapiSignature;
    }
}
