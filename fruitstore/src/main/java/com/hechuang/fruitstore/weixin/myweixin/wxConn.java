package com.hechuang.fruitstore.weixin.myweixin;
import com.hechuang.fruitstore.dao.WxUserLocationDao;
import com.hechuang.fruitstore.pojo.VipMember;
import com.hechuang.fruitstore.pojo.WxUserLocation;
import com.hechuang.fruitstore.service.VipMemberServiceDao;
import com.hechuang.fruitstore.weixin.utils.CheckUtil;
import com.hechuang.fruitstore.weixin.utils.MessageInit;
import com.hechuang.fruitstore.weixin.utils.MessageSign;
import com.hechuang.fruitstore.weixin.utils.MessageUtil;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by Stefan
 * Create Date 2017-11-18/10:44
 */
@RestController
@RequestMapping("/weixin")
@Slf4j
public class wxConn {

    @Autowired
    VipMemberServiceDao vipMemberServiceDao;

    @Autowired
    WxMpService wxMpService;
    @Autowired
    WxUserLocationDao wxUserLocationDao;
    @Autowired
    MessageUtil messageUtil;
    @Autowired
    MessageSign messageSign;
    @Autowired
    MessageInit messageInit;
    @GetMapping("/auth1")
    public String wxcheck(String signature, String timestamp, String nonce, String echostr) {
        if (CheckUtil.checksignature(signature, timestamp, nonce)) {
            return echostr;
        } else {
            return null;
        }
    }

    @PostMapping("/auth")
    public String dealMessage(HttpServletRequest request, HttpServletResponse response) {
      log.info("request:{}",request);
        try {
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            Map<String, String> map = messageUtil.xmlToMap(request);
            log.info("map:{}",map.get("Event"));
            String fromUserName = map.get("FromUserName");
            String toUserName = map.get("ToUserName");
            String msgType = map.get("MsgType");

            String content = map.get("Content");
            String message = null;
           // WxMpUser userWxInfo = wxMpService.getUserService().userInfo(fromUserName,null);
        //   log.info("用户信息{}",userWxInfo);
            if (messageSign.getMESSAGE_TEXT().equals(msgType)) {
            }
            else if (messageSign.getMESSAGE_EVENT().equals(msgType)){
                    String eventType=map.get("Event");
                    if (messageSign.getMESSAGE_SUBSCRIBE().equals(eventType)){
                        WxMpUser userWxInfo = wxMpService.getUserService().userInfo(fromUserName,null);
                            // TODO 可以添加关注用户到本地
                      VipMember pojo=  vipMemberServiceDao.findByOpenid(userWxInfo.getOpenId());
                        if (pojo==null) {
                            //新关注的用户存到数据库中成为一普通会员
                            VipMember vipMember = new VipMember();
                            vipMember.setOpenid(userWxInfo.getOpenId());
                            vipMember.setName(userWxInfo.getNickname());
                            vipMember.setSex(userWxInfo.getSex());
                            vipMember.setHead(userWxInfo.getHeadImgUrl());
                            if (userWxInfo.getCity() != null & userWxInfo.getProvince() != null) {
                                vipMember.setAddress(userWxInfo.getProvince() + userWxInfo.getCity());
                            }
                            vipMemberServiceDao.addVipMember(vipMember);
                            log.error("用户信息：{}", userWxInfo);
                        }
                        message= messageInit.initText(toUserName,fromUserName, messageInit.menuText(userWxInfo.getNickname()));
                    }
                    else if (messageSign.getMESSAGE_LOCATION().equals(eventType)){
                        String label=map.get("Label");
                        System.out.println(map);
                        //TODO  可以将用户地理位置信息保存到本地数据库，以便以后使用
                        WxUserLocation wxUserLocation=new WxUserLocation();
                        wxUserLocation.setOpenid(fromUserName);
                        wxUserLocation.setLat(Double.parseDouble(map.get("Latitude")));
                        wxUserLocation.setLng(Double.parseDouble(map.get("Longitude")));
                        wxUserLocationDao.save(wxUserLocation);
                        log.info("用户地理位置已更新");
                      //  message= messageInit.initText(toUserName,fromUserName,label);
                    }else if (messageSign.MESSAGE_CLICK.equals(eventType)){
                        String eventKey=map.get("EventKey");
                        if (eventKey.equals("31")){
                            message=messageInit.initNewsMessage(toUserName,fromUserName);
                        }
                    }
            }

            System.out.println(message);
            return message;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}



