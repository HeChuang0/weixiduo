package com.hechuang.fruitstore.weixin.utils;
import com.hechuang.fruitstore.weixin.myweixin.NewsMessage;
import com.hechuang.fruitstore.weixin.myweixin.TestMessage;
import com.hechuang.fruitstore.weixin.myweixin.news;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2017-12-04/19:34
 */
@Component
public class MessageInit {
    @Autowired
    MessageSign messageSign;
    @Autowired
    MessageUtil messageUtil;
    /*
    *
    * 主菜单
    * */
    public  String menuText(String username){
        StringBuffer sb=new StringBuffer();
        sb.append("亲爱的"+username+"，感谢您的关注！维系多果园餐厅【1.0正式公测了】！\n公测阶段如有任何意见和建议请尽快联系我们。\n" +
                "维喜多果园餐厅每天都在关心您的果篮子与菜篮子，并随时为您提供最健康的食品！");
        return sb.toString();
    }

    public  String initText(String toUserName,String fromUserName,String content){
        TestMessage tm = new TestMessage();
        tm.setFromUserName(toUserName);
        tm.setToUserName(fromUserName);
        tm.setMsgType(messageSign.getMESSAGE_TEXT());
        tm.setCreateTime(new Date().getTime()+"");
        tm.setContent(content);
        return messageUtil.textMessageToXml(tm);
    }


    public   String initNewsMessage(String toUserName,String fromUserName){
        String message =null;
        List<news> newsList =new ArrayList<>();
        NewsMessage newsMessage = new NewsMessage();

        news n=new news();
        n.setTitle("维喜多客服宝宝");
        n.setDescription("请加客服宝宝的微信，向她咨询物流、退款、商品等信息。当然，当你无聊的时候也可以找我们的客服宝宝听她讲水果与蔬菜的故事哦。只要你想，7*24小时为你在线！么么哒！");
        n.setPicUrl("http://p2ue116au.bkt.clouddn.com/wxd.png");
        n.setUrl("http://p2ue116au.bkt.clouddn.com/wxd.png");

        newsList.add(n);
        newsMessage.setToUserName(fromUserName);
        newsMessage.setFromUserName(toUserName);
        newsMessage.setCreateTime(new Date().getTime()+"");
        newsMessage.setMsgType(messageSign.getMESSAGE_NEWS());
        newsMessage.setArticles(newsList);
        newsMessage.setArticleCount(newsList.size());
        message = messageUtil.newsMessageToXml(newsMessage);
        return message;
    }


}
