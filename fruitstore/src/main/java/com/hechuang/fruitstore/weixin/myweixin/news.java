package com.hechuang.fruitstore.weixin.myweixin;

import lombok.Data;

/**
 * Create by stefan
 * Date on 2018-03-30  21:27
 * Convertion over Configuration!
 */
@Data
public class news {
    private String Title;
    private String Description;
    private String PicUrl;
    private String Url;
}
