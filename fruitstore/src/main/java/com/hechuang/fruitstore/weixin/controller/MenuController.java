package com.hechuang.fruitstore.weixin.controller;

import com.hechuang.fruitstore.weixin.utils.MenuUtil;
import com.hechuang.fruitstore.weixin.utils.demoMenuUtil;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpMenuService;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.menu.WxMpGetSelfMenuInfoResult;
import me.chanjar.weixin.mp.bean.menu.WxMpMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Stefan
 * Create Date 2018-01-18/22:32
 */

/*
*
* 自定义菜单创建
* */
@RestController
@RequestMapping("/menu")
@Slf4j
public class MenuController {
    @Autowired
    private WxMpService wxService;
    @Autowired
    MenuUtil menuUtil;

    @PostMapping("/create")
    public String menuCreate() throws WxErrorException {
        return wxService.getMenuService().menuCreate(menuUtil.initMenu());
    }

}
