package com.hechuang.fruitstore.weixin.myweixin;

import lombok.Data;

/**
 * Created by Stefan
 * Create Date 2017-11-18/11:16
 */
@Data
public class TestMessage {
    private String ToUserName;
    private String FromUserName;
    private String CreateTime;
    private String MsgType;
    private String MsgId;
    private String Content;
}
