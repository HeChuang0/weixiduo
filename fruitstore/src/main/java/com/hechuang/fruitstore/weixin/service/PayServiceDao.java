package com.hechuang.fruitstore.weixin.service;

import com.hechuang.fruitstore.dto.OrderDTO;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.service.OrderMasterServiceDao;
import com.hechuang.fruitstore.service.WebSocket;
import com.hechuang.fruitstore.utils.JsonUtil;
import com.hechuang.fruitstore.utils.MathUtil;
import com.hechuang.fruitstore.vo.OrderVO;
import com.lly835.bestpay.enums.BestPayTypeEnum;
import com.lly835.bestpay.model.PayRequest;
import com.lly835.bestpay.model.PayResponse;
import com.lly835.bestpay.model.RefundRequest;
import com.lly835.bestpay.model.RefundResponse;
import com.lly835.bestpay.service.impl.BestPayServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Stefan
 * Create Date 2018-01-19/18:24
 */
@Slf4j
@Service
public class PayServiceDao {
    @Autowired
    BestPayServiceImpl bestPayService;
    @Autowired
    OrderMasterServiceDao orderMasterServiceDao;

    @Autowired
    private WebSocket webSocket;
    /*
    * 创建支付
    * */
    public PayResponse create(OrderDTO orderDTO){
        log.info("订单信息{}",orderDTO);
        PayRequest payRequest=new PayRequest();
        payRequest.setOpenid(orderDTO.getOpenid());
        payRequest.setOrderAmount(orderDTO.getOrderAmount().doubleValue());
        payRequest.setOrderId(orderDTO.getOrderid());
        payRequest.setOrderName("维喜多果园餐厅订单");
        payRequest.setPayTypeEnum(BestPayTypeEnum.WXPAY_H5);
        log.info("微信支付{}", JsonUtil.toJson(payRequest));
        PayResponse payResponse=bestPayService.pay(payRequest);
        return payResponse;
    }
/*
* 微信异步通知
* */
    public PayResponse notify(String notifyData) {
        //1. 验证签名
        //2. 支付的状态
        //3. 支付金额
        //4. 支付人(下单人 == 支付人)

        PayResponse payResponse = bestPayService.asyncNotify(notifyData);
        log.info("【微信支付】异步通知, payResponse={}", JsonUtil.toJson(payResponse));

        //查询订单
        OrderVO orderVO = orderMasterServiceDao.detail(payResponse.getOrderId());
        OrderDTO orderDTO=new OrderDTO();
        BeanUtils.copyProperties(orderVO,orderDTO);

        //判断订单是否存在
        if (orderDTO == null) {
            log.error("【微信支付】异步通知, 订单不存在, orderId={}", payResponse.getOrderId());
            throw new WeiXiDuoException(ResultEnum.ORDER_NOT_EXIST);
        }

        //判断金额是否一致(0.10   0.1)
        if (!MathUtil.equals(payResponse.getOrderAmount(), orderDTO.getOrderAmount().doubleValue())) {
            log.error("【微信支付】异步通知, 订单金额不一致, orderId={}, 微信通知金额={}, 系统金额={}",
                    payResponse.getOrderId(),
                    payResponse.getOrderAmount(),
                    orderDTO.getOrderAmount());
            throw new WeiXiDuoException(ResultEnum.WXPAY_NOTIFY_MONEY_VERIFY_ERROR);
        }

        //修改订单的支付状态
        orderMasterServiceDao.paid(orderDTO);

        //发送websocket消息
        webSocket.sendMessage(orderDTO.getOrderid());

        return payResponse;
    }

    /**
     * 退款
     * @param orderDTO
     */
    public RefundResponse refund(OrderDTO orderDTO) {
        RefundRequest refundRequest = new RefundRequest();
        refundRequest.setOrderId(orderDTO.getOrderid());
        refundRequest.setOrderAmount(orderDTO.getOrderAmount().doubleValue());
        refundRequest.setPayTypeEnum(BestPayTypeEnum.WXPAY_H5);
        log.info("【微信退款】request={}", JsonUtil.toJson(refundRequest));

        RefundResponse refundResponse = bestPayService.refund(refundRequest);
        log.info("【微信退款】response={}", JsonUtil.toJson(refundResponse));

        return refundResponse;
    }
}
