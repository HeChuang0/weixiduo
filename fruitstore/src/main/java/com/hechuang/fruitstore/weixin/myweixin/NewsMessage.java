package com.hechuang.fruitstore.weixin.myweixin;

import lombok.Data;

import java.util.List;

/**
 * Create by stefan
 * Date on 2018-03-30  21:28
 * Convertion over Configuration!
 */
@Data
public class NewsMessage {
    private String ToUserName;
    private String FromUserName;
    private String CreateTime;
    private String MsgType;
    private String MsgId;
    private int ArticleCount;
    private List<news> Articles;
}
