package com.hechuang.fruitstore.weixin.config;

import com.lly835.bestpay.config.WxPayH5Config;
import com.lly835.bestpay.service.impl.BestPayServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created by Stefan
 * Create Date 2018-01-19/18:50
 */
@Component
public class WechatPayConfig {
    @Autowired
    private WechatMpProperties wechatMpProperties;

    @Bean
    public BestPayServiceImpl bestPayService() {
        BestPayServiceImpl bestPayService = new BestPayServiceImpl();
        bestPayService.setWxPayH5Config(wxPayH5Config());
        return bestPayService;
    }

    @Bean
    public WxPayH5Config wxPayH5Config() {
        WxPayH5Config wxPayH5Config = new WxPayH5Config();
        wxPayH5Config.setAppId(wechatMpProperties.getAppId());
        wxPayH5Config.setAppSecret(wechatMpProperties.getSecret());
        wxPayH5Config.setMchId(wechatMpProperties.getMchId());
        wxPayH5Config.setMchKey(wechatMpProperties.getMchKey());
        wxPayH5Config.setKeyPath(wechatMpProperties.getKeyPath());
        wxPayH5Config.setNotifyUrl(wechatMpProperties.getNotifyUrl());
        return wxPayH5Config;
    }
}
