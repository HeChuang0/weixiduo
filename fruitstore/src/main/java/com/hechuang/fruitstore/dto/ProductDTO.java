package com.hechuang.fruitstore.dto;

import lombok.Data;

/**
 * Created by Stefan
 * Create Date 2018-01-17/0:24
 */
@Data
public class ProductDTO {
    /** 商品Id. */
    private Integer productId;

    /** 数量. */
    private Integer productQuantity;

    public ProductDTO(Integer productId, Integer productQuantity) {
        this.productId = productId;
        this.productQuantity = productQuantity;
    }
}
