package com.hechuang.fruitstore.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hechuang.fruitstore.enums.OrderStatusEnum;
import com.hechuang.fruitstore.enums.PayStatusEnum;
import com.hechuang.fruitstore.pojo.OrderDetail;
import com.hechuang.fruitstore.utils.EnumUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-01-16/22:48
 */
@Data
public class OrderDTO {
    /** 订单id. */
    private Integer discountId;
    private String orderid;
    @NotNull(message = "收货地址不能为空")
    private String address;//买家地址
    @NotNull(message = "买家微信不能为空")
    private String openid;//买家微信openid
    @NotNull(message = "订单总金额不能为空")
    private BigDecimal orderAmount;//订单总金额
    private Integer orderStatus= OrderStatusEnum.NEW.getCode();//订单状态, 默认为新下单
    private Integer payStatus= PayStatusEnum.WAIT.getCode();//支付状态, 默认未支付
    @NotNull(message = "商品list不能为空")
    private List<OrderDetail> orderDetails;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime=new Date();//

    @JsonIgnore
    public OrderStatusEnum getOrderStatusEnum() {
        return EnumUtil.getByCode(orderStatus, OrderStatusEnum.class);
    }

    @JsonIgnore
    public PayStatusEnum getPayStatusEnum() {
        return EnumUtil.getByCode(payStatus, PayStatusEnum.class);
    }
}
