package com.hechuang.fruitstore.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by Stefan
 * Create Date 2018-02-07/20:09
 */
@Data
@Entity
public class ProductSubcategory {
    @Id
    @GeneratedValue
    private Integer id;
    private String subcategoryName;
    private String categoryImg;
    private Integer maincategoryId;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;//
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime = new Date();//

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
