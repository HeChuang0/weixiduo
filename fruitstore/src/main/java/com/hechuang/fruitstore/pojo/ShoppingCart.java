package com.hechuang.fruitstore.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Value;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
public class ShoppingCart {
    @Id
    @GeneratedValue
    protected Integer id;
    @Column(name = "openid")
    private String openid;//
    @Column(name = "product_id")
    private Integer productId;//商品Id
    private Integer productQuantity;//商品数量
    private Integer cartStatus;//是否选择，0=未勾选，1=已勾选
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;//
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime = new Date();//

}