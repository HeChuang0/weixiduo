package com.hechuang.fruitstore.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class Sign {
    @Id
    @GeneratedValue
    protected Integer id;
    private String openid;//
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date signDate = new Date();//签到时间


}