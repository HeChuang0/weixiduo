package com.hechuang.fruitstore.pojo;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @Author: xht
 * @Date: 15:37 2018/2/27
 */
@Data
@Entity
public class District {

    @Id
    @GeneratedValue
    Integer id;
    String name;
    Integer parentId;
}
