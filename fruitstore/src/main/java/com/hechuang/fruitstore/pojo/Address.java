package com.hechuang.fruitstore.pojo;

import com.hechuang.fruitstore.enums.AddressStatusEnum;
import lombok.Data;
import org.springframework.data.jpa.repository.Modifying;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Data
public class Address implements Serializable{
    private static final long serialVersionUID = -3602794416331385461L;
    @Id
    @GeneratedValue
    private Integer id;
    @NotNull(message = "openid不能为空")
    private String openid;//微信号
    private String address;//收货地址
    private String consignee;//收货人
    private String phone;
    private Integer checked= AddressStatusEnum.NOTCHECKED.getCode();//是否默认1是默认地址
}