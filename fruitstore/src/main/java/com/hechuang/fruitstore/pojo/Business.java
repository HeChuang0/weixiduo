package com.hechuang.fruitstore.pojo;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Stefan
 * Create Date 2018-02-26/17:16
 */
@Data
@Entity
public class Business {
    @Id
    @GeneratedValue
    private Integer id;
    private String businessName;
    private String businessImage;
    private String businessAddress;
}
