package com.hechuang.fruitstore.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class Manager {
    @Id
    @GeneratedValue
    protected Integer id;
    private String managerName;//
    //private String phone;//可用手机号作为登录账号
    private String managerPassword;//
    private String openid;//
    private Date createTime;//
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime = new Date();//

}