package com.hechuang.fruitstore.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
@Entity
@Data
public class OrderDetail {
    @Id
    @GeneratedValue
    protected Integer id;
    private String orderid;//订单id
    private Integer productId;//商品id
    private String productName;//商品名
    private BigDecimal productPrice;//商品单价
    private BigDecimal discountPrice;//打折后的价钱
    private Integer productQuantity;//商品数量
    private String productIcon;//商品小图
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime=new Date();//
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime = new Date();//
}