package com.hechuang.fruitstore.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
public class VipMember implements Serializable {
    private static final long serialVersionUID = 341561665861762110L;
    @Id
    @GeneratedValue
    protected Integer id;
    private Integer vipCardId;//vip_card的外键
    private String name;//会员名
    private String sex;//性别，先生，女士
    private String head;//头像
    private String address;//家庭住址
    private String realName;//真实姓名
    private String birthday;//出生年月
    private Integer haschild;//是否有小孩（0无1有）
    private Integer ismarried;//是否已婚（0未婚，1已婚）
    private String phone;//手机号
    private String openid;//微信用户唯一标识
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;//
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime = new Date();//

}