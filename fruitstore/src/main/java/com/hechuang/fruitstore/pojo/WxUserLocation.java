package com.hechuang.fruitstore.pojo;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Stefan
 * Create Date 2018-02-27/19:16
 */
@Data
@Entity
public class WxUserLocation {
    @Id
    private String openid;
    private double lat;
    private double lng;

}
