package com.hechuang.fruitstore.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
public class DiscountSet implements Serializable {
    private static final long serialVersionUID = 3049649010243779908L;
    @Id
    @GeneratedValue
    protected Integer id;
    private String discountName;//
    private BigDecimal cashCoupon;//现金券
    private BigDecimal fullCut;//满减标志
    private Integer type;//0新人红包，1是普通，2大转盘的,3是积分兑换的，4是商成发放的
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date startTime;//有效期开始时间
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;//有效期结束时间

}