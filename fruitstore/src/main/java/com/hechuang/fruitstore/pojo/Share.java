package com.hechuang.fruitstore.pojo;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Create by stefan
 * Date on 2018-04-11  16:14
 * Convertion over Configuration!
 */
@Data
@Entity
public class Share {
    @Id
    private String openid;
    private Integer count;
}
