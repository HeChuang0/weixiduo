package com.hechuang.fruitstore.pojo;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class VipCard {
    @Id
    @GeneratedValue
    protected Integer id;
    private String vipName;//会员卡名
    private Double discount;//会员折扣
    private String description;//会员卡特权描述

}