package com.hechuang.fruitstore.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OrderBy;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
public class OrderMaster  implements Serializable {
    private static final long serialVersionUID = 1739797460495394363L;
    @Id
    private String orderid;//以时间生成的字符串
    private String openid;//
    private Integer discountId;//优惠券id
    private String address;//收货地址
    private BigDecimal orderAmount;//金额总数
    private Integer orderStatus;//订单状态（0新订单，1订单完结）
    private Integer payStatus;//支付状态（0未支付，1已支付）
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;//
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime = new Date();//
}