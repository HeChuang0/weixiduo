package com.hechuang.fruitstore.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hechuang.fruitstore.enums.RedPacketUseTypeEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
public class RedPacket {
    @Id
    @GeneratedValue
    protected Integer id;
    private String openid;//
    private Integer discountId;
    private Integer useType= RedPacketUseTypeEnum.NOUSE.getCode(); //0未使用，1使用，2过期

}