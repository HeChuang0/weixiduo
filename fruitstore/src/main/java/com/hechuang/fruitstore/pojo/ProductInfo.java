package com.hechuang.fruitstore.pojo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.hechuang.fruitstore.enums.ProductStatusEnum;
import com.hechuang.fruitstore.form.ProductForm;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
public class ProductInfo implements Serializable{

    private static final long serialVersionUID = 6495377364284824983L;
    @Id
    @GeneratedValue
    protected Integer id;
    private String productName;//商品名
    private BigDecimal productPrice;//单价
    private String specification;//规格
    private Integer productStock;//库存
    private String productDescription;//商品的描述
    private String productIcon;//商品小图
    private Integer productStatus;//状态（0正常，1下架）
    private Integer categoryId;//主类目id
    private Integer subCategoryId;//子类目id

    private Integer salesVolume=0;//销量

    private Integer foretaste= ProductStatusEnum.NOTFORETASTE.getCode();//试吃活动（默认是未加入试吃活动）

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;//
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime = new Date();//

}