package com.hechuang.fruitstore.enums;

/**
 * Create by ToFree on 2018/1/11
 */
public interface CodeEnum {
    Integer getCode();
}
