package com.hechuang.fruitstore.enums;

import lombok.Getter;

/**商品状态
 * Create by ToFree on 2018/1/11
 */
@Getter
public enum ProductStatusEnum implements CodeEnum {
    UP(0, "在架"),
    DOWN(1, "下架"),
    ISFORETASTE(0,"加入试吃活动"),
    NOTFORETASTE(1,"未加入试吃活动")
    ;

    private Integer code;

    private String message;

    ProductStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }


}
