package com.hechuang.fruitstore.enums;

import lombok.Getter;

/**付款状态
 * Create by ToFree on 2018/1/11
 */
@Getter
public enum PayStatusEnum implements CodeEnum {

    WAIT(0, "等待支付"),
    SUCCESS(1, "支付成功"),
    REFUND(2, "已退款"),

    ;

    private Integer code;

    private String message;

    PayStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
