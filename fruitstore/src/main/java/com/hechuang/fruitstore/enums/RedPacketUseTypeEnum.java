package com.hechuang.fruitstore.enums;

import lombok.Getter;

/**
 * Created by Stefan
 * Create Date 2018-01-26/12:40
 */
@Getter
public enum RedPacketUseTypeEnum {
    NOUSE(0, "未使用"),
    USEED(1, "已使用"),
    OVERDUE(2,"过期")
    ;
    private Integer code;

    private String message;

    RedPacketUseTypeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
