package com.hechuang.fruitstore.enums;

import com.lly835.bestpay.rest.type.Get;
import lombok.Getter;

/**
 * Create by ToFree on 2018/1/15
 */
@Getter
public enum ShoppingCartEnum implements CodeEnum {
    UNCHECKED(0, "未选中"),
    CHECKED(1, "已选中"),
    ;
    private Integer code;

    private String message;

    ShoppingCartEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
