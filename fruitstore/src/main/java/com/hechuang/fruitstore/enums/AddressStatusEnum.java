package com.hechuang.fruitstore.enums;

import lombok.Getter;

/**
 * Created by Stefan
 * Create Date 2018-02-08/22:15
 */
@Getter
public enum AddressStatusEnum implements CodeEnum {
    CHECKED(1,"默认地址"),
    NOTCHECKED(0,"普通地址")
    ;

    private Integer code;

    private String message;

    AddressStatusEnum(Integer code,String message){
        this.code = code;
        this.message = message;
    }

}
