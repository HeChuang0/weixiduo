package com.hechuang.fruitstore.enums;

import lombok.Getter;

/**
 * Created by Stefan
 * Create Date 2018-03-09/18:21
 */
@Getter
public enum DiscountCodeEnum {
    NEW_DISCOUNT(0,"新人红包"),
    COMMON_DISCOUNT(1,"普通红包"),
    TURNTABLE_DISCOUNT(2,"转盘红包"),
    INTEGRAL_DISCOUNT(3,"积分红包"),
    MARKET_DISCOUNT(4,"商城红包");
    private Integer code;

    private String message;

    DiscountCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
