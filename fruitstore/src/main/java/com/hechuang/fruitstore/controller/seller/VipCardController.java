package com.hechuang.fruitstore.controller.seller;

import com.hechuang.fruitstore.form.CardForm;
import com.hechuang.fruitstore.pojo.VipCard;
import com.hechuang.fruitstore.service.VipCardServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/VipCardAction")
public class VipCardController {

    @Autowired
    VipCardServiceDao vipCardServiceDao;

    @ApiOperation(value="创建会员卡等级", notes="创建会员卡等级")
    @PostMapping("/add")
    public ResultVO add(@RequestBody(required = false) @Validated CardForm cardForm, BindingResult bindingResult){
        if(bindingResult.hasErrors()||cardForm==null){
            return ResultVOUtil.error("会员卡信息不完整");
        }
        VipCard data = vipCardServiceDao.save(cardForm);
        return ResultVOUtil.success("添加成功",data);
    }

    @ApiOperation(value="修改会员卡等级", notes="修改会员卡等级")
    @PostMapping("/update")
    public ResultVO update(@RequestBody(required = false) @Validated CardForm cardForm, BindingResult bindingResult){
        if(bindingResult.hasErrors()||cardForm==null){
            return ResultVOUtil.error("会员卡信息不完整");
        }
        VipCard data = vipCardServiceDao.save(cardForm);
        return ResultVOUtil.success("修改成功",data);
    }

    @ApiOperation(value="删除会员卡等级", notes="删除会员卡等级")
    @GetMapping("/delete")
    public ResultVO delete(Integer id){
        if(id==null){
           return ResultVOUtil.error("id未传");
        }
        vipCardServiceDao.delete(id);
        return ResultVOUtil.success("删除成功");
    }

    @ApiOperation(value="会员卡等级列表", notes="查看会员卡等级列表")
    @GetMapping("/get")
    public LayuiResultVo get(){
        List<VipCard> data = (List<VipCard>) vipCardServiceDao.get().get("list");
        return new LayuiResultVo<VipCard>(200,"查询成功",data, (Long) vipCardServiceDao.get().get("size"));
    }

    @ApiOperation(value="会员卡等级详情", notes="查看会员卡等级详情")
    @GetMapping("/findById")
    public ResultVO getById(Integer id){
        if(id==null){
            return ResultVOUtil.error("id未传");
        }
        VipCard data = vipCardServiceDao.getById(id);
        System.out.println(data);
        if(data==null){
            return ResultVOUtil.error("查询失败");
        }
        return ResultVOUtil.success("查询成功",data);
    }

}
