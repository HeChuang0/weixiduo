package com.hechuang.fruitstore.controller;

import com.hechuang.fruitstore.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Stefan
 * Create Date 2018-01-18/10:20
 */
@Controller
@Slf4j
public class demo {
    @RequestMapping("/order")
    public String web(){
        return "admin/message";
    }
    @GetMapping("/hello")
    public String  checkOpenid(@RequestParam String openid){
        return openid;
    }
}
