package com.hechuang.fruitstore.controller.buyer;

import com.hechuang.fruitstore.dao.WxUserLocationDao;
import com.hechuang.fruitstore.pojo.WxUserLocation;
import com.hechuang.fruitstore.service.BusinessServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.ResultVO;
import com.hechuang.fruitstore.weixin.handler.LocationHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Stefan
 * Create Date 2018-02-26/19:43
 */
@RestController
@RequestMapping("/BuyerBusinessAction")
public class BuyerBusinessController {
    @Autowired
    BusinessServiceDao businessServiceDao;
    @Autowired
    LocationHandler locationHandler;
    @Autowired
    WxUserLocationDao wxUserLocationDao;
    @GetMapping("/nearbyBusinesses")
    public ResultVO nearbyBusinesses(@RequestParam String openid){
        WxUserLocation wxUserLocation=wxUserLocationDao.findOne(openid);
        return ResultVOUtil.success("查询成功",businessServiceDao.nearbyBusinesses(wxUserLocation.getLat(),wxUserLocation.getLng()));
    }
}
