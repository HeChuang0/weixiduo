package com.hechuang.fruitstore.controller.seller;

import com.hechuang.fruitstore.form.VipMemberForm;
import com.hechuang.fruitstore.pojo.VipMember;
import com.hechuang.fruitstore.query.VipMemberQueryParams;
import com.hechuang.fruitstore.service.VipMemberServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Stefan
 * Create Date 2018-01-15/0:11
 */
@Slf4j
@RestController
@RequestMapping("/VipMemberAction")
public class VipMemberController {
    @Autowired
    VipMemberServiceDao vipMemberServiceDao;

    /*@ApiOperation(value="用户微信第一次登录创建会员", notes="用户微信第一次登录创建会员")
    @PostMapping("/add")
    public ResultVO save(@RequestBody(required = false) @Validated VipMemberForm vipMemberForm, BindingResult bindingResult){
        if(bindingResult.hasErrors()||vipMemberForm==null){
            log.error("会员信息不完整");
            return ResultVOUtil.error("会员信息不完整");
        }
        VipMember data=vipMemberServiceDao.addVipMember(vipMemberForm);
        if (data==null){
            log.error("创建会员失败");
            return ResultVOUtil.error("创建会员失败");
        }
        log.error((data==null)+"");
        return ResultVOUtil.success("创建成功",vipMemberForm);

    }*/
    @ApiOperation(value="商家后台查看所有会员", notes="商家后台查看所有会员")
    @GetMapping("/get")
    public LayuiResultVo get(VipMemberQueryParams params){
        Map map = vipMemberServiceDao.get(params);
        List list = (List) map.get("list");
        if (list != null) {
            return new LayuiResultVo(200, "查询成功", list, (Long) map.get("size"));
        }
        return new LayuiResultVo(200, "查询失败！");
    }
}
