package com.hechuang.fruitstore.controller.buyer;

import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.enums.ShoppingCartEnum;
import com.hechuang.fruitstore.pojo.ShoppingCart;
import com.hechuang.fruitstore.service.CartServiceDao;
import com.hechuang.fruitstore.utils.ListUtil;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.utils.StringUtil;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;



/**
 * Create by ToFree on 2018/1/15
 */
@Slf4j
@RestController
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartServiceDao cartServiceDao;

    @GetMapping("/add")
   // @CachePut(cacheNames = "Carts", key = "#openid", unless = "#result.getSuccess() == false ")
    @ApiOperation("将商品加入购物车")
    public ResultVO add(@RequestParam String openid, @RequestParam Integer productId, @RequestParam Integer count) {
        log.info("openid:{},productId{},count{}",openid,productId,count);
        if (StringUtil.isEmpty(openid) || productId == null || count == null) {
            log.error("添加购物车失败：参数不正确");
            log.info(openid, productId, count);
            return ResultVOUtil.error(ResultEnum.PARAM_ERROR.toString());
        }
        return cartServiceDao.add(openid, productId, count);
    }

    @GetMapping("/update")
    //@CachePut(cacheNames = "Carts", key = "#openid", unless = "#result.getSuccess() == false ")
    @ApiOperation("更新购物车")
    public ResultVO update(@RequestParam String openid, @RequestParam Integer productId, @RequestParam Integer count) {
        log.info("{},{},{}",openid, productId, count);
        if (StringUtil.isEmpty(openid) || productId == null || count == null) {
            log.error("更新购物车失败：参数不正确");
            log.info(openid, productId, count);
            return ResultVOUtil.error(ResultEnum.PARAM_ERROR.toString());
        }
        return cartServiceDao.update(openid, productId, count);
    }

    @GetMapping("/deleteProduct")
   // @CachePut(cacheNames = "Carts", key = "#openid", unless = "#result.getSuccess() == false ")
    @ApiOperation("删除购物车商品" )
    public ResultVO deleteProduct(@RequestParam String openid, @RequestParam String productIds) {
        if (StringUtil.isEmpty(openid) || StringUtil.isEmpty(productIds)) {
            log.error("删除购物车失败：参数不正确");
            return ResultVOUtil.error(ResultEnum.PARAM_ERROR.toString());
        }
        log.info(productIds);
        return cartServiceDao.deleteProduct(openid, productIds);
    }

    @GetMapping("/list")
    //@Cacheable(cacheNames = "Carts", key = "#openid", unless = "#result.getSuccess() == false ")
    @ApiOperation("查询购物车")
    public ResultVO list(@RequestParam String openid) {
        if (StringUtil.isEmpty(openid)) {
            log.error("查询购物车失败：参数不正确");
            return ResultVOUtil.error(ResultEnum.PARAM_ERROR.toString());
        }
        return cartServiceDao.list(openid);
    }

    @GetMapping("/selectAll")
   // @CachePut(cacheNames = "Carts", key = "#openid", unless = "#result.getSuccess() == false ")
    @ApiOperation("全选购物车")
    public ResultVO selectAll(@RequestParam String openid,@RequestParam  Integer status) {
        if (StringUtil.isEmpty(openid)) {
            log.error("全选购物车失败：参数不正确");
            return ResultVOUtil.error(ResultEnum.PARAM_ERROR.toString());
        }
        if (ShoppingCartEnum.UNCHECKED.getCode().equals(status)){
            return cartServiceDao.selectOrUnselectAll(openid, ShoppingCartEnum.UNCHECKED.getCode());
        }
        return cartServiceDao.selectOrUnselectAll(openid, ShoppingCartEnum.CHECKED.getCode());
    }

   /* @GetMapping("/unSelectAll")
    //@CachePut(cacheNames = "Carts", key = "#openid", unless = "#result.getSuccess() == false ")
    @ApiOperation("反全选购物车")
    public ResultVO unSelectAll(@RequestParam String openid) {
        if (StringUtil.isEmpty(openid)) {
            log.error("反全选购物车：参数不正确");
            return ResultVOUtil.error(ResultEnum.PARAM_ERROR.toString());
        }
        return cartServiceDao.selectOrUnselectAll(openid, ShoppingCartEnum.UNCHECKED.getCode());
    }*/

    @GetMapping("/selectProduct")
    //@CachePut(cacheNames = "Carts", key = "#openid", unless = "#result.getSuccess() == false ")
    @ApiOperation("选择购物车商品")
    public ResultVO selectProduct(@RequestParam String openid, @RequestParam Integer productId,@RequestParam Integer status) {
        if (StringUtil.isEmpty(openid) || productId == null) {
            log.error("选择购物车商品失败：参数不正确");
            return ResultVOUtil.error(ResultEnum.PARAM_ERROR.toString());
        }
        if (ShoppingCartEnum.UNCHECKED.getCode().equals(status)){
            return cartServiceDao.selectOrUnselect(openid, productId ,ShoppingCartEnum.UNCHECKED.getCode());
        }
        return cartServiceDao.selectOrUnselect(openid, productId ,ShoppingCartEnum.CHECKED.getCode());
    }

    /*@GetMapping("/unSelectProduct")
    //@CachePut(cacheNames = "Carts", key = "#openid", unless = "#result.getSuccess() == false ")
    @ApiOperation("取消选择购物车商品")
    public ResultVO unSelectProduct(@RequestParam String openid, @RequestParam Integer productId) {
        if (StringUtil.isEmpty(openid) || productId == null) {
            log.error("取消选择购物车商品失败：参数不正确");
            return ResultVOUtil.error(ResultEnum.PARAM_ERROR.toString());
        }
        return cartServiceDao.selectOrUnselect(openid, productId, ShoppingCartEnum.UNCHECKED.getCode());
    }*/

    @GetMapping("/getCartProductCount")
    @ApiOperation("查询购物车商品数量")
    public ResultVO getCartProductCount(@RequestParam String openid) {
        if (StringUtil.isEmpty(openid)) {
            log.error("查询购物车商品数量失败：参数不正确");
            return ResultVOUtil.error(ResultEnum.PARAM_ERROR.toString());
        }
        ResultVO resultVO = new ResultVO("查询成功");
        resultVO.setTotal(cartServiceDao.getCartProductCount(openid));
        return resultVO;
    }

    @GetMapping("/getCartProduct")
    @ApiOperation("查询购物车商品")
    public ResultVO getCartProduct(@RequestParam String openid, @RequestParam Integer productId) {
        if (StringUtil.isEmpty(openid)) {
            log.error("查询购物车商品失败：参数不正确");
            return ResultVOUtil.error(ResultEnum.PARAM_ERROR.toString());
        }
        return cartServiceDao.getCartByProductIdOpenid(openid, productId);
    }

//    @GetMapping("/createOrder")
//    public ResultVO createOrder(@RequestParam String openid) {
//        if (StringUtil.isEmpty(openid)) {
//            log.error("创建订单失败：参数不正确");
//            return ResultVOUtil.error(ResultEnum.PARAM_ERROR.toString());
//        }
//
//        return null;
//    }


}
