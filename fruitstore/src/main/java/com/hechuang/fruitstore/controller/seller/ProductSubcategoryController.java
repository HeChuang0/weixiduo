package com.hechuang.fruitstore.controller.seller;

import com.hechuang.fruitstore.dao.ProductSubcategoryDao;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.form.SubcategoryForm;
import com.hechuang.fruitstore.service.ProductSubcategoryServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.simpleframework.xml.core.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-02-07/20:38
 */
@RestController
@Slf4j
@RequestMapping("/ProductSubcategoryAction")
public class ProductSubcategoryController {
    @Autowired
    ProductSubcategoryDao productSubcategoryDao;
    @Autowired
    ProductSubcategoryServiceDao productSubcategoryServiceDao;
    @ApiOperation("商家新增修改子类目")
    @PostMapping("/save")
    public ResultVO save(@RequestBody @Validate SubcategoryForm form, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            log.error("{}",bindingResult.getFieldError().getDefaultMessage());
            throw new WeiXiDuoException(ResultEnum.PARAM_ERROR);
        }
        return ResultVOUtil.success("添加成功",productSubcategoryServiceDao.save(form));
    }
    @ApiOperation("商家查看所有子类目")
    @GetMapping("/findBymaincategoryId")
    public LayuiResultVo findBymaincategoryId(@RequestParam(value = "maincategoryId") Integer maincategoryId,@RequestParam(value = "page",defaultValue = "1") Integer page, @RequestParam(value = "size",defaultValue = "8") Integer size){
        PageRequest pageRequest=new PageRequest(page - 1,size);
        return new LayuiResultVo(200, "查询成功",productSubcategoryServiceDao.findBymaincategoryId(maincategoryId,pageRequest), productSubcategoryDao.countByMaincategoryId(maincategoryId));

    }

    @ApiOperation("商家查看单条子类目")
    @GetMapping("/findOneById")
    public ResultVO findOneById(@RequestParam(value = "id") Integer id){
       return ResultVOUtil.success("查询成功",productSubcategoryServiceDao.findOne(id));
    }

    @ApiOperation("删除子类目")
    @GetMapping("/delete")
    public LayuiResultVo delete(@RequestParam Integer id){
        try{
            productSubcategoryDao.delete(id);
        }catch (WeiXiDuoException e){
            return new LayuiResultVo(400,"删除失败",null,0);
        }

        return new LayuiResultVo(200,"删除成功",null,1);

    }
}
