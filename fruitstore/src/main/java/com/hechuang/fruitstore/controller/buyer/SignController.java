package com.hechuang.fruitstore.controller.buyer;

import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.form.SignForm;
import com.hechuang.fruitstore.pojo.Sign;
import com.hechuang.fruitstore.service.SignServiceDao;
import com.hechuang.fruitstore.utils.CookieUtil;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.simpleframework.xml.core.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-01-16/0:16
 */
@Slf4j
@RestController
@RequestMapping("/SignAction")
public class SignController {
    @Autowired
    SignServiceDao signServiceDao;
    @ApiOperation(value="用户签到", notes="用户签到")
    @PostMapping("/sign")
    public ResultVO sign(@RequestBody @Validate SignForm signForm, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            log.info("参数{}",signForm);
            log.error("参数有误{}",bindingResult.getFieldError().getCode());
        }
        Sign pojo=signServiceDao.sign(signForm);
        if (pojo==null){
            log.info("今日已经签到");
            return ResultVOUtil.error("今日您已签到");
        }
        return ResultVOUtil.success("签到成功",pojo);
    }
    @GetMapping("/signList")
    @ApiOperation("会员获取签到列表")
    public ResultVO signList(@RequestParam String openid, HttpServletResponse response){
        return ResultVOUtil.success("查询成功",signServiceDao.signList(openid));


    }
}
