package com.hechuang.fruitstore.controller.buyer;

import com.hechuang.fruitstore.dao.ProductSubcategoryDao;
import com.hechuang.fruitstore.pojo.ProductSubcategory;
import com.hechuang.fruitstore.service.ProductSubcategoryServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-02-07/21:29
 */
@RestController
@RequestMapping("/BuyerProductSubcategoryAction")
@Slf4j
public class BuyerProductSubcategoryController {
    @Autowired
    ProductSubcategoryServiceDao productSubcategoryServiceDao;
    //通过主类目查询子类目
    @ApiOperation("买家通过主类目查询子类目")
    @GetMapping("/findByMaincategoryId")
    public ResultVO findByMaincategoryId(@RequestParam Integer id){
        List<ProductSubcategory> productSubcategoryList=productSubcategoryServiceDao.BuyerFindBymaincategoryId(id);
        if (productSubcategoryList.size()==0){
            log.info("没有子目录");
            return ResultVOUtil.success("没有相应的子目录");
        }
        return ResultVOUtil.success("查询子目录成功",productSubcategoryList);
    }




}
