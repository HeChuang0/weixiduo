package com.hechuang.fruitstore.controller;

import com.hechuang.fruitstore.constant.CookieConstant;
import com.hechuang.fruitstore.constant.RedisConstant;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.service.LoginServiceDao;
import com.hechuang.fruitstore.utils.CookieUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Create by ToFree on 2018/1/19
 */
@Controller
@Slf4j
@RequestMapping("/LoginAction")
public class LoginController {
    @Autowired
    private LoginServiceDao loginServiceDao;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @PostMapping("/login")
    public ModelAndView login(@RequestParam String name, @RequestParam String password,
                              HttpServletResponse response,HttpServletRequest request, Map<String, Object> map) {
        if (name==null||password==null||name==""||password==""){
            return new ModelAndView("redirect:/");
        }
        //1、和数据库匹配
        log.info("用户名：{},密码：{}",name,password);
        if (loginServiceDao.login(name ,password) == false) {
            map.put("msg", ResultEnum.LOGIN_FAIL.getMessage());
            map.put("url","/");
            return new ModelAndView("/admin/login/error", map);
        }
        //2、设置token至redis
        String token = UUID.randomUUID().toString();
        Integer expire = RedisConstant.EXPIRE;
        redisTemplate.opsForValue().set(String.format(RedisConstant.TOKEN_PREFIX,token), name, expire, TimeUnit.SECONDS);
        //3、设置token至cookie
        CookieUtil.set(response, CookieConstant.TOKEN, token, expire);
        HttpSession session = request.getSession();
        session.setAttribute("name",name);
        return new ModelAndView("redirect:/admin");
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response, Map<String, Object> map) {
        //1、从cookie里查询
        Cookie cookie = CookieUtil.get(request, CookieConstant.TOKEN);
        //2、清除redis
        redisTemplate.opsForValue().getOperations().delete(String.format(RedisConstant.TOKEN_PREFIX, cookie.getValue()));
        //3、清除cookie
        CookieUtil.set(response, CookieConstant.TOKEN, null, 0);
        return new ModelAndView("redirect:/admin");
    }
}
