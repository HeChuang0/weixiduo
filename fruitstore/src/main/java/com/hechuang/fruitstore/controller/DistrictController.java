package com.hechuang.fruitstore.controller;

import com.hechuang.fruitstore.pojo.District;
import com.hechuang.fruitstore.service.DistrictServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: xht
 * @Date: 15:46 2018/2/27
 */
@RestController
@RequestMapping("/DistrictAction")
public class DistrictController {

    @Autowired
    DistrictServiceDao districtServiceDao;

    @RequestMapping("/get")
    public LayuiResultVo get(@RequestParam(value = "parentId",defaultValue = "0") Integer parentId){
        List<District> districtList = districtServiceDao.getByParentId(parentId);
        return new LayuiResultVo(200,"查询成功",districtList,districtList.size());
    }

    @RequestMapping("/getByName")
    public ResultVO getByName(@RequestParam(value = "name") String name){
        return ResultVOUtil.success("查询成功",districtServiceDao.getByName(name));
    }

}
