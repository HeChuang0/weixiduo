package com.hechuang.fruitstore.controller.buyer;

import com.hechuang.fruitstore.dao.ProductInfoDao;
import com.hechuang.fruitstore.enums.ProductStatusEnum;
import com.hechuang.fruitstore.pojo.ProductInfo;
import com.hechuang.fruitstore.query.ProductInfoQueryParams;
import com.hechuang.fruitstore.service.ProductInfoServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by Stefan
 * Create Date 2018-01-23/23:12
 */
@Slf4j
@RestController
@RequestMapping("/BuyerProductInfoAction")
@CacheConfig(cacheNames = "product")
public class BuyerProductInfoController {
    @Autowired
    ProductInfoServiceDao productInfoServiceDao;
    @Autowired
    ProductInfoDao productInfoDao;

    @ApiOperation("查看商品所有（查）")
    @GetMapping("/get")
    @Cacheable(key = "123")
    public ResultVO get() {
        return ResultVOUtil.success("查询成功",productInfoDao.findAll());
    }

    @ApiOperation("通过产品主分类查找产品列表")
    @GetMapping("/findByCategoryId")
    public ResultVO<ProductInfo> findByCategoryId(@RequestParam Integer categoryId) {
        if (categoryId != null) {
            List<ProductInfo> list = productInfoServiceDao.findByCategoryId(categoryId);
            return ResultVOUtil.success("查询成功", list);
        }
        return ResultVOUtil.error("查询失败，请求参数为空");
    }

    @ApiOperation("通过产品子分类查找产品列表")
    @GetMapping("/findBySubCategoryId")
    public ResultVO findBySubCategoryId(@RequestParam Integer subId) {
        if (subId != null) {
            List<ProductInfo> list = productInfoServiceDao.findBySubCategoryId(subId);
            return ResultVOUtil.success("查询成功", list);
        }
        return ResultVOUtil.error("查询失败，请求参数为空");
    }

    @ApiOperation("通过关键字查看商品（模糊查询）")
    @GetMapping("/getByCondition")
    public ResultVO<ProductInfo> getByCondition(@RequestParam String condition) {
        if (validate(condition)) {
            List<ProductInfo> list = productInfoServiceDao.getByCondition(condition);
            return new ResultVO<>(true, "查询成功", list, list.size());
        }
        return ResultVOUtil.error("查询失败，请求参数为空");
    }
    @GetMapping("/findOne")
    @ApiOperation("查询商品详情")
    public ResultVO findOne(@RequestParam Integer id){
        return ResultVOUtil.success("查询单个商品成功",productInfoDao.findOne(id));
    }
@GetMapping("/findByForetaste")
@ApiOperation("微信端查看试吃商品列表")
    public ResultVO findByForetaste(){
        return ResultVOUtil.success("查询试吃商品成功",productInfoDao.findByForetaste(ProductStatusEnum.ISFORETASTE.getCode()));
    }
@GetMapping("/getNewProducts")
@ApiOperation("微信前端查询七天新品")

    public ResultVO getNewProducts(){
        return ResultVOUtil.success("查询7天新品成功",productInfoServiceDao.getNewProducts());
    }


    /**
     * 校验参数合法性
     *
     * @param obj
     * @return
     */
    private boolean validate(Object obj) {
        if (obj instanceof String) {
            if (obj.equals("")) {
                return false;
            }
        }
        if (null == obj) {
            return false;
        }
        return true;
    }
}
