package com.hechuang.fruitstore.controller.seller;

import com.hechuang.fruitstore.dto.OrderDTO;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.pojo.OrderMaster;
import com.hechuang.fruitstore.query.OrderMasterQueryParams;
import com.hechuang.fruitstore.service.OrderMasterServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.OrderVO;
import com.hechuang.fruitstore.vo.ResultVO;
import com.lly835.bestpay.rest.type.Get;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Create by ToFree on 2018/1/12
 */
//http://127.0.0.1:8080/weixiduo/OrderMasterAction/getOrder?openid=31324&orderid=31321421
@Slf4j
@RestController
@RequestMapping("/OrderMasterAction")
public class OrderMasterController {
	@Autowired
	OrderMasterServiceDao orderMasterServiceDao;
	@GetMapping("/detail")
	@ApiOperation("查看订单详情")
	public ResultVO deatil(@RequestParam String orderid){
		return  ResultVOUtil.success("成功",orderMasterServiceDao.detail(orderid));
	}


	@GetMapping("/sellerOrderList")
	@ApiOperation("卖家查看已发货和新订单列表")
	public LayuiResultVo sellerOrderList(@RequestParam(required = false, defaultValue = "1") Integer page,@RequestParam(required = false, defaultValue = "8") Integer pageSize,@RequestParam(defaultValue = "2017-12-31 16:25:46") String startTime,@RequestParam(defaultValue="2100-01-30 16:25:46") String endTime){
		log.info("pastDate{}，currentDate{}",startTime,endTime);
		PageRequest pageRequest=new PageRequest(page - 1,pageSize);
		Map map=orderMasterServiceDao.sellerOrderList(startTime,endTime,pageRequest);
		List<OrderMaster> orderMasterList= (List<OrderMaster>) map.get("list");
		Long size = (Long) map.get("size");
		if (CollectionUtils.isEmpty(orderMasterList)){
			return new LayuiResultVo(200, "没有订单");
		}
		return new LayuiResultVo(200, "查询成功", orderMasterList, size);
	}

	@GetMapping("/cancel")
	@ApiOperation("商家取消订单")
	public ResultVO cancel(@RequestParam("orderid") String orderid){
		try{
			OrderVO orderVO=orderMasterServiceDao.detail(orderid);
			OrderDTO orderDTO=new OrderDTO();
			BeanUtils.copyProperties(orderVO,orderDTO);
			orderMasterServiceDao.cancel(orderDTO);
			return ResultVOUtil.success("取消订单成功");
		}catch (WeiXiDuoException e){
			log.error("【卖家端取消订单】发生异常{}", e);
			return ResultVOUtil.error("【卖家端取消订单】发生异常") ;

		}
	}
	@GetMapping("/finish")
	@ApiOperation("商家发货")
	public ResultVO finish(@RequestParam("orderid") String orderid){
		try{
			OrderVO orderVO=orderMasterServiceDao.detail(orderid);
			OrderDTO orderDTO=new OrderDTO();
			BeanUtils.copyProperties(orderVO,orderDTO);
			orderMasterServiceDao.finish(orderDTO);
			return ResultVOUtil.success("订单发货成功");
		}catch (WeiXiDuoException e){
			log.error("【卖家端发货订单】发生异常{}", e);
			return ResultVOUtil.error("【卖家端发货订单】发生异常") ;
		}

	}
	@ApiOperation("查看最近2天未发货的新订单")
@GetMapping("/latelyOrderList")
	public LayuiResultVo latelyOrderList(@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "8") Integer pageSize
	){
		Map map=orderMasterServiceDao.latelyOrderList(page-1,pageSize);
			Long size= (Long) map.get("size");
			List<OrderMaster> orderMasterList= (List<OrderMaster>) map.get("list");
		log.info("查看最近2天未发货的新订单{}",map);
		if (CollectionUtils.isEmpty(orderMasterList)){
			return new LayuiResultVo(200, "没有订单");
		}
		return new LayuiResultVo(200,"查询成功",orderMasterList,size);
	}

	@GetMapping("/refundOrderList")
	@ApiOperation("卖家查看已取消订单列表")
	public LayuiResultVo refundOrderList(@RequestParam(required = false, defaultValue = "1") Integer page,@RequestParam(required = false, defaultValue = "8") Integer pageSize){
		PageRequest pageRequest=new PageRequest(page - 1,pageSize);
		List<OrderMaster> orderMasterList= (List<OrderMaster>) orderMasterServiceDao.refundOrderList(pageRequest).get("list");
		Long size = (Long) orderMasterServiceDao.refundOrderList(pageRequest).get("size");
		if (CollectionUtils.isEmpty(orderMasterList)){
			return new LayuiResultVo(200, "没有订单");
		}
		return new LayuiResultVo(200, "查询成功", orderMasterList, size);
	}

	//删除取消的订单
	@GetMapping("/delete")
	@ApiOperation("卖家删除已取消订单列表")
	public LayuiResultVo delete(@RequestParam String orderid){
		try{
			orderMasterServiceDao.delete(orderid);
			log.info("删除成功订单编号:{}",orderid);
			return new LayuiResultVo(200,"删除成功");
		}catch (WeiXiDuoException e){
			log.error("删除订单失败{}",e.getMessage());
			return new LayuiResultVo(200,"删除失败");
		}
	}
}
