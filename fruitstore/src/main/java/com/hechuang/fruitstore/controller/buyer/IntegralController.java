package com.hechuang.fruitstore.controller.buyer;

import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.pojo.Integral;
import com.hechuang.fruitstore.service.IntegralServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.ResultVO;
import com.lly835.bestpay.rest.type.Post;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Stefan
 * Create Date 2018-01-15/22:52
 */
@Slf4j
@RestController
@RequestMapping("/IntegralAction")
public class IntegralController {
	@Autowired
	IntegralServiceDao integralServiceDao;

	@PostMapping("/receive")
	@ApiOperation(value = "用户领取积分", notes = "用户领取积分")
	public ResultVO receive(@RequestBody Integral integral) {
		if (integral == null) {
			throw new WeiXiDuoException(ResultEnum.PARAM_ERROR);
		}
		return ResultVOUtil.success("领取积分成功", integralServiceDao.receive(integral));
	}

	@ApiOperation(value = "用户查看积分", notes = "用户查看积分")
	@GetMapping("/findByOpenid")
	public ResultVO findMyIntegral(@RequestParam String openid) {
		Integral integral = integralServiceDao.findMyIntegral(openid);
		if (integral == null) {
			log.error("未领取积分");
			return ResultVOUtil.error("您还未领取积分");
		}
		return ResultVOUtil.success("查看个人积分成功", integral);
	}
}
