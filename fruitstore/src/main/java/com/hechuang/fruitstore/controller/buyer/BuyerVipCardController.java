package com.hechuang.fruitstore.controller.buyer;

import com.hechuang.fruitstore.pojo.VipCard;
import com.hechuang.fruitstore.service.VipCardServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Stefan
 * Create Date 2018-01-23/23:16
 */
@Slf4j
@RestController
@RequestMapping("/BuyerVipCardAction")
public class BuyerVipCardController {
    @Autowired
    VipCardServiceDao vipCardServiceDao;

    @ApiOperation(value="会员卡等级详情", notes="查看会员卡等级详情")
    @GetMapping("/findById")
    public ResultVO getById(Integer id){
        if(id==null){
            return ResultVOUtil.error("id未传");
        }
        VipCard data = vipCardServiceDao.getById(id);
        System.out.println(data);
        if(data==null){
            return ResultVOUtil.error("查询失败");
        }
        return ResultVOUtil.success("查询成功",data);
    }
}
