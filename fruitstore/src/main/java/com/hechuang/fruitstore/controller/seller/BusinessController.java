package com.hechuang.fruitstore.controller.seller;

import com.hechuang.fruitstore.dao.BusinessDao;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.form.BusinessForm;
import com.hechuang.fruitstore.pojo.Business;
import com.hechuang.fruitstore.service.BusinessServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.simpleframework.xml.core.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Stefan
 * Create Date 2018-02-26/17:53
 */
@RestController
@RequestMapping("/BusinessAction")
@Slf4j
public class BusinessController {
    @Autowired
    BusinessServiceDao businessServiceDao;
    @Autowired
    BusinessDao businessDao;
    @ApiOperation("商户新增和更新")
    @PostMapping("/save")
    public ResultVO save(@RequestBody(required = false) @Validate BusinessForm form,BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            return ResultVOUtil.error("添加失败，请求参数不完整");
        }
        log.info("{}",form);
        return ResultVOUtil.success("修改成功",businessServiceDao.save(form));
    }
@GetMapping("/delete")
@ApiOperation("删除商户")
    public ResultVO delete(@RequestParam Integer id){
        try{
            businessServiceDao.delete(id);
            return ResultVOUtil.success("删除成功");
        }catch (Exception e){
            return ResultVOUtil.error("删除失败");
        }

    }
@GetMapping("/businessList")
@ApiOperation("查看商户列表")
    public LayuiResultVo businessList(@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "8") Integer size){
        PageRequest pageRequest=new PageRequest(page-1,size);
        Page<Business> businessPage=businessServiceDao.getBusinesses(pageRequest);
        return new LayuiResultVo(200,"查询商户列表成功",businessPage.getContent(),businessDao.count());
    }

    @GetMapping("/findOneById")
    @ApiOperation("根据id查询商户")
    public ResultVO findOneById(@RequestParam(value = "id") Integer id){
        return ResultVOUtil.success("查询成功",businessServiceDao.finfOne(id));
    }
}
