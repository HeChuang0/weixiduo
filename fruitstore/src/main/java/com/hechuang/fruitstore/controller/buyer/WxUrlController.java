package com.hechuang.fruitstore.controller.buyer;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Create by stefan
 * Date on 2018-03-23  13:48
 * Convertion over Configuration!
 */
@Controller
@RequestMapping("/weixiduo")
public class WxUrlController {
    @GetMapping("/index")
    public  String  index(){
        return "wx/main/index";
    }
    @GetMapping("/address/add")
    public String addAddress(){
        return "wx/main/address/AddAddress";
    }

    @GetMapping("/address/gladdress")
    public String gladdress(){
        return "wx/main/address/gladdress";
    }

    @GetMapping("/address/update")
    public String updateaddress(){
        return "wx/main/address/updateAddress";
    }
    @GetMapping("/category/category")
    public String category(){
        return  "wx/main/category/category";
    }

    @GetMapping("/category/list")
    public String categorylist(){
        return  "wx/main/category/list";
    }
    @GetMapping("/coupon/yhq")
    public String yhq(){
        return "wx/main/coupon/yhq";
    }
    @GetMapping("/display/detail")
    public String  displayDetail(){
        return "wx/main/display/detail";
    }

    @GetMapping("/display/newProduct")
    public String  displaynewProduct(){
        return "wx/main/display/newProduct";
    }

    @GetMapping("/display/experienceShops")
    public String  displayexperienceShops(){
        return "wx/main/display/experienceShops";
    }

    @GetMapping("/display/foreTaste")
    public String  displayforeTaste(){
        return "wx/main/display/foreTaste";
    }

    @GetMapping("/display/search")
    public String  displaySearch(){
        return "wx/main/display/search";
    }

    @GetMapping("/list/dryFruit")
    public String listDryFruit(){
        return "wx/main/list/dryFruit";
    }

    @GetMapping("/list/local")
    public String listlocal(){
        return "wx/main/list/local";
    }

    @GetMapping("/list/seafood")
    public String listseafood(){
        return "wx/main/list/seafood";
    }

    @GetMapping("/list/vegetable")
    public String listvegetable(){
        return "wx/main/list/vegetable";
    }

    @GetMapping("/member/daily")
    public String  memberDaily(){
        return "wx/main/member/daily";
    }

    @GetMapping("/member/jifen")
    public String  memberJifen(){
        return "wx/main/member/jifen";
    }

    @GetMapping("/member/redPacket")
    public String  memberredPacket(){
        return "wx/main/member/redPacket";
    }

    @GetMapping("/member/rule")
    public String  memberRule(){
        return "wx/main/member/rule";
    }

    @GetMapping("/member/tequan")
    public String  membertequan(){
        return "wx/main/member/tequan";
    }

    @GetMapping("/member/vip")
    public String  memberVip(){
        return "wx/main/member/vip";
    }

    @GetMapping("/member/youhui")
    public String  memberyouhui(){
        return "wx/main/member/youhui";
    }

    @GetMapping("/member/share")
    public String  membershare(){
        return "wx/main/member/share";
    }

    @GetMapping("/member/zhanghao")
    public String  memberzhanghao(){
        return "wx/main/member/zhanghao";
    }

    @GetMapping("/order/allorder")
    public String  orderallorder(){
        return "wx/main/order/allorder";
    }

    @GetMapping("/order/sell")
    public String  ordersell(){
        return "wx/main/order/sell";
    }

    @GetMapping("/order/pay")
    public String  orderpay(){
        return "wx/main/order/pay";
    }

    @GetMapping("/order/settleOrder")
    public String  ordersettleOrder(){
        return "wx/main/order/settleOrder";
    }

    @GetMapping("/shopCart/emptyShopCart")
    public String  shopCartemptyShopCart(){
        return "wx/main/shopCart/emptyShopCart";
    }

    @GetMapping("/shopCart/shopcart")
    public String  shopCarshopcart(){
        return "wx/main/shopCart/shopcart";
    }


























}
