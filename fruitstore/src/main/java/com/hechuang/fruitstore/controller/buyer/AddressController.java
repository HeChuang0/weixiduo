package com.hechuang.fruitstore.controller.buyer;

import com.hechuang.fruitstore.pojo.Address;
import com.hechuang.fruitstore.service.AddressServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.omg.PortableInterceptor.RequestInfo;
import org.simpleframework.xml.core.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: fing
 * @Description:
 * @Date: 下午7:36 18-1-16
 */

@Slf4j
@RestController
@RequestMapping("/AddressAction")
@CacheConfig(cacheNames = "address")
public class AddressController {

	@Autowired
	AddressServiceDao addressServiceDao;

	@ApiOperation("用户新增地址")
	@PostMapping("/add")
	@CacheEvict(key = "#address.openid")
	public ResultVO<Address> add(@RequestBody @Validate Address address) {
		if (validate(address)) {
			if (null != addressServiceDao.add(address)) {
				return ResultVOUtil.success("添加成功", addressServiceDao.add(address));
			}
		}
		return ResultVOUtil.error("参数错误，请重试");
	}
	@ApiOperation("买家查询单个地址")
	@GetMapping("/getOne")
	public ResultVO getOne(Integer id){
		return ResultVOUtil.success("查询成功",addressServiceDao.getOne(id));
	}

	@ApiOperation("获取用户地址列表")
	@GetMapping("/findByOpenid")
	@Cacheable(key = "#openid")
	public ResultVO findByOpenid(@RequestParam(required = false) String openid) {
		if (openid == null) {
			return ResultVOUtil.error("参数错误，请重试");
		}
		List<Address> list = addressServiceDao.findByOpenid(openid);
		return new ResultVO(true, "查询成功", list, list.size());
	}

	@ApiOperation("用户修改地址")
	@PostMapping("/update")
	@CacheEvict(key = "#address.openid")
	public ResultVO update(@RequestBody(required = false) @Validate Address address, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return ResultVOUtil.error("修改失败，请求参数不完整");
		}
		if (validate(address)) {
			Address update = addressServiceDao.update(address);
			if (update != null) {
				return ResultVOUtil.success("修改成功");
			}
		}
		return ResultVOUtil.error("修改失败");
	}

	@ApiOperation("用户删除地址")
	@GetMapping("/delete")
	@CacheEvict(key = "#openid")
	public ResultVO delete(@RequestParam(required = false) Integer id,@RequestParam(required = false) String openid) {
		if (validate(id)) {
			if (1 == addressServiceDao.delete(id)) {
				return ResultVOUtil.success("删除成功");
			}
		}
		return ResultVOUtil.error("参数错误，请重试");

	}
	@ApiOperation("修改默认收货地址")
	@GetMapping("/updateChecked")
	@CacheEvict(key = "#openid")
	public ResultVO updateChecked(@RequestParam(value = "id") Integer id,@RequestParam(required = false) String openid){
		Address address=addressServiceDao.updateChecked(id,openid);
		return ResultVOUtil.success("修改成功",address);
	}


	/**
	 * 校验参数合法性
	 *
	 * @param obj
	 * @return
	 */
	private boolean validate(Object obj) {
		if (obj instanceof String) {
			if (obj.equals("")) {
				return false;
			}
		}
		if (null == obj) {
			return false;
		}
		return true;
	}

}
