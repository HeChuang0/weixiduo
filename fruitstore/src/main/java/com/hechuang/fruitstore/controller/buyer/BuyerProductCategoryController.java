package com.hechuang.fruitstore.controller.buyer;

import com.hechuang.fruitstore.dao.ProductCategoryDao;
import com.hechuang.fruitstore.service.ProductCategoryServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by Stefan
 * Create Date 2018-02-09/21:18
 */
@RestController
@Slf4j
@RequestMapping("/BuyerProductCategoryAction")
public class BuyerProductCategoryController {
    @Autowired
    private ProductCategoryDao productCategoryDao;
    @ApiOperation("查看类目列表")
    @GetMapping("/get")
    public ResultVO getProductCategoryList() {
        return ResultVOUtil.success("查询成功",productCategoryDao.findAll());
    }
}
