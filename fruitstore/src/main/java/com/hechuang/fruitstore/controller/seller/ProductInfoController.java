package com.hechuang.fruitstore.controller.seller;

import com.hechuang.fruitstore.form.ProductForm;
import com.hechuang.fruitstore.pojo.ProductInfo;
import com.hechuang.fruitstore.query.ProductInfoQueryParams;
import com.hechuang.fruitstore.service.ProductInfoServiceDao;
import com.hechuang.fruitstore.utils.QiNiuUtil;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.simpleframework.xml.core.Validate;
import org.springframework.beans.PropertyAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @Author: fing
 * @Description:
 * @Date: 下午9:06 18-1-15
 */
@Slf4j
@RestController
@RequestMapping("/ProductInfoAction")
public class ProductInfoController {

    @Autowired
    ProductInfoServiceDao productInfoServiceDao;



    @ApiOperation("查看商品所有（查）")
    @GetMapping("/get")
    public LayuiResultVo get(ProductInfoQueryParams params) {
        if (validate(params.getPage()) && validate(params.getPageSize())) {
            Map map = productInfoServiceDao.get(params);
            return new LayuiResultVo(200, "查询成功", (List) map.get("list"), (Long) map.get("size"));
        }
        return new LayuiResultVo(400, "查询失败", null, 0);
    }

    @ApiOperation("商家后台新增商品（增）")
    @PostMapping("/add")
    public ResultVO add(@RequestBody(required = false) @Validate ProductForm productForm, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return ResultVOUtil.error("添加失败，请求参数不完整");
        }
        ProductInfo productInfo=productInfoServiceDao.add(productForm);
        if (validate(productInfo)) {
            return ResultVOUtil.success("添加成功", productInfo);
        }
        return ResultVOUtil.error("添加失败，请求参数为空");
    }

    @ApiOperation("更新产品信息")
    @PostMapping("/update")
    public ResultVO update(@RequestBody(required = false) @Validate ProductForm productForm, BindingResult bindingResult) {
        log.info("参数{}",productForm);
        if (bindingResult.hasErrors()) {
            return ResultVOUtil.error("修改失败，请求参数不完整");
        }
        if (validate(productForm)) {
            return ResultVOUtil.success("修改成功", productInfoServiceDao.update(productForm));
        }
        return ResultVOUtil.error("修改失败，请求参数为空");
    }

    @ApiOperation("根据ID删除产品")
    @PostMapping("/delete")
    public ResultVO delete(@RequestParam Integer id) {
        if (id!=null) {
            int i = productInfoServiceDao.deleteById(id);
            if (1 == i) {
                return ResultVOUtil.success("删除成功");
            } else {
                return ResultVOUtil.error("删除失败");
            }
        }
        return ResultVOUtil.error("删除失败，请求参数为空");
    }

    @ApiOperation("产品上架")
    @PostMapping("/onSale")
    public ResultVO onSale(@RequestParam Integer id) {
        if (id!=null) {
            int i = productInfoServiceDao.onSale(id);
            if (1 == i) {
                return ResultVOUtil.success("设置成功");
            } else {
                return ResultVOUtil.error("设置失败");
            }
        }
        return ResultVOUtil.error("设置失败，请求参数为空");
    }

    @ApiOperation("产品下架")
    @PostMapping("/offSale")
    public ResultVO offSale(@RequestParam Integer id) {
        if (id!=null) {
            int i = productInfoServiceDao.offSale(id);
            if (1 == i) {
                return ResultVOUtil.success("设置成功");
            } else {
                return ResultVOUtil.error("设置失败");
            }
        }
        return ResultVOUtil.error("设置失败，请求参数为空");
    }

    @ApiOperation("根据id查询商品")
    @GetMapping("/findOneById")
    public ResultVO get(@RequestParam(value = "id") Integer id) {
        return ResultVOUtil.success("查询成功",productInfoServiceDao.findOne(id));
    }


    /**
     * 校验参数合法性
     *
     * @param obj
     * @return
     */
    private boolean validate(Object obj) {
        if (obj instanceof String) {
            if (obj.equals("")) {
                return false;
            }
        }
        if (null == obj) {
            return false;
        }
        return true;
    }


}
