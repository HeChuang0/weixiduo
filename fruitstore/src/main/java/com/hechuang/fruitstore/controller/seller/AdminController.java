package com.hechuang.fruitstore.controller.seller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.ws.rs.Path;

/**
 * @Author: fing
 * @Description:
 * @Date: 上午10:43 18-1-17
 */

@Controller
@RequestMapping("/admin")
public class AdminController {

	@GetMapping(value = {"","/"})
	public String index() {
		return "admin/index";
	}

	@GetMapping("/product")
	public String toProduct() {
		return "admin/product/product";
	}

	@GetMapping("/addProduct")
	public String toAddProduct() {
		return "admin/product/product-add";
	}

	@GetMapping("/editProduct")
	public String toEditProduct() {
		return "admin/product/product-edit";
	}

	@GetMapping("/business")
	public String toBusiness() {
		return "admin/business/business";
	}

	@GetMapping("/addBusiness")
	public String toAddBusiness() {
		return "admin/business/business-add";
	}

	@GetMapping("/editBusiness")
	public String toEditBusiness() {
		return "admin/business/business-edit";
	}

	@GetMapping("/productCategory")
	public String toproductCategory() {
		return "admin/category/productCategory";
	}

	@GetMapping("/addProductCategory")
	public String toAddProductCategory() {
		return "admin/category/productCategory-add";
	}

	@GetMapping("/productSubCategory/{id}")
	public String toproductSubCategory(@PathVariable Integer id, ModelMap map) {
		map.put("id",id);
		return "admin/subcategory/productSubCategory";
	}

	@GetMapping("/addProductSubCategory")
	public String toAddProductSubCategory() {
		return "admin/subcategory/productSubCategory-add";
	}

	@GetMapping("/vipMember")
	public String tovipMember() {
		return "admin/vipMember";
	}

	@GetMapping("/addVipMember")
	public String toAddVipMember() {
		return "admin/vipMember-add";
	}

	@GetMapping("/order")
	public String toOrder() {
		return "admin/order/order";
	}

	@GetMapping("/orderLately")
	public String toOrder_lately() {
		return "admin/order/order-lately";
	}

	@GetMapping("/orderRefund")
	public String toOrder_refund() {
		return "admin/order/order-refund";
	}

	@GetMapping("/addOrder")
	public String toAddOrder() {
		return "admin/order/order-add";
	}

	@GetMapping("/orderDetail/{id}")
	public String toOrderDetail(@PathVariable String id, Model model) {
		model.addAttribute("id", id);
		return "admin/order/order-detail";
	}

	@GetMapping("/EditProductCategory/{id}")
	public String toEditProductCategory(@PathVariable Integer id, Model model) {
		model.addAttribute("id", id);
		return "admin/category/productCategory-edit";
	}

	@GetMapping("/EditProductSubCategory")
	public String toEditProductSubCategory() {
		return "admin/subcategory/productSubCategory-edit";
	}

	@GetMapping("/vipCard")
	public String toVipCard() {
		return "admin/vipcard/vipCard";
	}

	@GetMapping("/addVipCard")
	public String toAddVipCard() {
		return "admin/vipcard/vipCard-add";
	}

	@GetMapping("/editVipCard/{id}")
	public String toEditVipCard(@PathVariable Integer id, Model model) {
		model.addAttribute("id", id);
		return "admin/vipcard/vipCard-edit";
	}

	@GetMapping("/discountSet")
	public String toDiscountSet() {
		return "admin/discountset/discountSet";
	}

	@GetMapping("/addDiscountSet")
	public String toAddDiscountSet() {
		return "admin/discountset/discountSet-add";
	}

	@GetMapping("/editDiscountSet/{id}")
	public String toEditDiscountSet(@PathVariable Integer id, Model model) {
		model.addAttribute("id", id);
		return "admin/discountset/discountSet-edit";
	}
}
