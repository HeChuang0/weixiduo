package com.hechuang.fruitstore.controller.buyer;

import com.hechuang.fruitstore.dao.DiscountSetDao;
import com.hechuang.fruitstore.dao.RedPacketDao;
import com.hechuang.fruitstore.enums.DiscountCodeEnum;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.pojo.DiscountSet;
import com.hechuang.fruitstore.pojo.RedPacket;
import com.hechuang.fruitstore.service.RedPacketServiceDao;
import com.hechuang.fruitstore.utils.ListUtil;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.DistciuntSetVO;
import com.hechuang.fruitstore.vo.RedPacketVO;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-01-16/14:46
 */
@Slf4j
@RestController
@RequestMapping("/RedPacketAction")
@CacheConfig(cacheNames = "redPacket")
public class RedPacketController {
    @Autowired
    RedPacketServiceDao redPacketServiceDao;
    @Autowired
    DiscountSetDao discountSetDao;

    @Autowired
    RedPacketDao redPacketDao;

    @GetMapping("/list")
    @ApiOperation("查看红包列表")
    public ResultVO findByOpenid(@RequestParam("openid") String openid) {
        if (StringUtils.isEmpty(openid)) {
            log.error("【查询领取的红包列表】openid为空");
            throw new WeiXiDuoException(ResultEnum.PARAM_ERROR);
        }
        List<RedPacketVO> redPacketVOList = redPacketServiceDao.findUserRed(openid);
        if (ListUtil.isEmpty(redPacketVOList)) {
            return ResultVOUtil.error("您还未领取红包");
        }
        return ResultVOUtil.success("查看本人红包成功",redPacketVOList);

    }

    @ApiOperation("领取普通和新人红包")
    @PostMapping("/receive")
    public ResultVO receive(@RequestBody RedPacket redPacket) {
        log.info("领取红包参数:{}",redPacket);
        RedPacket pojo = redPacketServiceDao.receive(redPacket);
        if (pojo == null) {
            return ResultVOUtil.error("您已经领取过红包");
        }
        return ResultVOUtil.success("领取红包成功", redPacket);
    }

    @ApiOperation("在订单界面查找可使用的现金券")
    @GetMapping("/getOrderDiscountSets")
    public ResultVO getOrderDiscountSets(@RequestParam String openid, @RequestParam BigDecimal orderAmount, @RequestParam Integer useType) {
        log.info("参数{},{},{}",openid,orderAmount,useType);
        List<DistciuntSetVO> discountSetList = redPacketServiceDao.getOrderDiscountSets(orderAmount, openid, useType);
        log.info("discountSetList:{}",discountSetList);
        return ResultVOUtil.success("查询成功", discountSetList);
    }

    @GetMapping("/findOne")
    public ResultVO findById(@RequestParam Integer id) {
        return ResultVOUtil.success("查询成功", discountSetDao.findOne(id));
    }

    @GetMapping("/getNew")
    public ResultVO getNew() {
        return ResultVOUtil.success("新人红包查询成功", discountSetDao.findByType(0));
    }

    @ApiOperation("查看是否领取新人红包")
    @GetMapping("/findNewPackage")
    public ResultVO findNewPackage(@RequestParam String openid,@RequestParam Integer discountId) {
        RedPacket pojo = redPacketServiceDao.isReceive(openid,discountId);
        if (pojo == null) {
            return ResultVOUtil.error("未领取新人红包");
        }
        return ResultVOUtil.success("已经领取过新人红包", pojo);

    }
    @GetMapping("/getMarketDescounts")
    @ApiOperation("微信端查询商城红包")
    @Cacheable(key = "222")
    public ResultVO getMarketDescount(){
        return ResultVOUtil.success("查询商城红包成功",discountSetDao.findByType(DiscountCodeEnum.MARKET_DISCOUNT.getCode()));
    }
}
