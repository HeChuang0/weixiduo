package com.hechuang.fruitstore.controller.buyer;

import com.hechuang.fruitstore.dao.OrderMasterDao;
import com.hechuang.fruitstore.dto.OrderDTO;
import com.hechuang.fruitstore.enums.PayStatusEnum;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.pojo.OrderMaster;
import com.hechuang.fruitstore.service.OrderMasterServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.OrderVO;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-01-23/23:08
 */
@Slf4j
@RestController
@RequestMapping("/BuyerOrderAction")
@CacheConfig(cacheNames = "order")
public class BuyerOrderController {
    @Autowired
    OrderMasterServiceDao orderMasterServiceDao;
    @Autowired
    OrderMasterDao orderMasterDao;
    @GetMapping("/detail")
    @ApiOperation("查看订单详情")
    public ResultVO deatil(@RequestParam String orderid){
        return  ResultVOUtil.success("成功",orderMasterServiceDao.detail(orderid));
    }

    @GetMapping("/buyerOrderList")
    @ApiOperation("买家查看订单列表")
    @Cacheable(key = "#openid")
    public ResultVO buyerOrderList(@RequestParam String openid){
        List<OrderMaster> orderMasterList=orderMasterServiceDao.buyerOrderList(openid);
        if (CollectionUtils.isEmpty(orderMasterList)){
            ResultVOUtil.error("没有订单");
        }
        return ResultVOUtil.success("查询成功",orderMasterList);
    }

    @GetMapping("/buyerOrderNoPayList")
    public ResultVO  buyerOrderNoPayList(@RequestParam String openid){
        List<OrderMaster> orderMasterList=orderMasterDao.findByPayStatusOrderByOrderidDesc(PayStatusEnum.WAIT.getCode());
        return  ResultVOUtil.success("查询成功",orderMasterList);


    }

    @PostMapping("/create")
    @ApiOperation("创建订单")
    @CacheEvict(key = "#orderDTO.openid")
    public ResultVO create(@RequestBody(required = false) @Validated OrderDTO orderDTO, BindingResult bindingResult){
        log.info("创建订单传过来的参数：{}",orderDTO);
        if (bindingResult.hasErrors()){
            log.error("参数有误,orderDTO={}",orderDTO);
            throw new WeiXiDuoException(ResultEnum.PARAM_ERROR.getCode(),
                    bindingResult.getFieldError().getDefaultMessage());
        }
        if(CollectionUtils.isEmpty(orderDTO.getOrderDetails())){
            log.error("【创建订单】购物车不能为空");
            throw new WeiXiDuoException(ResultEnum.CART_EMPTY);
        }
        return ResultVOUtil.success("创建订单成功",orderMasterServiceDao.create(orderDTO));
    }

    @GetMapping("/cancel")
    @CacheEvict(key = "#openid")
    public ResultVO cancel(@RequestParam("orderid") String orderid,@RequestParam("openid") String openid){
        try{
            OrderVO orderVO=orderMasterServiceDao.detail(orderid);
            OrderDTO orderDTO=new OrderDTO();
            BeanUtils.copyProperties(orderVO,orderDTO);
            orderMasterServiceDao.cancel(orderDTO);
            return ResultVOUtil.success("取消订单成功");
        }catch (WeiXiDuoException e){
            log.error("【买家端取消订单】发生异常{}", e);
            return ResultVOUtil.error("【买家端取消订单】发生异常") ;
        }

    }

    //删除取消的订单
    @GetMapping("/delete")
    @ApiOperation("买家删除已取消订单列表")
    @CacheEvict(key = "#openid")
    public LayuiResultVo delete(@RequestParam String orderid,@RequestParam("openid") String openid){
        try{
            orderMasterServiceDao.delete(orderid);
            log.info("删除成功订单编号:{}",orderid);
            return new LayuiResultVo(200,"删除成功");
        }catch (WeiXiDuoException e){
            log.error("删除订单失败{}",e.getMessage());
            return new LayuiResultVo(200,"删除失败");
        }
    }

}
