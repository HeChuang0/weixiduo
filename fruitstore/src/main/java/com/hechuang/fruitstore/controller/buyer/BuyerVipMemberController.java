package com.hechuang.fruitstore.controller.buyer;

import com.hechuang.fruitstore.form.VipMemberForm;
import com.hechuang.fruitstore.pojo.VipCard;
import com.hechuang.fruitstore.pojo.VipMember;
import com.hechuang.fruitstore.service.VipCardServiceDao;
import com.hechuang.fruitstore.service.VipMemberServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.ResultVO;
import com.hechuang.fruitstore.vo.VipMemberVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static java.awt.SystemColor.info;

/**
 * Created by Stefan
 * Create Date 2018-01-23/23:18
 */

@Slf4j
@RestController
@RequestMapping("/BuyerVipMemberAction")
@CacheConfig(cacheNames = "members")
public class BuyerVipMemberController {
    @Autowired
    VipMemberServiceDao vipMemberServiceDao;
    @Autowired
    VipCardServiceDao vipCardServiceDao;

    @ApiOperation(value="用户修改资料", notes="用户修改资料")
    @PostMapping("/update")
    @CachePut(key = "#vipMemberForm.openid")
    public ResultVO update(@RequestBody(required = false) @Validated VipMemberForm vipMemberForm, BindingResult bindingResult){
        if(bindingResult.hasErrors()||vipMemberForm==null){
            log.error("会员信息不完整");
            return ResultVOUtil.error("会员信息不完整");
        }
        VipMember data=vipMemberServiceDao.updateVipMember(vipMemberForm);
        return ResultVOUtil.success("修改成功",data);

    }

    @ApiOperation(value="微信用户查看自己的信息", notes="微信用户查看自己的信息")
    @GetMapping("/findByOpenid/{openId}")
    //@Cacheable(key = "#openId")
    public ResultVO findByOpenid(@PathVariable  String openId){
        VipMemberVO vipMemberVO = new VipMemberVO();
        VipMember vipMember=vipMemberServiceDao.findByOpenid(openId);
        log.info("{}",vipMember);
        if (vipMember == null){
            log.error("查看会员资料失败");
            return ResultVOUtil.error("查看会员资料失败");
        }
        VipCard vipCard = vipCardServiceDao.getById(vipMember.getVipCardId());
        BeanUtils.copyProperties(vipMember,vipMemberVO);
        vipMemberVO.setVipName(vipCard.getVipName());
        vipMemberVO.setDiscount(vipCard.getDiscount());
        return ResultVOUtil.success("查看成功",vipMemberVO);

    }
}
