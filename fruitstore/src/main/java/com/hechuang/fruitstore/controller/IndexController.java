package com.hechuang.fruitstore.controller;

import com.hechuang.fruitstore.utils.ImageUtil;
import com.hechuang.fruitstore.utils.QiNiuUtil;
import com.hechuang.fruitstore.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @Author: fing
 * @Description:
 * @Date: 下午6:57 18-1-17
 */
@Controller
@Slf4j
public class IndexController {

    @GetMapping(value = {"/login"})
    public String index() {
        return "admin/login/login";
    }

	@GetMapping(value = {"/"})
	public String main() {
		return "admin/login/login";
	}
	@PostMapping(value = "/upload")
	@ResponseBody
	public ResultVO upload(@RequestBody MultipartFile file){
    	ResultVO resultVO=QiNiuUtil.uploadByStream(ImageUtil.cut(file));
    	//删除上传的文件
		File f = new File("./"+file.getOriginalFilename());
		if (f.exists()){
			f.delete();
		}
		return resultVO;
	}
}
