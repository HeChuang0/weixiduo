package com.hechuang.fruitstore.controller.seller;

import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.form.DiscountSetForm;
import com.hechuang.fruitstore.pojo.DiscountSet;
import com.hechuang.fruitstore.service.DiscountSetServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.ResultVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.simpleframework.xml.core.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.xml.transform.Result;

/**
 * Created by Stefan
 * Create Date 2018-01-16/18:08
 */
@RestController
@Slf4j
@RequestMapping("/DiscountSetAction")
public class DiscountSetController {
    @Autowired
    DiscountSetServiceDao discountSetServiceDao;
    @PostMapping("/add")
    @ApiOperation("商家设置优惠券")
    @CacheEvict(cacheNames = "redPacket",key = "222")
    public ResultVO add(@RequestBody(required = false) @Validate DiscountSetForm form, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            log.error("[设置优惠券]请求参数不完整");
            return ResultVOUtil.error("设置优惠券失败,因为"+bindingResult.getFieldError().getDefaultMessage());
        }
        if (form==null){
            return ResultVOUtil.error("参数为空");
        }
        return ResultVOUtil.success("添加成功",discountSetServiceDao.add(form));
    }
    @GetMapping("/list")
    @ApiOperation("商家后台查看优惠券列表")
    public LayuiResultVo list(@RequestParam(value = "page",defaultValue = "0") Integer page,@RequestParam(value = "size",defaultValue = "8") Integer size){
        PageRequest pageRequest=new PageRequest(page-1,size);
        return new LayuiResultVo(200,"查看红包成功",discountSetServiceDao.get(pageRequest).getContent(),discountSetServiceDao.get(pageRequest).getTotalElements());

    }
    @ApiOperation("商家后台删除过期的优惠券")
    @GetMapping("/delete")
    @CacheEvict(cacheNames = "redPacket",key = "222")
    public LayuiResultVo delete(@RequestParam Integer id){
        try{
            discountSetServiceDao.delete(id);
            return new LayuiResultVo(200,"删除成功");

        }catch (WeiXiDuoException e){
            log.error("删除失败");
            return new LayuiResultVo(200,"删除失败");

        }
    }
    @ApiOperation("商家后台修改有效的优惠券")
    @PostMapping("/update")
    @CacheEvict(cacheNames = "redPacket",key = "222")
    public LayuiResultVo update(@RequestBody(required = false) @Validate DiscountSetForm form, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            log.error("请求参数有误{}",form);
            return new  LayuiResultVo(200,"修改失败");
    }
    DiscountSet discountSet=discountSetServiceDao.update(form);
        if (discountSet==null){
            log.error("优惠券为空");
            return new  LayuiResultVo(200,"修改失败");
        }
        return new  LayuiResultVo(200,"修改成功");
    }

}
