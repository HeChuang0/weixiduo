package com.hechuang.fruitstore.controller.seller;

import com.hechuang.fruitstore.dao.ProductCategoryDao;
import com.hechuang.fruitstore.pojo.ProductCategory;
import com.hechuang.fruitstore.service.ProductCategoryServiceDao;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.ResultVO;
import com.lly835.bestpay.rest.type.Post;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.RequestGroupInfo;
import org.simpleframework.xml.core.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.crypto.spec.IvParameterSpec;
import javax.naming.spi.DirStateFactory;
import java.security.ProtectionDomain;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author: fing
 * @Description:
 * @Date: 上午10:17 18-1-15
 */
@Slf4j
@RestController
@RequestMapping("/ProductCategoryAction")
public class ProductCategoryController {

	@Autowired
	private ProductCategoryServiceDao productCategoryServiceDao;


	@ApiOperation("查看类目列表")
	@GetMapping("/get")
	public LayuiResultVo getProductCategoryList(@RequestParam(value = "page",defaultValue = "1") Integer page, @RequestParam(value = "size",defaultValue = "8") Integer size) {
		if (validate(page) && validate(size)) {
			Map map = productCategoryServiceDao.get(page, size);
			List list = (List) map.get("list");
			if (list != null) {
				return new LayuiResultVo(200, "查询成功", list, (Long) map.get("size"));
			}
		}
		return new LayuiResultVo(200, "查询失败！");
	}

	@ApiOperation("创建类目")
	@PostMapping("/add")
	public ResultVO add(@RequestBody(required = false) ProductCategory productCategory) {
		if (validate(productCategory)) {
			productCategory.setCreateTime(new Date());
			ProductCategory add = productCategoryServiceDao.add(productCategory);
			if (validate(add)) {
				return ResultVOUtil.success("添加成功");
			}
			return ResultVOUtil.error("添加失败，有重复");
		}
		return ResultVOUtil.error("添加失败，数据参数为空");
	}

	@ApiOperation("修改类目")
	@PostMapping("/update")
	public ResultVO update(@RequestBody(required = false) ProductCategory productCategory) {
		if (validate(productCategory)) {
			productCategory.setCreateTime(new Date());
			ProductCategory add = productCategoryServiceDao.update(productCategory);
			if (validate(add)) {
				return ResultVOUtil.success("修改成功");
			}
			return ResultVOUtil.error("修改失败，没有这个ID");
		}
		return ResultVOUtil.error("添加失败，数据参数为空");
	}

	@ApiOperation("删除类目")
	@PostMapping("/delete")
	public ResultVO delete(@RequestParam(required = false) Integer id) {
		if (validate(id)) {
			if (productCategoryServiceDao.delete(id) == 1) {
				return ResultVOUtil.success("删除成功");
			}
			return ResultVOUtil.error("删除失败，没有这个ID");
		}
		return ResultVOUtil.error("添加失败，数据参数为空");
	}
//
//	@ApiOperation("通过产品分类ID查询详情信息")
//	@GetMapping("/{id}")
//	public ResultVO<ProductCategory> getProductCategoryById(@PathVariable Integer id) {
//		ProductCategory one = productCategoryDao.findOne(id);
//		if (validate(one)) {
//			return ResultVOUtil.success("查询成功", one);
//		}
//		return ResultVOUtil.error("查询失败，没有这条数据");
//	}

//	@ApiOperation("添加分类")
//	@PostMapping("/add")
//	public ResultVO saveProductCategory(@RequestBody ProductCategory productCategory) {
//		if (validate(productCategory)) {
//			ProductCategory save = productCategoryDao.save(productCategory);
//			if (validate(save)) {
//				return ResultVOUtil.success("添加成功");
//			}
//		}
//		return ResultVOUtil.error("添加失败");
//	}
//
//
//	@ApiOperation("更新分类信息")
//	@PostMapping("/{id}")
//	public ResultVO updateProductCategory(@PathVariable Integer id, @RequestBody ProductCategory productCategory) {
//		if (validate(productCategory)) {
//			System.out.println(productCategory);
//			productCategoryDao.save(productCategory);
//			return ResultVOUtil.success("更新成功");
//		}
//
//		return ResultVOUtil.error("更新失败");
//	}

	private boolean validate(Object obj) {
		if (null == obj) {
			return false;
		}
		return true;
	}
}
