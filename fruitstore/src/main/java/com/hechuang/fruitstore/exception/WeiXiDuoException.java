package com.hechuang.fruitstore.exception;

import com.hechuang.fruitstore.enums.ResultEnum;

/**
 * Created by Stefan
 * Create Date 2018-01-13/22:33
 */
public class WeiXiDuoException extends RuntimeException {
    private Integer code;

    public WeiXiDuoException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());

        this.code = resultEnum.getCode();
    }

    public WeiXiDuoException(Integer code, String message) {
        super(message);
        this.code = code;
    }

}
