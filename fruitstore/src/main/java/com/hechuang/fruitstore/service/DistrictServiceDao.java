package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.DistrictDao;
import com.hechuang.fruitstore.pojo.District;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: xht
 * @Date: 15:44 2018/2/27
 */
@Service
public class DistrictServiceDao {

    @Autowired
    DistrictDao districtDao;

    public List<District> getByParentId(Integer parentId){
        return districtDao.findDistrictsByParentId(parentId);
    }

    public District getByName(String name){
        return districtDao.findDistinctByName(name);
    }

}
