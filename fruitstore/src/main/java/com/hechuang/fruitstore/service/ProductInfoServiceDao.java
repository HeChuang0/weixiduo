package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.ProductInfoDao;
import com.hechuang.fruitstore.dto.ProductDTO;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.form.ProductForm;
import com.hechuang.fruitstore.pojo.ProductInfo;
import com.hechuang.fruitstore.query.ProductInfoQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.jpa.spi.ParameterRegistration;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: fing
 * @Description:
 * @Date: 下午9:09 18-1-15
 */
@Service
@Slf4j
@CacheConfig(cacheNames = "product")
public class ProductInfoServiceDao {
	@Autowired
	ProductInfoDao productInfoDao;

	public Map get(ProductInfoQueryParams params) {

		Map map = new ConcurrentHashMap();
		map.put("list", productInfoDao.findAll(new PageRequest(params.getPage() - 1,
				params.getPageSize())).getContent());
		map.put("size", productInfoDao.count());
		return map;
	}
	//通过主类目查询商品
	public ProductInfo getByCategoryId(ProductInfoQueryParams params) {
		return productInfoDao.findOne(params.getId());
	}
	//通过子类目查询商品
	public List<ProductInfo> findBySubCategoryId(Integer subId){
		return productInfoDao.findBySubCategoryId(subId);

	}
	public List<ProductInfo> getByCondition(String condition) {
		return productInfoDao.findProductInfoByProductNameContaining(condition);
	}
    @CacheEvict(key = "123")
    @CachePut(key = "#productForm.id")
	public ProductInfo add(ProductForm productForm) {
		ProductInfo productInfo=new ProductInfo();
		BeanUtils.copyProperties(productForm,productInfo);
		productInfo.setCreateTime(new Date());
		productInfo.setSalesVolume(0);
		return productInfoDao.save(productInfo);
	}

    @CacheEvict(key = "123")
    @CachePut(key = "#productForm.id")
	public ProductInfo update(ProductForm productForm) {
		ProductInfo productInfo=new ProductInfo();
		BeanUtils.copyProperties(productForm,productInfo);
		productInfo.setCreateTime(new Date());
		return productInfoDao.save(productInfo);
	}
    @CacheEvict(key = "123")
	public int deleteById(Integer id) {
		return productInfoDao.deleteById(id);
	}

	public List<ProductInfo> findByCategoryId(Integer categoryId) {
		return productInfoDao.findProductInfoByCategoryId(categoryId);
	}
    @CacheEvict(key = "123")
	public int onSale(Integer id) {
		return productInfoDao.onSale(id);
	}
    @CacheEvict(key = "123")
	public int offSale(Integer id) {
		return productInfoDao.onSale(id);
	}

	//减库存decreaseStock
    @CacheEvict(key = "123")
	public void  decreaseStock(List<ProductDTO> productDTOList){
		for(ProductDTO productDTO:productDTOList){
			ProductInfo productInfo=productInfoDao.findOne(productDTO.getProductId());
			if (productInfo==null){
				throw new WeiXiDuoException(ResultEnum.ORDER_NOT_EXIST);
			}
			Integer result=productInfo.getProductStock()-productDTO.getProductQuantity();
			if (result<0){
				throw new WeiXiDuoException(ResultEnum.PRODUCT_STOCK_ERROR);
			}
			productInfo.setProductStock(result);
			productInfo.setSalesVolume(productInfo.getSalesVolume()+productDTO.getProductQuantity());
			productInfoDao.save(productInfo);
		}


	}

	//加库存
    @CacheEvict(key = "123")
	public void  increaseStock(List<ProductDTO> productDTOList){
		for(ProductDTO productDTO:productDTOList){
			ProductInfo productInfo=productInfoDao.findOne(productDTO.getProductId());
			if (productInfo==null){
				throw new WeiXiDuoException(ResultEnum.ORDER_NOT_EXIST);
			}
			Integer result=productInfo.getProductStock()+productDTO.getProductQuantity();
			productInfo.setProductStock(result);
			productInfo.setSalesVolume(productInfo.getSalesVolume()-productDTO.getProductQuantity());
			productInfoDao.save(productInfo);
		}
	}

	//通过商品id查询商品
    @Cacheable(key = "#id")
	public ProductInfo findOne(Integer id){
		return productInfoDao.findOne(id);
	}
	public  List<ProductInfo> getNewProducts(){
		return productInfoDao.getNewProducts();
	}


}
