package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.IntegralDao;
import com.hechuang.fruitstore.pojo.Integral;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Stefan
 * Create Date 2018-01-15/22:05
 */
@Service
public class IntegralServiceDao {
    @Autowired
    IntegralDao integralDao;
    @Transactional
    public Integral receive(Integral integral){
        //1判断数据库中是否有记录
        Integral pojo=integralDao.findByOpenid(integral.getOpenid());
        if (pojo==null){
            return integralDao.save(integral);
        }
        //2有记录就计算积分数
        Integer integralQuantity=pojo.getIntegralQuantity()+integral.getIntegralQuantity();
        integral.setIntegralQuantity(integralQuantity);
        integral.setId(pojo.getId());
        return integralDao.save(integral);
    }

    public Integral findMyIntegral(String openid){
        return integralDao.findByOpenid(openid);
    }
}
