package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.ProductCategoryDao;
import com.hechuang.fruitstore.pojo.ProductCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.security.ProtectionDomain;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: fing
 * @Description:
 * @Date: 下午4:30 18-1-18
 */

@Service
public class ProductCategoryServiceDao {


	@Autowired
	private ProductCategoryDao productCategoryDao;

	public Map get(Integer page, Integer pageSize) {
		Map map = new ConcurrentHashMap();
		map.put("size", productCategoryDao.count());
		map.put("list", productCategoryDao.findAll(new PageRequest(page - 1, pageSize)).getContent());
		return map;
	}

	public ProductCategory add(ProductCategory productCategory) {
		ProductCategory save = null;
		if (productCategory.getId() == null) {
			save = productCategoryDao.save(productCategory);
		}
		return save;
	}

	public ProductCategory update(ProductCategory productCategory) {
		ProductCategory one = productCategoryDao.findOne(productCategory.getId());
		ProductCategory save = null;
		if (one != null) {
			save = productCategoryDao.save(productCategory);
		}
		return save;
	}

	public int delete(Integer id) {
		return productCategoryDao.deleteById(id);
	}

}
