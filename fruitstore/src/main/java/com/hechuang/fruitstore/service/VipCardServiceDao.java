package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.VipCardDao;
import com.hechuang.fruitstore.form.CardForm;
import com.hechuang.fruitstore.pojo.VipCard;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class VipCardServiceDao {

    @Autowired
    VipCardDao vipCardDao;

    //增加和修改
    public VipCard save(CardForm cardForm){
        VipCard vipCard=new VipCard();
        BeanUtils.copyProperties(cardForm,vipCard);
        return vipCardDao.save(vipCard);
    }

    //删除
    public void delete(Integer id){
        vipCardDao.delete(id);
    }

    //查询全部会员卡
    public Map get(){
		Map map = new ConcurrentHashMap();
		map.put("list", vipCardDao.findAll());
		map.put("size", vipCardDao.count());
        return map;
    }

    //根据id查询会员卡
    public VipCard getById(Integer id){
        return vipCardDao.findOne(id);
    }

}
