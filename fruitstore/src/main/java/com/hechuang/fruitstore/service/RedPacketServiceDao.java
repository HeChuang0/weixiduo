package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.DiscountSetDao;
import com.hechuang.fruitstore.dao.RedPacketDao;
import com.hechuang.fruitstore.enums.RedPacketUseTypeEnum;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.pojo.DiscountSet;
import com.hechuang.fruitstore.pojo.RedPacket;
import com.hechuang.fruitstore.vo.DistciuntSetVO;
import com.hechuang.fruitstore.vo.RedPacketVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-01-16/14:46
 */
@Service
@Slf4j
public class RedPacketServiceDao {
    @Autowired
    RedPacketDao redPacketDao;
    @Autowired
    DiscountSetDao discountSetDao;
    public Page<RedPacket> findByOpenid(String openid,Pageable pageable){
        return redPacketDao.findByOpenid(openid,pageable);

    }

//领取红包
    public RedPacket receive(RedPacket redPacket){
        //判断是否领取过
        RedPacket pojo=redPacketDao.findByDiscountIdAndOpenid(redPacket.getDiscountId(),redPacket.getOpenid());
        //数据库中没有数据就领取
        if (pojo==null){
            return redPacketDao.save(redPacket);
        }
        return null;
    }

    //判断是否领取新人红包
    public RedPacket isReceive(String openid,Integer discountId) {
        return redPacketDao.findByDiscountIdAndOpenid(discountId,openid);
    }

    //买家使用红包
    @Transactional
    public BigDecimal userPacket(BigDecimal  orderAmount,Integer id,String openid){
        //1判断优惠券是否存在
        RedPacket redPacket=redPacketDao.findByDiscountIdAndOpenid(id,openid);
        if (redPacket==null){
            log.error("优惠券不存在");
            throw new WeiXiDuoException(ResultEnum.DISCOUNT_NOT_EXIST);
        }
        //2判断优惠券是否可用
        if (redPacket.getUseType()!=RedPacketUseTypeEnum.NOUSE.getCode()){
            log.error("优惠券已使用或者已过期");
            throw new WeiXiDuoException(ResultEnum.DISCOUNT_STATUS_ERROR);
        }
        //3判断是否满减
        DiscountSet discountSet=discountSetDao.findOne(redPacket.getDiscountId());
        if (discountSet.getFullCut().compareTo(orderAmount)==1){
            log.error("未达到满减标准{}{}",discountSet.getFullCut(),orderAmount);
            throw new WeiXiDuoException(ResultEnum.FULLCUTNOT);
        }
        //4.减去折扣
        orderAmount=orderAmount.subtract(discountSet.getCashCoupon());
        //5.修改使用状态
        redPacket.setUseType(RedPacketUseTypeEnum.USEED.getCode());
        redPacketDao.save(redPacket);
        return orderAmount;
    }


    //在订单界面查找可使用的现金券
    @Transactional
    public List<DistciuntSetVO> getOrderDiscountSets(BigDecimal  orderAmount,String openid,Integer useType){
        List<RedPacket> redPacketList=redPacketDao.findByOpenid(openid);
        log.info("{}",redPacketList);
        List<DistciuntSetVO> discountSetList=new ArrayList<>();
        //1.判断是否可用
        for (RedPacket redPacket:redPacketList){
            DistciuntSetVO distciuntSetVO=new DistciuntSetVO();
            if (redPacket.getUseType().equals(RedPacketUseTypeEnum.NOUSE.getCode())){

                DiscountSet discountSet=discountSetDao.findOne(redPacket.getDiscountId());
                log.info("{}",discountSet);
                //2.判断满减
                log.info("{}",(discountSet.getFullCut().compareTo(orderAmount))== -1);
                if (!((discountSet.getFullCut().compareTo(orderAmount))==1)){
                    BeanUtils.copyProperties(discountSet,distciuntSetVO);
                    distciuntSetVO.setUseType(redPacket.getUseType());
                    discountSetList.add(distciuntSetVO);
                }
            }
        }

        return discountSetList;
    }

    public List<RedPacketVO> findUserRed(String openid){
        List<RedPacket> redPacketList=redPacketDao.findByOpenid(openid);
        List<RedPacketVO> redPacketVOList=new ArrayList<>();
        for (RedPacket redPacket : redPacketList) {
           DiscountSet discountSet= discountSetDao.findOne(redPacket.getDiscountId());
           RedPacketVO redPacketVO=new RedPacketVO();
            BeanUtils.copyProperties(discountSet,redPacketVO);
            redPacketVO.setUseType(redPacket.getUseType());
            redPacketVOList.add(redPacketVO);
        }
        return  redPacketVOList;
    }

}
