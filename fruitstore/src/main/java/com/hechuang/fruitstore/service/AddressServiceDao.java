package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.AddressDao;
import com.hechuang.fruitstore.enums.AddressStatusEnum;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.form.AddressForm;
import com.hechuang.fruitstore.pojo.Address;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: fing
 * @Description:
 * @Date: 下午7:47 18-1-16
 */

@Service
@Slf4j
public class AddressServiceDao {

	@Autowired
	AddressDao addressDao;

	public Address add(Address address) {
		List<Address> pojos=addressDao.findAddressByOpenid(address.getOpenid());
		if (pojos.size()==0){
			address.setChecked(AddressStatusEnum.CHECKED.getCode());
		}
		return addressDao.save(address);
	}

	public List<Address> findByOpenid(String id) {
		return addressDao.findAddressByOpenid(id);
	}

	public Address update(Address address) {
		return addressDao.save(address);
	}

	public int delete(Integer id) {
		return addressDao.deleteById(id);
	}

	//是否默认
	@Transactional
	public Address updateChecked(Integer id,String openid){
		//1修改以前的默认地址为普通地址
		Address address=addressDao.findByCheckedAndOpenid(AddressStatusEnum.CHECKED.getCode(),openid);
		if (address!=null){
			address.setChecked(AddressStatusEnum.NOTCHECKED.getCode());
			addressDao.save(address);
		}

		//2修改选中地址为默认地址
		Address address1=addressDao.findOne(id);
		if (address1==null){
			log.error("address1为空{}",address1);
			throw new WeiXiDuoException(ResultEnum.ADDRESS_NOT_EXIST);
		}
		if (address1.getChecked().equals(AddressStatusEnum.NOTCHECKED.getCode())){
			address1.setChecked(AddressStatusEnum.CHECKED.getCode());
		}
		return addressDao.save(address1);
	}

	public Address getOne(Integer id){
		return  addressDao.findOne(id);
	}
}
