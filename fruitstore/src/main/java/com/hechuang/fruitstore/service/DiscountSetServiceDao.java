package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.DiscountSetDao;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.form.DiscountSetForm;
import com.hechuang.fruitstore.pojo.DiscountSet;
import com.hechuang.fruitstore.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Stefan
 * Create Date 2018-01-16/17:10
 */
@Service
@Slf4j
public class DiscountSetServiceDao {
    @Autowired
    DiscountSetDao discountSetDao;
    public DiscountSet add(DiscountSetForm form){
        DiscountSet discountSet=new DiscountSet();
        BeanUtils.copyProperties(form,discountSet);
        return discountSetDao.save(discountSet);
    }

    public Page<DiscountSet> get(Pageable pageable) {
        return discountSetDao.findAll(pageable);
    }

//删除过期的优惠券
    @Transactional
    public void delete(Integer id){
        DiscountSet discountSet=discountSetDao.findOne(id);
        if (discountSet==null){
            log.error("优惠券不存在");
            throw  new WeiXiDuoException(ResultEnum.DISCOUNT_NOT_EXIST);
        }
        if (discountSet.getEndTime().getTime()> System.currentTimeMillis()){
            log.info("优惠券删除失败,还未过期{}",discountSet.getEndTime());
            throw new WeiXiDuoException(ResultEnum.DISCOUNT_STATUS_ERROR);
        }
        discountSetDao.delete(id);
    }
//修改有效期优惠券
    @Transactional
    public DiscountSet update(DiscountSetForm form){
        DiscountSet discountSet=discountSetDao.findOne(form.getId());
        if (discountSet==null){
            log.error("优惠券不存在");
            throw  new WeiXiDuoException(ResultEnum.DISCOUNT_NOT_EXIST);
        }
        if (discountSet.getEndTime().getTime()< System.currentTimeMillis()){
            log.info("优惠券修改失败,已过期{}",discountSet.getEndTime());
            throw new WeiXiDuoException(ResultEnum.DISCOUNT_STATUS_ERROR);

        }
        BeanUtils.copyProperties(form,discountSet);
        return discountSetDao.save(discountSet);

    }
}
