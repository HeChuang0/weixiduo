package com.hechuang.fruitstore.service;

import com.google.common.base.Splitter;
import com.hechuang.fruitstore.dao.CartDao;
import com.hechuang.fruitstore.dao.ProductInfoDao;
import com.hechuang.fruitstore.enums.ShoppingCartEnum;
import com.hechuang.fruitstore.pojo.ProductInfo;
import com.hechuang.fruitstore.pojo.ShoppingCart;
import com.hechuang.fruitstore.utils.BigDecimalUtil;
import com.hechuang.fruitstore.utils.ListUtil;
import com.hechuang.fruitstore.utils.ResultVOUtil;
import com.hechuang.fruitstore.vo.CartProductVO;
import com.hechuang.fruitstore.vo.CartVO;
import com.hechuang.fruitstore.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Create by ToFree on 2018/1/15
 */
@Service
@Slf4j
public class CartServiceDao {
    @Autowired
    private CartDao cartDao;
    @Autowired
    private ProductInfoDao productInfoDao;

    /**
     * 添加购物车的方法
     *
     * @param openid
     * @param productId
     * @param count
     * @return
     */
    public ResultVO add(String openid, Integer productId, int count) {
        ShoppingCart cart = cartDao.getCartByProductIdOpenid(openid, productId);
        if (cart == null) {
            //这个产品不再购物车，需要新增一个产品的记录
            ShoppingCart cartItem = new ShoppingCart();
            cartItem.setProductQuantity(count);
            cartItem.setCartStatus(ShoppingCartEnum.CHECKED.getCode());
            cartItem.setProductId(productId);
            cartItem.setOpenid(openid);
            cartDao.save(cartItem);
        } else {
            //这个产品已经在购物车了
            //数量相加
            count = count + cart.getProductQuantity();
            cart.setProductQuantity(count);
            cartDao.save(cart);
        }
        return this.list(openid);
    }

    /**
     * 更新购物车
     *
     * @param openid
     * @param productId
     * @param count
     * @return
     */
    public ResultVO update(String openid, Integer productId, Integer count) {
        log.info("{},{},{}",openid,productId,count);
        ShoppingCart cart = cartDao.findByProductIdAndOpenid(productId, openid);
        log.info("cart{}",cart);
        if (cart != null) {
            //购物车存在此商品，更新数量
            cart.setProductQuantity(count);
        }
        cartDao.save(cart);
        return this.list(openid);
    }

    /**
     * 将商品移出购物车
     *
     * @param openid
     * @param productIds "1,2,3"
     * @return
     */
    @Transactional
    public ResultVO deleteProduct(String openid, String productIds) {
//        cartDao.deleteByOpenidProductIds(openid, productId);
//        ShoppingCart cart = cartDao.getCartByProductIdOpenid(openid, productId);
//        cartDao.delete(cart);
        List<String> productIdList = Splitter.on(",").splitToList(productIds);
        if (ListUtil.isEmpty(productIdList)) {
            return ResultVOUtil.error("参数错误");
        }
        for (String s: productIdList) {
            cartDao.deleteByOpenidProductIds(openid,Integer.valueOf(s));
        }
        int total = productIdList.size();
        ResultVO resultVO = this.list(openid);
        resultVO.setTotal(total);
        return resultVO;

    }

    public ResultVO getCartByProductIdOpenid(String openid, Integer productId) {
        return ResultVOUtil.success("chengg", cartDao.getCartByProductIdOpenid(openid, productId));
    }

    /**
     * 返回当前购物车信息
     *
     * @param openid
     * @return
     */
    public ResultVO list(String openid) {
        ResultVO resultVO = new ResultVO("操作成功");
        CartVO cartVO = this.getCartVOLimit(openid);
        resultVO.setTotal(this.getCartProductCount(openid));
        resultVO.getData().add(cartVO);
        return resultVO;
    }

    /**
     * 全选/反全选
     *
     * @param openid
     * @param checked
     * @return
     */
    public ResultVO selectOrUnselectAll(String openid, Integer checked) {
        cartDao.checkedOrUncheckedAllProduct(openid, checked);
        return this.list(openid);
    }

    /**
     * 选择/反选
     *
     * @param openid
     * @param checked
     * @param productId
     * @return
     */
    public ResultVO selectOrUnselect(String openid, Integer productId, Integer checked) {
        cartDao.checkedOrUncheckedProduct(openid, productId, checked);
        return this.list(openid);
    }

    /**
     * 获取购物车商品的数量
     *
     * @param openid
     * @return
     */
    public int getCartProductCount(String openid) {
        return cartDao.getCartProductCount(openid);
    }

//    public ResultVO createOrder(String openid) {
//        return null;
//    }


    /**
     * 这个是操作购物车的核心方法
     * 拼装并返回CartVO
     * 查询当前购物车的商品
     * 计算商品总价和购物车总价
     * 判断库存，如果库存不足则设置购物车数量等于库存
     *
     * @param openid
     * @return
     */

    protected CartVO getCartVOLimit(String openid) {
        CartVO cartVO = new CartVO();
        List<ShoppingCart> cartList = cartDao.getCartByOpenid(openid);
        List<CartProductVO> cartProductVOList = new ArrayList();
        BigDecimal cartTotalPrice = new BigDecimal("0");
        if (!ListUtil.isEmpty(cartList)) {
            for (ShoppingCart cartItem : cartList) {
                CartProductVO cartProductVO = new CartProductVO();
                cartProductVO.setId(cartItem.getId());
                cartProductVO.setOpenid(openid);
                cartProductVO.setProductId(cartItem.getProductId());

                ProductInfo productInfo = productInfoDao.findOne(cartItem.getProductId());
                if (productInfo != null) {
                    cartProductVO.setProductIcon(productInfo.getProductIcon());
                    cartProductVO.setProductName(productInfo.getProductName());
                    cartProductVO.setProductDescription(productInfo.getProductDescription());
                    cartProductVO.setProductStatus(productInfo.getProductStatus());
                    cartProductVO.setProductPrice(productInfo.getProductPrice());
                    cartProductVO.setProductStock(productInfo.getProductStock());

                    //判断库存
                    int buyLimitCount = 0;
                    if (productInfo.getProductStock() >= cartItem.getProductQuantity()) {
                        cartProductVO.setLimitQuantity("LIMIT_NUM_SUCCESS");
                        //库存充足时
                        buyLimitCount = cartItem.getProductQuantity();
                    } else {
                        buyLimitCount = productInfo.getProductStock();
                        cartProductVO.setLimitQuantity("LIMIT_NUM_FAIL");
                        //购物车中更新有效库存
                        ShoppingCart cartForQuantity = new ShoppingCart();
                        BeanUtils.copyProperties(cartForQuantity, cartItem);
                        cartForQuantity.setProductQuantity(buyLimitCount);
                        cartDao.save(cartForQuantity);
                    }
                    cartProductVO.setQuantity(buyLimitCount);
                    //计算总价
                    cartProductVO.setProductTotalPrice(BigDecimalUtil.mul(productInfo.getProductPrice().doubleValue(), cartProductVO.getQuantity()));
                    cartProductVO.setProductChecked(cartItem.getCartStatus().intValue());
                }
                if (cartItem.getCartStatus() == ShoppingCartEnum.CHECKED.getCode()) {
                    //如果已经勾选，增加到整个总价的购物车中
                    cartTotalPrice = BigDecimalUtil.add(cartTotalPrice.doubleValue(), cartProductVO.getProductTotalPrice().doubleValue());
                }
                cartProductVOList.add(cartProductVO);
            }
        }
        cartVO.setCartTotalPrice(cartTotalPrice);
        cartVO.setCartProductVOList(cartProductVOList);
        cartVO.setAllChecked(this.getAllCheckedStatus(openid));
        //cartVO.setImgageHost();
        return cartVO;
    }

    /**
     * 判断购物车的商品是不是全选状态
     *
     * @param openid
     * @return
     */
    private Boolean getAllCheckedStatus(String openid) {
        if (openid == null) {
            return false;
        }
        return cartDao.getCartProductCheckedStatusByOpenid(openid) == ShoppingCartEnum.UNCHECKED.getCode();
    }
}
