package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.VipMemberDao;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.form.VipMemberForm;
import com.hechuang.fruitstore.pojo.VipMember;
import com.hechuang.fruitstore.query.VipMemberQueryParams;
import com.hechuang.fruitstore.vo.VipMemberVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Stefan
 * Create Date 2018-01-15/0:02
 */
@Service
@Slf4j
public class VipMemberServiceDao {
    @Autowired
    VipMemberDao vipMemberDao;

    @Transactional
    public VipMember addVipMember(VipMember vipMember) {
        //判断数据库中是否有这个会员
        VipMember member = vipMemberDao.findByOpenid(vipMember.getOpenid());
        if (member == null) {
            vipMember.setVipCardId(2);
            return vipMemberDao.save(vipMember);
        }
        return null;
    }


    public VipMember updateVipMember(VipMemberForm vipMemberForm){
        VipMember pojo=vipMemberDao.findByOpenid(vipMemberForm.getOpenid());
        if (pojo==null){
            log.error("会员用户不存在");
            throw new WeiXiDuoException(ResultEnum.PARAM_ERROR);
        }
        vipMemberForm.setId(pojo.getId());
        vipMemberForm.setVipCardId(pojo.getVipCardId());
        BeanUtils.copyProperties(vipMemberForm,pojo);
        return vipMemberDao.save(pojo);
    }


    public VipMember findByOpenid(String openId){
        if (openId.equals("")){
            log.error("OpenId为空");
            throw new WeiXiDuoException(ResultEnum.PARAM_ERROR);
        }
        return vipMemberDao.findByOpenid(openId);
    }


    public Map get(VipMemberQueryParams params) {
        PageRequest request = new PageRequest(params.getPage()-1, params.getPageSize(), null);
        Map map = new ConcurrentHashMap();
        map.put("size", vipMemberDao.count());
        map.put("list", vipMemberDao.findAll(request).getContent());
        return map;
    }

}
