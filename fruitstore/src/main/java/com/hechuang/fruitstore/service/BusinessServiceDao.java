package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.config.BaiduMapConfig;
import com.hechuang.fruitstore.config.BaiduMapResultBean;
import com.hechuang.fruitstore.dao.BusinessDao;
import com.hechuang.fruitstore.form.BusinessForm;
import com.hechuang.fruitstore.pojo.Business;
import com.hechuang.fruitstore.utils.CountDistanceUtil;
import com.hechuang.fruitstore.utils.HttpClientUtil;
import com.hechuang.fruitstore.utils.JsonUtil;
import com.hechuang.fruitstore.vo.BusinessVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-02-26/17:33
 */
@Service
@Slf4j
public class BusinessServiceDao {
    @Autowired
    BusinessDao businessDao;
    public Business save(BusinessForm form){
        Business business=new Business();
        BeanUtils.copyProperties(form,business);
        log.info("{}",business);
        return businessDao.save(business);
    }

    public void delete(Integer id){
        businessDao.delete(id);
    }

    public Business finfOne(Integer id){
        return businessDao.findOne(id);
    }

    public Page<Business> getBusinesses(PageRequest pageRequest){
        return businessDao.findAll(pageRequest);
    }

    public List<BusinessVO> nearbyBusinesses( double lat,
            double lng ){
        List<BusinessVO> businessVOList=new ArrayList<>();
        List<Business> businessList=businessDao.findAll();
        for (Business business :businessList){
            BaiduMapConfig baiduMapConfig=new BaiduMapConfig();
            String url=baiduMapConfig.getGeocodingUrl();
            //1.拼接url
            String realUrl=String.format(url,business.getBusinessAddress(),"json",baiduMapConfig.getAk());
            //2.发送请求并json转为object
            BaiduMapResultBean baiduMapResultBean=new BaiduMapResultBean();
            baiduMapResultBean= (BaiduMapResultBean) JsonUtil.jsonToObject(HttpClientUtil.sendGet(realUrl), baiduMapResultBean);
            //3.获得商户的纬度和经度
            double business_lat=baiduMapResultBean.getResult().getLocation().getLat();
            double business_lng=baiduMapResultBean.getResult().getLocation().getLng();
            //4.计算两点的距离
            double distance=CountDistanceUtil.getDistance(lat,lng,business_lat,business_lng);

            BusinessVO businessVO=new BusinessVO();
            BeanUtils.copyProperties(business,businessVO);
            businessVO.setDistance(distance);
            businessVOList.add(businessVO);
        }
        return businessVOList;




    }

}
