package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.SignDao;
import com.hechuang.fruitstore.form.SignForm;
import com.hechuang.fruitstore.pojo.Integral;
import com.hechuang.fruitstore.pojo.Sign;
import com.hechuang.fruitstore.utils.ListUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-01-15/23:36
 */
@Slf4j
@Service
public class SignServiceDao {
    @Autowired
    SignDao signDao;
    @Autowired
    IntegralServiceDao integralServiceDao;


    public  List<Sign> signList(String openid){
        return signDao.findByOpenid(openid);
    }

    @Transactional
    public Sign sign(SignForm signForm){
        //判断今天是否签到
        Integral integral=new Integral();
        integral.setOpenid(signForm.getOpenid());
        integral.setIntegralQuantity(signForm.getIntegralQuantity());

        Sign sign=new Sign();
        BeanUtils.copyProperties(signForm,sign);

        List<Sign> pojos=signDao.findByOpenid(signForm.getOpenid());
        if (ListUtil.isEmpty(pojos)){
            integralServiceDao.receive(integral);
            return signDao.save(sign);
        }
        List<String> dates=new ArrayList<>();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        for (Sign s:pojos){
            dates.add(formatter.format(s.getSignDate()));
        }
        Date date = new Date();
        if (!dates.contains(formatter.format(date))){
            integralServiceDao.receive(integral);
            return signDao.save(sign);
        }
        return null;
    }
}
