package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.ProductSubcategoryDao;
import com.hechuang.fruitstore.form.SubcategoryForm;
import com.hechuang.fruitstore.pojo.ProductSubcategory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-02-07/20:17
 */
@Service
public class ProductSubcategoryServiceDao {
    @Autowired
    ProductSubcategoryDao productSubcategoryDao;
    //新增和修改的接口
    public ProductSubcategory save(SubcategoryForm form){
        ProductSubcategory productSubcategory=new ProductSubcategory();
        BeanUtils.copyProperties(form,productSubcategory);
        productSubcategory.setCreateTime(new Date());
        return productSubcategoryDao.save(productSubcategory);
    }

    //通过主类目查询子类目
    public List<ProductSubcategory> findBymaincategoryId(Integer maincategoryId, PageRequest pageRequest){
        return productSubcategoryDao.findByMaincategoryId(maincategoryId,pageRequest).getContent();
    }

    public List<ProductSubcategory> BuyerFindBymaincategoryId(Integer maincategoryId){
        return productSubcategoryDao.findByMaincategoryId(maincategoryId);
    }

    //通过id查询单条数据
    public ProductSubcategory findOne(Integer id){
        return productSubcategoryDao.findOne(id);
    }

}
