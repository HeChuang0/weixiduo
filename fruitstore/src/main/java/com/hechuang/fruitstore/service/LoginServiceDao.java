package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.ManagerDao;
import com.hechuang.fruitstore.pojo.Manager;
import com.hechuang.fruitstore.utils.StringUtil;
import com.hechuang.fruitstore.vo.LayuiResultVo;
import com.hechuang.fruitstore.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Create by ToFree on 2018/1/19
 */
@Service
@Slf4j
public class LoginServiceDao {
    @Autowired
    private ManagerDao managerDao;
    public Boolean login(String managerName, String managerPW) {
        if (StringUtil.isEmpty(managerName) || StringUtil.isEmpty(managerPW)) {
            return false;
        }
        Manager manager = managerDao.getByName(managerName);
        if (manager == null) {
            return false;
        }
        if (manager.getManagerPassword().toString().equals(managerPW)) {
            return true;
        }
        Boolean sd = manager.getManagerPassword().toString().equals(managerPW);
        return false;
    }
}
