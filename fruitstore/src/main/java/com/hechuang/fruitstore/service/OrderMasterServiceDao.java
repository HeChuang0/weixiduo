package com.hechuang.fruitstore.service;

import com.hechuang.fruitstore.dao.DiscountSetDao;
import com.hechuang.fruitstore.dao.OrderDetailDao;
import com.hechuang.fruitstore.dao.OrderMasterDao;
import com.hechuang.fruitstore.dto.ProductDTO;
import com.hechuang.fruitstore.dto.OrderDTO;
import com.hechuang.fruitstore.enums.OrderStatusEnum;
import com.hechuang.fruitstore.enums.PayStatusEnum;
import com.hechuang.fruitstore.enums.ResultEnum;
import com.hechuang.fruitstore.exception.WeiXiDuoException;
import com.hechuang.fruitstore.pojo.DiscountSet;
import com.hechuang.fruitstore.pojo.OrderDetail;
import com.hechuang.fruitstore.pojo.OrderMaster;
import com.hechuang.fruitstore.utils.DateUtil;
import com.hechuang.fruitstore.utils.KeyUtil;
import com.hechuang.fruitstore.vo.OrderVO;
import com.hechuang.fruitstore.weixin.service.PayServiceDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Create by ToFree on 2018/1/12
 */
@Service
@Slf4j
public class OrderMasterServiceDao {
	@Autowired
	OrderMasterDao orderMasterDao;
	@Autowired
	OrderDetailDao orderDetailDao;
	@Autowired
	ProductInfoServiceDao productInfoServiceDao;
	//查看订单详情
	@Autowired
	CartServiceDao cartServiceDao;
	@Autowired
	private WebSocket webSocket;
	@Autowired
	private PayServiceDao payServiceDao;
	@Autowired
	private RedPacketServiceDao redPacketServiceDao;
	@Autowired
	DiscountSetDao discountSetDao;

	/*
	 *
	 * 查看订单详情
	 * */
	public OrderVO detail(String orderid) {
		OrderMaster orderMaster = orderMasterDao.findByOrderid(orderid);
		if (orderMaster == null) {
			throw new WeiXiDuoException(ResultEnum.ORDER_NOT_EXIST);
		}
		List<OrderDetail> orderDetailList = orderDetailDao.findByOrderid(orderMaster.getOrderid());
		if (CollectionUtils.isEmpty(orderDetailList)) {
			throw new WeiXiDuoException(ResultEnum.ORDERDETAIL_NOT_EXIST);
		}

		OrderVO orderVO = new OrderVO();
		BeanUtils.copyProperties(orderMaster, orderVO);
		orderVO.setOrderDetails(orderDetailList);
		//查看是否使用优惠券
		if (orderMaster.getDiscountId()!=null){
			DiscountSet discountSet=discountSetDao.findOne(orderMaster.getDiscountId());
			orderVO.setDiscountSet(discountSet);
		}
		return orderVO;
	}

	//用户查看自己订单列表
	public List<OrderMaster> buyerOrderList(String openid) {
		return orderMasterDao.findByOpenidOrderByOrderidDesc(openid);
	}

	//商家查看订单列表
	@Transactional
	public Map sellerOrderList(String pastDate,String currnetDate,PageRequest pageRequest) {
		Long size= orderMasterDao.countByCreateTimeBetweenAndOrderStatusNot(DateUtil.getDate(pastDate),DateUtil.getDate(currnetDate),OrderStatusEnum.CANCEL.getCode());
		List<OrderMaster> orderMasterList=orderMasterDao.findByCreateTimeBetweenAndOrderStatusNotOrderByOrderidDesc(DateUtil.getDate(pastDate),DateUtil.getDate(currnetDate),OrderStatusEnum.CANCEL.getCode(),pageRequest).getContent();
		log.info("商家查看订单列表{}",orderMasterList);
		Map map = new ConcurrentHashMap<>();
		map.put("list", orderMasterList);
		map.put("size", size);
		return map;

	}

	//取消订单
	@Transactional
	public void cancel(OrderDTO orderDTO) {
		//1判断订单状态
		if (!orderDTO.getOrderStatus().equals(OrderStatusEnum.NEW.getCode())) {
			log.error("取消订单状态不正确 orderId={}, orderStatus={}", orderDTO.getOrderid(), orderDTO.getOrderStatus());
			throw new WeiXiDuoException(ResultEnum.ORDER_STATUS_ERROR);
		}

		//4.如果付了钱，退钱
		if (orderDTO.getPayStatus().equals(PayStatusEnum.SUCCESS.getCode())) {
			//退钱
			payServiceDao.refund(orderDTO);
			orderDTO.setPayStatus(PayStatusEnum.REFUND.getCode());
		}

		//2修改状态
		OrderMaster orderMaster = new OrderMaster();
		BeanUtils.copyProperties(orderDTO, orderMaster);
		orderMaster.setOrderStatus(OrderStatusEnum.CANCEL.getCode());
		orderMasterDao.save(orderMaster);

		//3.反还库存
		List<OrderDetail> orderDetailList = orderDTO.getOrderDetails();
		if (CollectionUtils.isEmpty(orderDTO.getOrderDetails())) {
			log.error("【取消订单】订单中无商品详情, orderDTO={}", orderDTO);
			throw new WeiXiDuoException(ResultEnum.ORDER_DETAIL_EMPTY);
		}
		List<ProductDTO> ProductDTOList = orderDTO.getOrderDetails().stream().map(e -> new ProductDTO(e.getProductId(), e.getProductQuantity())).collect(Collectors.toList());
		productInfoServiceDao.increaseStock(ProductDTOList);
	}


	//发货
	@Transactional
	public void finish(OrderDTO orderDTO) {
		//判断订单状态
		if (!orderDTO.getOrderStatus().equals(OrderStatusEnum.NEW.getCode())) {
			log.error("【发货订单】订单状态不正确, orderId={}, orderStatus={}", orderDTO.getOrderid(), orderDTO.getOrderStatus());
			throw new WeiXiDuoException(ResultEnum.ORDER_STATUS_ERROR);
		}

		//修改订单状态
		orderDTO.setOrderStatus(OrderStatusEnum.FINISHED.getCode());
		OrderMaster orderMaster = new OrderMaster();
		BeanUtils.copyProperties(orderDTO, orderMaster);
		OrderMaster updateResult = orderMasterDao.save(orderMaster);
		if (updateResult == null) {
			log.error("【发货订单】更新失败, orderMaster={}", orderMaster);
			throw new WeiXiDuoException(ResultEnum.ORDER_UPDATE_FAIL);
		}
	}

	//创建订单点提交订单时生成
	@Transactional
	public OrderDTO create(OrderDTO orderDTO) {
		//1生成订单orderid
		String orderid = KeyUtil.genUniqueKey();
		orderDTO.setOrderid(orderid);
		//是否使用优惠券
		if (orderDTO.getDiscountId()!=null){
			log.info("使用优惠券");
			orderDTO.setOrderAmount(redPacketServiceDao.userPacket(orderDTO.getOrderAmount(),orderDTO.getDiscountId(),orderDTO.getOpenid()));
		}

		//2订单入库
		OrderMaster orderMaster = new OrderMaster();
		BeanUtils.copyProperties(orderDTO, orderMaster);
		orderMasterDao.save(orderMaster);
		//3订单详情入库
		for (OrderDetail orderDetail : orderDTO.getOrderDetails()) {
			orderDetail.setOrderid(orderid);
			orderDetailDao.save(orderDetail);
		}
		//减库存
		List<ProductDTO> productDTOList = orderDTO.getOrderDetails().stream().map(e -> new ProductDTO(e.getProductId(), e.getProductQuantity())).collect(Collectors.toList());



		productInfoServiceDao.decreaseStock(productDTOList);
		return orderDTO;
	}

	/*
	 * 修改支付状态
	 * */
	@Transactional
	public OrderDTO paid(OrderDTO orderDTO) {
		log.info("进入修改支付状态");
		//判断订单状态
		if (!orderDTO.getOrderStatus().equals(OrderStatusEnum.NEW.getCode())) {
			log.error("【订单支付完成】订单状态不正确, orderId={}, orderStatus={}", orderDTO.getOrderid(), orderDTO.getOrderStatus());
			throw new WeiXiDuoException(ResultEnum.ORDER_STATUS_ERROR);
		}

		//判断支付状态
		if (!orderDTO.getPayStatus().equals(PayStatusEnum.WAIT.getCode())) {
			log.error("【订单支付完成】订单支付状态不正确, orderDTO={}", orderDTO);
			throw new WeiXiDuoException(ResultEnum.ORDER_PAY_STATUS_ERROR);
		}

		//修改支付状态
		orderDTO.setPayStatus(PayStatusEnum.SUCCESS.getCode());
		OrderMaster orderMaster = new OrderMaster();
		BeanUtils.copyProperties(orderDTO, orderMaster);
		log.info("【支付状态】{}", orderMaster);
		OrderMaster updateResult = orderMasterDao.save(orderMaster);
		log.info("【支付状态后】{}", orderMaster);
		if (updateResult == null) {
			log.error("【订单支付完成】更新失败, orderMaster={}", orderMaster);
			throw new WeiXiDuoException(ResultEnum.ORDER_UPDATE_FAIL);
		}

		return orderDTO;
	}

//查看最近三天未发货的新订单
	public  Map latelyOrderList(Integer page,Integer size){
		Map map = new ConcurrentHashMap<>();
		List<OrderMaster> orderMasterList=orderMasterDao.latelyOrderList(page,size);
		log.info("查看最近三天未发货的新订单{}",orderMasterList);
		map.put("list", orderMasterList);
		log.info("查看最近三天{}",orderMasterList);
		map.put("size", orderMasterDao.countlatelyOrder());
		log.info("查看最近三{}",orderMasterList);
		return map;
	}

	//查看退款的订单
	public  Map refundOrderList(PageRequest pageRequest){
		Map map = new ConcurrentHashMap<>();
		map.put("list", orderMasterDao.findByOrderStatusOrderByOrderidDesc(OrderStatusEnum.CANCEL.getCode(),pageRequest).getContent());
		map.put("size", orderMasterDao.countByOrderStatus(OrderStatusEnum.CANCEL.getCode()));
		return map;
	}

	//删除退款订单
	@Transactional
	public void delete(String orderid){
		//1.查是否有此订单
		OrderMaster orderMaster=orderMasterDao.findOne(orderid);
		if (orderMaster==null){
			log.error("[删除订单失败]，订单不存在");
			throw new WeiXiDuoException(ResultEnum.ORDER_NOT_EXIST);
		}
		//2.判断是否为取消的订单
		if (orderMaster.getOrderStatus()!=OrderStatusEnum.CANCEL.getCode()){
			log.error("[删除订单失败]，订单状态有误");
			throw new WeiXiDuoException(ResultEnum.ORDER_STATUS_ERROR);
		}
		//3.删除
		orderMasterDao.delete(orderid);
	}

}
