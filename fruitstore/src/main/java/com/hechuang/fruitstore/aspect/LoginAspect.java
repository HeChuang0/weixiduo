package com.hechuang.fruitstore.aspect;

        import com.hechuang.fruitstore.constant.CookieConstant;
        import com.hechuang.fruitstore.constant.RedisConstant;
        import com.hechuang.fruitstore.exception.LoginException;
        import com.hechuang.fruitstore.utils.CookieUtil;
        import com.hechuang.fruitstore.utils.StringUtil;
        import lombok.extern.slf4j.Slf4j;
        import org.aspectj.lang.annotation.Aspect;
        import org.aspectj.lang.annotation.Before;
        import org.aspectj.lang.annotation.Pointcut;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.data.redis.core.StringRedisTemplate;
        import org.springframework.stereotype.Component;
        import org.springframework.web.context.request.RequestContextHolder;
        import org.springframework.web.context.request.ServletRequestAttributes;

        import javax.servlet.http.Cookie;
        import javax.servlet.http.HttpServletRequest;

/**
 * Create by ToFree on 2018/1/19
 */
@Aspect
@Component
@Slf4j
public class LoginAspect {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Pointcut("execution(public * com.hechuang.fruitstore.controller.seller.*.*(..))"
    )
    public void verify() {
    }

    @Before("verify()")
    public void doVerify() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        //查询cookie
        Cookie cookie = CookieUtil.get(request, CookieConstant.TOKEN);
        if (cookie == null) {
            log.warn("[登录校验]：Cookie查不到token");
            throw new LoginException();
        }
        //查redis
        String tokenValue = redisTemplate.opsForValue().get(String.format(RedisConstant.TOKEN_PREFIX, cookie.getValue()));
        if (StringUtil.isEmpty(tokenValue)) {
            log.warn("[登录校验]：Redis查不到token");
            throw new LoginException();
        }
    }
}
