package com.hechuang.fruitstore.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * Create by stefan
 * Date on 2018-03-19  12:53
 * Convertion over Configuration!
 */
@Slf4j
@Aspect
@Component
public class LogAspect {
    @Pointcut("execution(public * com.hechuang.fruitstore.controller.buyer.*.*(..))")
    public void logPointCut() {
    }

    @Before("logPointCut()")
    public void saveSysLog(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        log.info("method:{}",method);
        //请求的方法名
        String className = joinPoint.getTarget().getClass().getName();
        //log.info("请求完全类名：{}",className);
        String methodName = signature.getName();
        //请求的参数
        Object[] args = joinPoint.getArgs();
        String[] paramNames=signature.getParameterNames();
        log.info("请求参数：{}",paramNames);
        log.info("{}",args);
    }
}
