package com.hechuang.fruitstore.config;

import lombok.Data;

/**
 * Created by Stefan
 * Create Date 2018-02-26/16:15
 */
@Data
public class BaiduMapResultBean {
    private Integer status;
    private Result result;
    @Data
    public class Result{
        LocationBean location=new LocationBean();
        Integer precise;
        Integer confidence;
        String level;
    }
    @Data
    public class LocationBean{
        double lng;//经度
        double lat;//纬度
    }
}
