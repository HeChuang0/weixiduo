package com.hechuang.fruitstore.config;

import lombok.Getter;

/**
 * Created by Stefan
 * Create Date 2018-02-26/14:15
 */
@Getter
public class BaiduMapConfig {
    private String geocodingUrl="http://api.map.baidu.com/geocoder/v2/?address=%s&output=%s&ak=%s";
    private String ak="K3WRXBCoQ2Z3kDwgnFKPiRZSVOqO1goE";
}
