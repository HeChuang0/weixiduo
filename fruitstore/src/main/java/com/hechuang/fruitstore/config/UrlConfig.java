package com.hechuang.fruitstore.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Stefan
 * Create Date 2018-01-31/9:40
 */
@Data
@ConfigurationProperties(prefix = "wechat.projectUrl")
@Component
public class UrlConfig {
    /**
     * 微信公众平台授权url
     */
    public String wechatMpAuthorize;

    /**
     * 维喜多url
     */
    public String weixiduo;

}
