package com.hechuang.fruitstore.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Create by ToFree on 2018/1/15
 */
@Data
public class CartProductVO implements Serializable{

    private static final long serialVersionUID = 1714226057728800054L;
    private Integer id;
    private String openid;
    private Integer productId;
    private Integer quantity;//此购物车商品的数量
    private String productName;
    private String productDescription;
    private String productIcon;
    private BigDecimal productPrice;
    private Integer productStatus;//状态（0正常，1下架）
    private Integer productStock;//库存
    private BigDecimal productTotalPrice;//此商品的总价
    private Integer productChecked;//此商品是否勾选
    private String limitQuantity;//限制数量的返回结果
    //限制购买数量，LIMIT_NUM_SUCCESS表示库存足够，LIMIT_NUM_FAIL表示库存不足


}
