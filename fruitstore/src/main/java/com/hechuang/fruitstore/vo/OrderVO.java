package com.hechuang.fruitstore.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hechuang.fruitstore.enums.OrderStatusEnum;
import com.hechuang.fruitstore.enums.PayStatusEnum;
import com.hechuang.fruitstore.pojo.DiscountSet;
import com.hechuang.fruitstore.pojo.OrderDetail;
import com.hechuang.fruitstore.utils.EnumUtil;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-01-26/18:40
 */
@Data
public class OrderVO {
    private String orderid;//以时间生成的字符串
    private String address;//买家地址
    private String openid;
    private BigDecimal orderAmount;//订单总金额
    private Integer discountId;
    private Integer orderStatus= OrderStatusEnum.NEW.getCode();//订单状态, 默认为新下单
    private Integer payStatus= PayStatusEnum.WAIT.getCode();//支付状态, 默认未支付
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;
    @NotNull(message = "商品list不能为空")
    private List<OrderDetail> orderDetails;
    private DiscountSet discountSet;

    @JsonIgnore
    public OrderStatusEnum getOrderStatusEnum() {
        return EnumUtil.getByCode(orderStatus, OrderStatusEnum.class);
    }

    @JsonIgnore
    public PayStatusEnum getPayStatusEnum() {
        return EnumUtil.getByCode(payStatus, PayStatusEnum.class);
    }
}
