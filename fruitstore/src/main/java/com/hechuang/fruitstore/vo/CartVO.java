package com.hechuang.fruitstore.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Create by ToFree on 2018/1/15
 */
@Data
public class CartVO implements Serializable{

    private static final long serialVersionUID = -7723314748612735693L;
    List<CartProductVO> cartProductVOList;
    private BigDecimal cartTotalPrice;
    private Boolean allChecked;//是否被全选
    private String imgageHost;//购物车图片
}
