package com.hechuang.fruitstore.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Create by ToFree on 2018/1/11
 */
@Data
public class ResultVO <T> implements Serializable{

    private static final long serialVersionUID = -2819549758611900542L;

    /** 错误码. */
    private Boolean success = true;

    /** 提示信息. */
    private String msg;

    /** 具体内容. */
    private List data = new ArrayList(0);

    /** 返回记录数. */
    private long total;

    public ResultVO() {}

    public ResultVO(String msg) {
        super();
        this.msg = msg;
    }

    public void setErrMsg(String msg){
        this.success=false;
        this.msg=msg;
    }

    public void add(List datas){
        if(datas!=null){
            this.data.addAll(datas);
            this.total=this.data.size();
        }
    }
    public void add(Object data){
        this.data.add(data);
        this.total=1;
    }

    public ResultVO(List datas) {
        super();
        this.data = datas;
        if(datas!=null)
            this.total=datas.size();
    }

    public ResultVO(boolean success, String msg) {
        super();
        this.success = success;
        this.msg = msg;
    }

    public ResultVO(Boolean success, String msg, List data, long total) {
        this.success = success;
        this.msg = msg;
        this.data = data;
        this.total = total;
    }
}
