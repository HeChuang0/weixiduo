package com.hechuang.fruitstore.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: fing
 * @Description:
 * @Date: 下午2:40 18-1-17
 */

@Data
public class LayuiResultVo <T> {
	/** 错误码. */
	private Integer success = 200;

	/** 提示信息. */
	private String msg;

	/** 具体内容. */
	private List data=new ArrayList(10);

	/** 返回记录数. */
	private long total;

	public LayuiResultVo() {}

	public LayuiResultVo(String msg) {
		super();
		this.msg = msg;
	}

	public void setErrMsg(String msg){
		this.success=400;
		this.msg=msg;
	}

	public void add(List datas){
		if(datas!=null){
			this.data.addAll(datas);
			this.total=this.data.size();
		}
	}
	public void add(Object data){
		this.data.add(data);
		this.total=1;
	}

	public LayuiResultVo(List datas) {
		super();
		this.data = datas;
		if(datas!=null)
			this.total=datas.size();
	}

	public LayuiResultVo(Integer success, String msg) {
		super();
		this.success = success;
		this.msg = msg;
	}

	public LayuiResultVo(Integer success, String msg, List data, long total) {
		this.success = success;
		this.msg = msg;
		this.data = data;
		this.total = total;
	}
}