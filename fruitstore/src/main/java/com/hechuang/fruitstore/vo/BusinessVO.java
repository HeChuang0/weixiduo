package com.hechuang.fruitstore.vo;

import lombok.Data;

/**
 * Created by Stefan
 * Create Date 2018-02-26/18:44
 */
@Data
public class BusinessVO {
    private Integer id;
    private String businessName;
    private String businessImage;
    private String businessAddress;
    private double distance;
}
