package com.hechuang.fruitstore.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Create by ToFree on 2018/2/4
 */
@Data
public class VipMemberVO implements Serializable {
    private static final long serialVersionUID = -3532886057112506309L;
    private Integer id;
    private Integer vipCardId;//vip_card的外键
    private String name;//会员名
    private String sex;//性别，先生，女士
    private String head;//头像
    private String address;//家庭住址
    private String realName;//真实姓名
    private String birthday;//出生年月
    private Integer haschild;//是否有小孩（0无1有）
    private Integer ismarried;//是否已婚（0未婚，1已婚）
    private String phone;//手机号
    private String openid;//微信用户唯一标识
    private String createTime;//
    private String updateTime;//
    private String vipName;//会员卡名
    private Double discount;//会员折扣
}
