package com.hechuang.fruitstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class FruitstoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(FruitstoreApplication.class, args);
	}

}
