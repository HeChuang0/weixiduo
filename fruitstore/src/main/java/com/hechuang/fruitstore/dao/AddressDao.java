package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: fing
 * @Description:
 * @Date: 下午7:44 18-1-16
 */

@Repository
public interface AddressDao extends JpaRepository<Address, Integer> {

	List<Address> findAddressByOpenid(String id);

	@Transactional
	@Modifying
	@Query("DELETE FROM Address WHERE id = :id")
	int deleteById(@Param("id") Integer id);

	Address findByCheckedAndOpenid(Integer status,String openid);


}
