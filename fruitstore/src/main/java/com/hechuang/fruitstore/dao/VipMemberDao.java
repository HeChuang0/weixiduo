package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.VipMember;
import com.hechuang.fruitstore.vo.VipMemberVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by Stefan
 * Create Date 2018-01-14/23:58
 */
public interface VipMemberDao  extends JpaRepository<VipMember, Integer> {
    VipMember findByOpenid(String openId);


}
