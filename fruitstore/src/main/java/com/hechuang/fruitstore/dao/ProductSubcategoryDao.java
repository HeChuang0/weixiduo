package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.ProductSubcategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-02-07/20:13
 */

public interface ProductSubcategoryDao extends JpaRepository<ProductSubcategory,Integer> {
    Page<ProductSubcategory> findByMaincategoryId(Integer id,Pageable pageable);
    Long countByMaincategoryId(Integer id);

    List<ProductSubcategory> findByMaincategoryId(Integer id);
}
