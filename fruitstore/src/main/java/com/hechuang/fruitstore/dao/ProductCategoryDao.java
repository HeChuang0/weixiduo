package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.ProductCategory;
import com.hechuang.fruitstore.pojo.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.ManyToOne;
import java.beans.Transient;
import java.util.List;

/**
 * @Author: fing
 * @Description:
 * @Date: 上午10:18 18-1-15
 */
@Repository
public interface ProductCategoryDao extends JpaRepository<ProductCategory,Integer> {

	@Transactional
	@Modifying
	@Query("DELETE FROM ProductCategory WHERE id = :id")
	int deleteById(@Param("id") Integer id);
}
