package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Create by ToFree on 2018/1/15
 */
public interface CartDao extends JpaRepository<ShoppingCart, Integer> {
    @Query(value = "SELECT * FROM shopping_cart WHERE openid = ?1 AND product_id = ?2", nativeQuery = true)
    public ShoppingCart getCartByProductIdOpenid(String openid, Integer productId);

    public ShoppingCart findByProductIdAndOpenid(Integer productId,String openid);

    @Query(value = "SELECT * FROM shopping_cart WHERE openid = ?1", nativeQuery = true)
    List<ShoppingCart> getCartByOpenid(String openid);

    @Query(value = "SELECT COUNT(*) FROM shopping_cart WHERE cart_status = 0 AND openid = ?1", nativeQuery = true)
    int getCartProductCheckedStatusByOpenid(String openid);

    @Transactional
    @Modifying
    @Query("UPDATE ShoppingCart SET cartStatus = ?2 WHERE openid = ?1")
    int checkedOrUncheckedAllProduct(String openid, Integer checked);

    @Transactional
    @Modifying
    @Query(value = "UPDATE ShoppingCart SET cartStatus = ?3 WHERE openid = ?1 AND productId = ?2")
    int checkedOrUncheckedProduct(String openid, Integer productId, Integer checked);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM shopping_cart WHERE openid = :openid AND product_id = :productId", nativeQuery = true)
    int deleteByOpenidProductIds(@Param("openid") String openid, @Param("productId") Integer productId);

    @Query(value = "SELECT COUNT(id) AS count FROM shopping_cart WHERE openid = ?1", nativeQuery = true)
    int getCartProductCount(String openid);
}
