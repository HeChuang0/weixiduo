package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Create by ToFree on 2018/1/19
 */
public interface ManagerDao extends JpaRepository<Manager, Integer> {

    @Query(value = "SELECT * FROM manager WHERE manager_name = ?1", nativeQuery = true)
    Manager getByName(String manegerName);
}
