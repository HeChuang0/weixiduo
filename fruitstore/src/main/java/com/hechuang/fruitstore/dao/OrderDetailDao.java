package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.xml.soap.Detail;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-01-17/0:45
 */
public interface OrderDetailDao extends JpaRepository<OrderDetail,Integer> {
    List<OrderDetail> findByOrderid(String orderid);
}
