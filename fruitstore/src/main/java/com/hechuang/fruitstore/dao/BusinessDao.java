package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.Business;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by Stefan
 * Create Date 2018-02-26/17:32
 */
public interface BusinessDao extends JpaRepository<Business,Integer> {
}
