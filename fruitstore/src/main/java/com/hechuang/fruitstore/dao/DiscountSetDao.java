package com.hechuang.fruitstore.dao;
import com.hechuang.fruitstore.pojo.DiscountSet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-01-16/16:58
 */
public interface DiscountSetDao extends JpaRepository<DiscountSet, Integer> {
    void deleteByEndTimeAfter(Date overdueTime);
    List<DiscountSet> findByType(Integer type);

}
