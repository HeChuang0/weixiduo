package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.OrderMaster;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * Create by ToFree on 2018/1/12
 */
public interface OrderMasterDao extends JpaRepository<OrderMaster, String> {
//查询单个订单
    OrderMaster findByOrderid(String orderid);
    //用户查看订单列表
    List<OrderMaster> findByOpenidOrderByOrderidDesc(String openid);
    //商家查看退款的订单列表
    Page<OrderMaster> findByOrderStatusOrderByOrderidDesc(Integer status,Pageable pageable);
    Long countByOrderStatus(Integer status);


    //商家查看最近48小时未发货的订单
    @Query(value = "SELECT * FROM order_master WHERE date_add(NOW(), interval -2 day)<=create_time AND order_status=0 order by orderid DESC limit ?1,?2  ", nativeQuery = true)
    List<OrderMaster> latelyOrderList(Integer page,Integer size);
    @Query(value = "SELECT count(*) FROM order_master WHERE date_add(NOW(), interval -2 day)<=create_time AND order_status=0", nativeQuery = true)
    Long countlatelyOrder();


    //商家查看发货后或者新订单的订单列表
   Page<OrderMaster> findByCreateTimeBetweenAndOrderStatusNotOrderByOrderidDesc(Date pastDate,Date currentDate,Integer status,Pageable pageable);
   Long countByCreateTimeBetweenAndOrderStatusNot(Date pastDate,Date currentDate,Integer status);


   List<OrderMaster> findByPayStatusOrderByOrderidDesc(Integer status);

}
