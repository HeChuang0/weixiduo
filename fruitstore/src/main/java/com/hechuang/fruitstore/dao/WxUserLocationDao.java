package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.WxUserLocation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Stefan
 * Create Date 2018-02-27/19:25
 */
public interface WxUserLocationDao extends JpaRepository<WxUserLocation,String> {

}
