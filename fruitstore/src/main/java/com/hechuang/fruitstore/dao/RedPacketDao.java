package com.hechuang.fruitstore.dao;
import com.hechuang.fruitstore.pojo.RedPacket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-01-16/14:43
 */
public interface RedPacketDao extends JpaRepository<RedPacket,Integer> {
    Page<RedPacket> findByOpenid(String openid,Pageable pageable);
    RedPacket findByDiscountIdAndOpenid(Integer discount_id,String openid);
    List<RedPacket> findByOpenid(String openid);

}
