package com.hechuang.fruitstore.dao;
import com.hechuang.fruitstore.pojo.Sign;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-01-15/23:35
 */
public interface SignDao extends JpaRepository<Sign,Integer> {
    List<Sign> findByOpenid(String openid);
}
