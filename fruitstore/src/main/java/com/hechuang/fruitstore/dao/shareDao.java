package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.Share;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Create by stefan
 * Date on 2018-04-11  16:15
 * Convertion over Configuration!
 */
public interface shareDao extends JpaRepository<Share,String> {
}
