package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.ProductInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: fing
 * @Description:
 * @Date: 下午9:10 18-1-15
 */

@Repository
public interface ProductInfoDao extends JpaRepository<ProductInfo,Integer> {

	List<ProductInfo> findProductInfoByProductNameContaining(String condition);
//通过主类目查询商品
	List<ProductInfo> findProductInfoByCategoryId(Integer id);
//通过子类目查询商品
	List<ProductInfo> findBySubCategoryId(Integer subId);
	@Transactional
	@Modifying
	@Query("DELETE FROM ProductInfo WHERE id = :id")
	int deleteById(@Param("id") Integer id);

	@Transactional
	@Modifying
	@Query("UPDATE ProductInfo SET productStatus = 1 WHERE id = :id")
	int onSale(@Param("id") Integer id);

	@Transactional
	@Modifying
	@Query("UPDATE ProductInfo SET productStatus = 0 WHERE id = :id")
	int offSale(@Param("id") Integer id);


	List<ProductInfo> findByForetaste(Integer type);

	@Query(value = "SELECT  * FROM  product_info WHERE  date_add(NOW(), interval -7 day)<=create_time AND product_status=0",nativeQuery=true)
	List<ProductInfo> getNewProducts();



}
