package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.Integral;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Stefan
 * Create Date 2018-01-15/22:02
 */
public interface IntegralDao extends JpaRepository<Integral, Integer> {
    Integral findByOpenid(String openid);
}

