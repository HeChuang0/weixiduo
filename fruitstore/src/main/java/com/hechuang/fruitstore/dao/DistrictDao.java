package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.District;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @Author: xht
 * @Date: 15:42 2018/2/27
 */
public interface DistrictDao extends JpaRepository<District,Integer> {
    List<District> findDistrictsByParentId(Integer parentId);
    District findDistinctByName(String name);
}
