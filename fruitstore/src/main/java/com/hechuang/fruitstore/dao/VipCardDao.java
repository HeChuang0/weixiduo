package com.hechuang.fruitstore.dao;

import com.hechuang.fruitstore.pojo.VipCard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VipCardDao extends JpaRepository<VipCard,Integer> {

}
