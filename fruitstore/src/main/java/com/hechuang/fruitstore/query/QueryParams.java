package com.hechuang.fruitstore.query;

import lombok.Data;

import java.util.List;

/**
 * Create by ToFree on 2018/1/11
 */
@Data
public class QueryParams <T>{
    protected Integer id;
    protected Integer page;
    protected Integer rows;
    protected Integer pageSize;

    protected Integer recordIndex;
    protected String orderBy;
    protected List<T> datas;
    protected List<T> pojos;
    protected String condition;
}
