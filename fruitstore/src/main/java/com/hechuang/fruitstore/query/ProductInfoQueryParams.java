package com.hechuang.fruitstore.query;

import com.hechuang.fruitstore.pojo.ProductInfo;
import lombok.Data;

/**
 * @Author: fing
 * @Description:
 * @Date: 下午9:07 18-1-15
 */
@Data
public class ProductInfoQueryParams extends QueryParams<ProductInfo> {

	private Integer categoryId;

	private String condition;

    public Integer getPageSize() {
        return pageSize;
    }
}
