package com.hechuang.fruitstore.query;

import com.hechuang.fruitstore.pojo.OrderMaster;
import lombok.Data;

/**
 * Create by ToFree on 2018/1/12
 */
@Data
public class OrderMasterQueryParams extends QueryParams<OrderMaster> {
    private String orderid;
    private String openid;
}
