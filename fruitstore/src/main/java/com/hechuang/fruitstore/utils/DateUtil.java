package com.hechuang.fruitstore.utils;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Stefan
 * Create Date 2018-01-24/20:32
 */
public class DateUtil {
    public static  Date getDate(String date){
        long currentTime = System.currentTimeMillis();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date temp=new Date();
        try {
            temp=formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return temp;
    }
}
