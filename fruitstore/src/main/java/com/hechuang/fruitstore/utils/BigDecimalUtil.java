package com.hechuang.fruitstore.utils;

import java.math.BigDecimal;

/**
 * Create by ToFree on 2018/1/15
 */
public class BigDecimalUtil {
    private BigDecimalUtil(){}

    /**
     * 价格的加法
     * @param n1
     * @param n2
     * @return
     */
    public static BigDecimal add(double n1, double n2){
        BigDecimal b1 = new BigDecimal(Double.toString(n1));
        BigDecimal b2 = new BigDecimal(Double.toString(n2));
        return b1.add(b2);
    }

    /**
     * 价格的减法
     * @param n1
     * @param n2
     * @return
     */
    public static BigDecimal sub(double n1, double n2){
        BigDecimal b1 = new BigDecimal(Double.toString(n1));
        BigDecimal b2 = new BigDecimal(Double.toString(n2));
        return b1.subtract(b2);
    }

    /**
     * 价格的乘法
     * @param n1
     * @param n2
     * @return
     */
    public static BigDecimal mul(double n1, double n2){
        BigDecimal b1 = new BigDecimal(Double.toString(n1));
        BigDecimal b2 = new BigDecimal(Double.toString(n2));
        return b1.multiply(b2);
    }

    /**
     * 价格的除法
     * @param n1
     * @param n2
     * @return
     */
    public static BigDecimal div(double n1, double n2){
        BigDecimal b1 = new BigDecimal(Double.toString(n1));
        BigDecimal b2 = new BigDecimal(Double.toString(n2));
        //除不尽的策略:保留两位小数，四舍五入
        return b1.divide(b2,2,BigDecimal.ROUND_HALF_UP);


    }


}
