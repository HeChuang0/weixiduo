package com.hechuang.fruitstore.utils;


import com.hechuang.fruitstore.enums.CodeEnum;

/**
 * 枚举工具类
 * Create by ToFree on 2018/1/11
 */
public class EnumUtil {

    public static <T extends CodeEnum> T getByCode(Integer code, Class<T> enumClass) {
        for (T each: enumClass.getEnumConstants()) {
            if (code.equals(each.getCode())) {
                return each;
            }
        }
        return null;
    }
}
