package com.hechuang.fruitstore.utils;

import com.hechuang.fruitstore.pojo.DiscountSet;
import com.hechuang.fruitstore.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Stefan
 * Create Date 2018-01-14/0:58
 */
@Slf4j
public class ResultVOUtil {
    public static ResultVO success(String msg, Object object){
        ResultVO resultVO=new ResultVO(msg);
        if (object instanceof List ){
            List datas=new ArrayList();
            datas.addAll((Collection) object);
            resultVO.add(datas);
            log.error(resultVO.toString());
        }else {
            resultVO.add(object);
            log.error(resultVO.toString());
        }
        return  resultVO;
    }

    public static ResultVO success(String msg){
        ResultVO resultVO=new ResultVO(msg);
        return  resultVO;
    }

    public static ResultVO error(String msg){
        ResultVO resultVO=new ResultVO();
        resultVO.setSuccess(false);
        resultVO.setErrMsg(msg);
        return  resultVO;
    }
}
