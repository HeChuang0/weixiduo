package com.hechuang.fruitstore.utils;

import com.hechuang.fruitstore.config.BaiduMapConfig;
import com.hechuang.fruitstore.config.BaiduMapResultBean;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by Stefan
 * Create Date 2018-02-26/14:26
 */
@Slf4j
public class HttpClientUtil {

    public static String sendGet(String url){
        //String url="http://api.map.baidu.com/geocoder/v2/?address=北京市海淀区上地十街10号&output=json&ak=K3WRXBCoQ2Z3kDwgnFKPiRZSVOqO1goE";
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()){
                //log.info("信息{}",response.body().string());
                return response.body().string();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void main(String args[]) {
        BaiduMapConfig baiduMapConfig=new BaiduMapConfig();
        HttpClientUtil httpClientUtils=new HttpClientUtil();
        String realUrl=String.format(baiduMapConfig.getGeocodingUrl(),"北京市海淀区上地十街10号","json",baiduMapConfig.getAk());
        httpClientUtils.sendGet(realUrl);
        BaiduMapResultBean baiduMapResultBean=new BaiduMapResultBean();
        baiduMapResultBean= (BaiduMapResultBean) JsonUtil.jsonToObject(httpClientUtils.sendGet(realUrl), baiduMapResultBean);
        ;
        System.out.println(baiduMapResultBean.getResult());

    }

}
