package com.hechuang.fruitstore.utils;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.web.multipart.MultipartFile;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
@Slf4j
public class ImageUtil {
    public static  InputStream cut(MultipartFile in){
        try {
            BufferedImage b= ImageIO.read(in.getInputStream());
            int len=b.getWidth()>b.getHeight()?b.getHeight():b.getWidth();
            File f=new File(in.getOriginalFilename());
            Thumbnails.of(in.getInputStream()).size(len,len).keepAspectRatio(false).toFile(f);
            InputStream is=new FileInputStream(f);
            return is;
        } catch (IOException e) {
            log.error("图片处理失败");
            e.printStackTrace();
        }
        return null;


    }
}
