package com.hechuang.fruitstore.utils;

import com.google.gson.Gson;
import com.hechuang.fruitstore.vo.ResultVO;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;
@Slf4j
public class QiNiuUtil {

    //构造一个带指定Zone对象的配置类
    static Configuration cfg = new Configuration(Zone.zone0());
    static UploadManager uploadManager = new UploadManager(cfg);
    //生成上传凭证，然后准备上传
    static String accessKey = "kg7IEHmcjLMkBEM4PlQjowQVK7xsNbwKejM0hWPk";
    static String secretKey = "NJdQ8YQfbmdTTYXXbseweKAD5qTsmlC22FwcrlWt";
    //空间名
    static String bucket = "weixiduo";
    static Auth auth = Auth.create(accessKey, secretKey);
    //七牛云外链

    static String link = "http://p2ue116au.bkt.clouddn.com/";
    public static ResultVO uploadByStream(InputStream in){
        return uploadByStream(in,null);
    }

    public static ResultVO uploadByStream(InputStream in, String key){
        String url = null;
        String upToken = auth.uploadToken(bucket);
        try {
            Response response = uploadManager.put(in,key,upToken,null,null);
            //解析上传成功的结果
            log.info("上传状态{}",response.statusCode);
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            url = link + putRet.key;
            log.info("上传的图片链接{}",url);


        } catch (QiniuException ex) {
            Response r = ex.response;
            System.err.println(r.toString());
            try {
                System.err.println(r.bodyString());
            } catch (QiniuException ex2) {

            }
        }
        return  new ResultVO(true,url);
    }

}
