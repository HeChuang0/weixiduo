package com.hechuang.fruitstore.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Create by ToFree on 2018/1/11
 */
public class JsonUtil {

    public static String toJson(Object object) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        return gson.toJson(object);
    }


    public static Object jsonToObject(String json,Object object){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        gson.fromJson(json,object.getClass());
        return gson.fromJson(json,object.getClass());
    }

}
