package com.hechuang.fruitstore.constant;

/**
 * Create by ToFree on 2018/1/19
 */
public interface CookieConstant {
    String TOKEN = "token";
    Integer EXPIRE = 7200;
}
