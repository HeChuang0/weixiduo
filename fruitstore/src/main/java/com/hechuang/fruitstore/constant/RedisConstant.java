package com.hechuang.fruitstore.constant;

/**
 * redis常量
 * Create by ToFree on 2018/1/19
 */
public interface RedisConstant {
    String TOKEN_PREFIX = "token_%s";
    Integer EXPIRE = 86400;//2小时
}
