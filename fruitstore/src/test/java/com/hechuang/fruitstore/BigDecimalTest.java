package com.hechuang.fruitstore;

import org.junit.Test;

import java.math.BigDecimal;

/**
 * Create by ToFree on 2018/1/15
 */
public class BigDecimalTest {
    @Test
    public void test1() {
//        System.out.println(0.05+0.01);
//        System.out.println(1.0-0.42);
//        System.out.println(4.015*100);
//        System.out.println(123.3/100);
        BigDecimal num1 = new BigDecimal(0.05);
        BigDecimal num2 = new BigDecimal(0.01);
        System.out.println(num1.add(num2));
        BigDecimal n1 = new BigDecimal("0.05");
        BigDecimal n2 = new BigDecimal("0.01");
        System.out.println(n1.add(n2));
    }

}
